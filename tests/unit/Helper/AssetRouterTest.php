<?php
namespace Spinit\Osy\Helper\AssetRouter\TestUnit;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Helper\AssetRouter;
use Spinit\Osy\Http\Response;

/**
 * Description of AssetRoutertest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class AssetRouterTest extends TestCase
{
    public function testInvokePhp()
    {
        $asset = new AssetRouter(new Instance(), __DIR__, 'assetData/fileFound.php');
        $response = $asset(new Response());
        $this->assertEquals('ok', $response['test']);
        $this->assertEquals('fileFound', $response['info']['filename']);
        $this->assertEquals('', $response['info']['path']);
    }
    /**
     * @expectedException \Spinit\Util\Error\StopException
     */
    public function testInvokeText()
    {
        
        $asset = new AssetRouter(new Instance(), __DIR__, 'assetData/fileFound.txt');
        $this->expectOutputString(file_get_contents(__DIR__ . '/assetData/fileFound.txt'));
        $asset(null);
    }
    /**
     * @expectedException \Spinit\Util\Error\NotFoundException
     */
    public function testInvokeNotFound()
    {
        
        $asset = new AssetRouter(new Instance(), __DIR__, 'assetData/fileNotFound.txt');
        $asset(null);
    }
    
    public function testInvokeSearchFile()
    {
        $asset = new AssetRouter(new Instance(), __DIR__, 'assetData/fileFound/prova/image.png');
        $response = $asset(new Response());
        $this->assertEquals('ok', $response['test']);
        $this->assertEquals('prova/image.png', $response['info']['path']);
    }
    public function testInvokeSearchFile2()
    {
        $asset = new AssetRouter(new Instance(), __DIR__, 'assetData/fileFound');
        $response = $asset(new Response());
        $this->assertEquals('ok', $response['test']);
        $this->assertEquals('', $response['info']['path']);
    }
}

class Instance implements \Spinit\Osy\Type\InstanceInterface
{
    
    public function getInfo() {
        
    }

    public function getMain() {
        
    }

    public function getManager() {
        
    }

    public function getRequest() {
        
    }

    public function getRoot() {
        
    }

    public function run() {
        
    }

}