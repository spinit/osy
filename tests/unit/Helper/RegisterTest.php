<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Helper\RegisterUnitTest;

use Spinit\Osy\Helper\Register;
use Spinit\Util;
use PHPUnit\Framework\TestCase;

/**
 * Description of RegisterTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class RegisterTest extends TestCase
{
    public function testObject() {
        $object = new Register();
        $object->addItem('uno', array('id'=>1));
        $ct = $object->addRegister(new Register());
        $ct->addItem('due', array('id'=>2));
        $list = $object->getItems();
        $this->assertEquals(array('uno', 'due'), array_keys($list->getList()));
    }
}