<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Helper\ScanDirModules\TestUnit;

use Spinit\Osy\Helper\ScanDirModules;
/**
 * Description of ScanModulesTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ScanDirModulesTest extends \PHPUnit\Framework\TestCase
{
    public function testScan()
    {
        $object = new ScanDirModules();
        $this->assertEquals(array('ok:testDir', 'ok:testFile'), $object->getItemList(__DIR__.'/scandir', 'Prova'));
        $this->assertEquals(array(), $object->getItemList(__DIR__.'/scandirUnknow', 'Prova'));
    }
}
