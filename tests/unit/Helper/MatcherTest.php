<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Helper\MatcherTestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Helper\Matcher;
/**
 * Description of MatcherTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MatcherTest extends TestCase
{
    /**
     *
     * @var Matcher
     */
    private $object;
    
    public function setUp() {
        $this->object = new Matcher();
    }
    
    public function testExec()
    {
        $actual = new \ArrayObject(['uno'=>2, 'due'=>1]);
        
        $this->object->add('/path/:code?i', function($args) use ($actual) {
            $actual['uno'] = intval($args['code']);
        });
        $this->object->add('/path/:code?w', function($args) use ($actual) {
            $actual['uno'] = $args['code'];
        });
        $this->object->add('/path/:name?i/edit', function($args) use ($actual) {
            $actual['due'] = intval($args['name']);
        });
        $this->assertEquals(['uno'=>2, 'due'=>1], $actual->getArrayCopy());
        $this->object->exec('/path/1');
        $this->assertEquals(['uno'=>1, 'due'=>1], $actual->getArrayCopy());
        $this->object->exec('/path/tre');
        $this->assertEquals(['uno'=>'tre', 'due'=>1], $actual->getArrayCopy());
        $this->object->exec('/path/2/edit');
        $this->assertEquals(['uno'=>'tre', 'due'=>2], $actual->getArrayCopy());
        
    }
}
