<?php
namespace Spinit\Osy\UnitTest;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Spinit\Osy\Main;
use PHPUnit\Framework\TestCase;
use Spinit\Datasource\DataSource;
use Spinit\Datasource\Core\DataSetInterface;
use Spinit\Datasource\Core\AdapterInterface;
use Spinit\Util\Dictionary;
use Spinit\Osy\Http\Response;

class MainTest extends TestCase
{
    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        
        // registrando il nuovo protocollo posso gestire le connection string con la classe di test
        DataSource::registerAdapter('test', function($connectionString) {
            return new AdapterTestWrap($connectionString);
        });
    }
    public function testGetInstance()
    {
        $app = new MainWrap('test:notFound');
        $instance = $app->getInstance();
        $this->assertInstanceOf("\\Spinit\\Osy\\Core\\Instance", $instance);
        $this->assertEquals('test:notFound', $app->getConnestionString());
    }
    public function testRunError()
    {
        $app = new MainWrapInstance('test:notFound', function() {throw new \Exception('TEST OK');});
        $response = $app->run();
        $this->assertEquals('error', $response['status']);
        $this->assertEquals('TEST OK', $response['message']);
    }
    public function testRunOk()
    {
        $app = new MainWrapInstance('test:notFound', function() {return new Response(array('test'=>'ok'));});
        $response = $app->run();
        $this->assertEquals('success', $response['status']);
        $this->assertEquals('ok', $response['test']);
    }
    
    public function testAppRegistered()
    {
        $app = new MainWrap('test:notFound');
        $list = $app->getAppRegister();
        $this->assertContains('uri:opensymap:opensymap.org#System', $list->getItem(md5('uri:opensymap:opensymap.org#System')));
    }
}

class MainWrap extends Main
{
    protected function makeRequest()
    {
        return $this;
    }
    public function getUriPath()
    {
        return '';
    }
    public function getRoot()
    {
        return '';
    }
}

class MainWrapInstance extends MainWrap
{
    public function __construct($connectionString, $callable) {
        parent::__construct($connectionString);
        $this->callable = $callable;
    }
    public function getInstance($request = null) {
        return new Dictionary(array('run' => $this->callable));
    }
}
class AdapterTestWrap implements AdapterInterface
{
    private $conf;
    
    public function __construct($conf)
    {
        $this->conf = $conf;
    }

    public function align($struct) {
        
    }

    public function delete($resource, $key) {
        
    }

    public function insert($resource, $data) {
        
    }

    public function load($query, $param = array(), $args = array())
    {
        switch($this->conf) {
            case 'test:empty':
                throw new \Exception();
        }
        return new DataSetTestWrap($this->conf);
    }

    public function update($resource, $data, $key, $compare = false) {
        
    }

    public function find($resource, $fields, $pkey) {
        
    }

    public function first($query, $param = array()) {
        
    }

    public function getName() {
        
    }

    public function getSource() {
        
    }

    public function setSource($source) {
        
    }

}
class DataSetTestWrap implements DataSetInterface
{
    private $conf;
    
    public function __construct($conf)
    {
        $this->conf = $conf;
    }
    public function close() {
        
    }

    public function current() {
        switch($this->conf) {
            case 'test:notFound':
                return null;
        }
        $data = \array_pop(explode(':', $this->conf));
        return json_decode(base64_decode($data), 1);
    }

    public function isOpen() {
        
    }

    public function key() {
        
    }

    public function next() {
    }

    public function position() {
        
    }

    public function rewind() {
        
    }

    public function valid() {
        
    }
    public function getMetadata($type = '')
    {
        
    }

    public function getList() {
        return [];
    }

}
