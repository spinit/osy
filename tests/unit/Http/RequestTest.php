<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Http\TestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Http\Request;

/**
 * Description of RequestTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class RequestTest extends TestCase
{
    /**
     *
     * @var Request
     */
    private $object;
    
    public function testPart()
    {
        $this->object = new Request('POST', 'http://www.demo.test/path/example/');
        $this->assertEquals("http://www.demo.test/path/example/", (string) $this->object);
        $this->assertEquals("http://www.demo.test/path/example/", (string) $this->object->getRequest()->getUri());
    }
}
