<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Http\TestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Http\ServerRequest;

/**
 * Description of RequestTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class ServerRequestTest extends TestCase
{
    /**
     *
     * @var Request
     */
    private $object;
    
    public function setUp()
    {
        $this->object = new ServerRequest('POST', 'http://www.demo.test/path/example/');
    }
    public function testPart()
    {
        $this->assertEquals("http://www.demo.test/path/example/", (string) $this->object);
        $this->assertEquals("http://www.demo.test/path/example/", (string) $this->object->getRequest()->getUri());
    }
    public function testMethodRequest()
    {
        $this->assertEquals("POST", $this->object->getMethod());
        $this->object->withMethod('GET');
        $this->assertEquals("GET", $this->object->getMethod());
    }
    public function testMethodUri()
    {
        $this->assertEquals("www.demo.test", $this->object->getHost());
        $this->object->withHost('www.prova.test');
        $this->assertEquals("http://www.prova.test/path/example/", (string) $this->object);
    }
    
    public function testRoot()
    {
        $object = new ServerRequest('POST', 'http://www.demo.test/path/example/', ['Proxypath'=>'Ttest'], null, '1.1', array('SCRIPT_NAME'=>'/path/index.php'));
        $this->assertEquals("www.demo.test/Ttest/path", $object->getRoot());
        
        $object = new ServerRequest('POST', 'http://www.demo.test/path/example/', [], null, '1.1', array('SCRIPT_NAME'=>'/path/index.php'));
        $this->assertEquals("www.demo.test/path", $object->getRoot());
        
        $object = new ServerRequest('POST', 'http://www.demo.test/path/example/', ['Proxypath'=>'Ttest'], null, '1.1', array('SCRIPT_NAME'=>'/index.php'));
        $this->assertEquals("www.demo.test/Ttest", $object->getRoot());
        
        $object = new ServerRequest('POST', 'http://www.demo.test/path/example/', [], null, '1.1', array('SCRIPT_NAME'=>'/index.php'));
        $this->assertEquals("www.demo.test", $object->getRoot());

        $object = new ServerRequest('POST', 'http://www.demo.test/path/example/');
        $this->assertEquals("www.demo.test", $object->getRoot());

    }
}
