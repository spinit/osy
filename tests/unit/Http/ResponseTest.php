<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Http\TestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Http\Response;

/**
 * Description of ResponseTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class ResponseTest extends TestCase
{
    /**
     *
     * @var Response
     */
    private $object;
    
    public function setUp()
    {
        $this->object = new ResponseWrap();
    }
    
    public function testResponseEmpty()
    {
        $response = (string) $this->object;
        $arrayResponse = json_decode($response, 1);
        $this->assertEquals("success", $arrayResponse['status']);
        $this->assertEquals("", $arrayResponse['message']);
        $this->assertEquals(array(), $arrayResponse['data']);
    }
    
    public function testAddData()
    {
        $this->object->addData('type1', array('una'=>'prova'));
        $this->object->addData('type2', 'due', 'prove');
        $actual = \json_decode((string) $this->object, 1);
        $this->assertEquals(array(array("una"=>"prova")), $actual['data']['type1']);
        $this->assertEquals(array("due"=>"prove"), $actual['data']['type2']);
    }
    
    public function testAddCommand()
    {
        $this->object->addCommand('alert',"Messaggio di test");
        $actual = \json_decode((string) $this->object, 1);
        $this->assertEquals(array(array("alert", "Messaggio di test")), $actual['data']['command']);
    }
    
    public function testSetData()
    {
        $this->object->setData('type1', array('una'=>'prova'));
        $actual = \json_decode((string) $this->object, 1);
        $this->assertEquals(array("una"=>"prova"), $actual['data']['type1']);
        $this->object->setData('type1', array('due'=>'prova'));
        $actual = \json_decode((string) $this->object, 1);
        $this->assertEquals(array("due"=>"prova"), $actual['data']['type1']);
        $this->object->setData('type1', array('una'=>'prova'), array('due'=>'prova'));
        $actual = \json_decode((string) $this->object, 1);
        $this->assertEquals([array("una"=>"prova"),array("due"=>"prova")], $actual['data']['type1']);
    }
}

class ResponseWrap extends Response
{
    public function sendHeader($header) {
        return;
    }
}