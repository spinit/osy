<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Http\TestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Http\ResponseHtml;

/**
 * Description of ResponseTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class ResponseHtmlTesto extends TestCase
{
    /**
     *
     * @var Response
     */
    private $object;
    
    public function setUp()
    {
        $this->object = new ResponseHtmlWrap();
    }
    
    public function testResponseEmpty()
    {
        $response = (string) $this->object;
        $this->assertEquals("", $response);
    }
    public function testResponseNotEmpty()
    {
        $this->object->addContent('una');
        $this->object->addContent(' prova');
        $this->assertEquals("una prova", (string) $this->object);
        $this->object->addContent('un',array('altro','test'));
        $this->assertEquals('una prova["altro","test"]', (string) $this->object);
        
    }
    public function testResponseStatus()
    {
        $this->object->trigger('setTemplate','<!--.status-->');
        $response = (string) $this->object;
        $this->assertEquals("success", $response);
    }
    public function testResponseTemplate()
    {
        $this->object->trigger('setTemplate','<!--js--> <!--css-->');
        $this->object->trigger('addjs','hello');
        $this->object->trigger('addcss','world');
        $response = (string) $this->object;
        $this->assertEquals("hello world", $response);
    }
}

class ResponseHtmlWrap extends ResponseHtml
{
    public function sendHeader($header) {
        return;
    }
}