<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy\Core\ModelTestUnit;
use Spinit\Osy\Core\Model;
use PHPUnit\Framework\TestCase;
use Spinit\Osy\Adapter\Xml\ModelXmlAdapter;
use Spinit\Osy\Type\ApplicationInterface;

/**
 * Description of Model
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelTest extends TestCase
{
    public function testConstruct()
    {
        $fname = __DIR__.'/adapter/model/index.xml';
        $model = new Model(new ApplicationMock(), new ModelXmlAdapter($fname));
    }
}

class ApplicationMock implements ApplicationInterface
{
    
    public function getInstance() {
        return $this;
    }

    public function getModel($name) {
        
    }

    public function getDataSource($name = '') {
        
    }

    public function getLabel() {
        return '';
    }

    public function getName() {
        return '';
    }

}
