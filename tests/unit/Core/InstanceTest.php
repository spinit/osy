<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core\InstanceTestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Core\Instance;
use Spinit\Osy\Http\ServerRequest;
use Spinit\Osy\Test\DictionaryMock;
use Spinit\Datasource\Core\DataSetInterface;
use Spinit\Osy\Test\DummyDataSet;
use Spinit\Datasource\DataSource;
use Spinit\Util;

/**
 * Description of Instance
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class InstanceTest extends TestCase
{

    
    public function testRun()
    {
        $record = array('id'=>'1111', 'manager'=>'Identity', 'url'=>'www.domain.test/un');
        $mock = new DictionaryMock(array('load' => function() use ($record) { return new DataSetWrap($record); }));
        
        $object = new Instance($mock, new ServerRequest('POST', 'www.domain.test/un/path/'));
        
        $response = $object->run();
        $this->assertEquals("success", $response['status']);
    }
    
    public function testGetManagerEmpty()
    {
        $record = array('id'=>'1111', 'manager'=>'');
        $mock = new DictionaryMock(array('load' => function() use ($record) { return new DataSetWrap($record); }));
        
        $object = new Instance($mock, new ServerRequest('POST', 'www.domain.test/un/path/'));
        $manager = $object->getManager();
        $this->assertInstanceOf("\\Spinit\Osy\\Core\\Manager\\Frontend", $manager);
        $this->assertEquals('Frontend', $object->getInfo('manager'));
        $this->assertEquals('1111', $object->getInfo('id'));
        $this->assertEquals('', $object->getInfo('fieldUnknow'));
    }
    public function testGetManagerCustom()
    {
        $record = array('id'=>'1111', 'manager'=>'Identity');
        $mock = new DictionaryMock(array('load' => function() use ($record) { return new DataSetWrap($record); }));
        
        $object = new Instance($mock, new ServerRequest('POST', 'www.domain.test/un/path/'));
        $manager = $object->getManager();
        $this->assertInstanceOf("\\Spinit\Osy\\Core\\Manager\\Identity", $manager);
        $this->assertEquals('ok test', $manager->run('ok test'));
    }
    public function testGetManagerIstanceNotFound()
    {
        $record = false;
        $mock = new DictionaryMock(array('load' => function() use ($record) { return new DataSetWrap($record); }));
        
        $object = new Instance($mock, new ServerRequest('POST', 'www.domain.test/un/path/'));
        $manager = $object->getManager();
        $this->assertInstanceOf("\\Spinit\Osy\\Core\\Manager\\NotFound", $manager);
    }
    public function testGetManagerNoIstance()
    {
        $mock = new DictionaryMock(array('load' => function() {throw new \Exception(); }));
        
        $object = new Instance($mock, new ServerRequest('POST', 'www.domain.test/un/path/'));
        $manager = $object->getManager();
        $this->assertInstanceOf("\\Spinit\Osy\\Core\\Manager\\Install", $manager);
    }
}

class DataSetWrap extends DummyDataSet
{
    private $record;
    
    public function __construct($record)
    {
        $this->record = $record;
    }
    public function current()
    {
        return $this->record;
    }
}
