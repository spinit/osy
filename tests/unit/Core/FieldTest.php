<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core\FieldTestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Core\Field;
use Spinit\Osy\Type\FieldInterface;
use Spinit\Osy\Adapter\Xml\FieldXmlAdapter;
use Spinit\Osy\Type\FormInterface;
use Spinit\Osy\Type\FieldBuilderInterface;
use Spinit\Util;

/**
 * Description of Instance
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class FieldTest extends TestCase
{
    public function testBuild()
    {
        $field = new Field(new FormMock(), new FieldXmlAdapter('<field name="uno"><field/><field/></field>'));
        $this->assertEquals('uno', $field->build());
        $this->assertEquals(2, count($field->getChilds()));
    }
    public function testSetValue()
    {
        $field = new Field(new FormMock(), new FieldXmlAdapter('<field name="uno"></field>'));
        $field->setValue('due');
        $this->assertEquals('due', $field->getValue());
        $this->assertEquals(0, count($field->getChilds()));
    }
}

class FormMock implements FormInterface, FieldBuilderInterface
{
    
    public function getFieldBuilder($type)
    {
        return $this;
    }

    public function build(FieldInterface $field, $event = null) {
        return $field->getName();
    }
    public function trigger($name)
    {
        
    }
    public function getFullName() {
        return 'Mock';
    }

    public function getValue(FieldInterface $field) {
        
    }

}