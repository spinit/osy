<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy\Core\Manager\Factory\TestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Core\Manager\Factory;
use Spinit\Util;
use Spinit\Osy\Core\Manager;
use Spinit\Osy\Type\InstanceInterface;

/**
 * Description of FactoryTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class FactoryTest extends TestCase
{
    public function testConstructor()
    {
        Factory::registerManagerConstructor('MyManager', array($this, 'constructorTest'));
        
        $manager = Factory::getManager('MyManager', new InstanceWrap());
        $this->assertInstanceOf(__NAMESPACE__.'\\MyManager', $manager);
        $this->assertEquals(__NAMESPACE__.'\\MyManager', $manager->run('senza router ritorna il nome della classe'));
    }
    public function testRouter()
    {
        Factory::registerManagerRouter('MyManager', function($path) {
            return array($this, 'runManager');
        });
        
        $manager = Factory::getManager('MyManager', new InstanceWrap());
        $this->assertInstanceOf(__NAMESPACE__.'\\MyManager', $manager);
        $this->assertEquals('ok test', $manager->run('ok test'));
    }
    public function constructorTest($instance)
    {
        $this->assertNotNull($instance);
        return new MyManager($instance);
    }
    
    public function runManager($path)
    {
        return $path;
    }

}

class MyManager extends Manager
{
    
    public function run($path = null) {
        try {
            return call_user_func($this->getRunner($path), $path);
        } catch (\Exception $ex) {
            return __CLASS__;
        }
    }

}

class InstanceWrap implements InstanceInterface {

    public function getInfo() {

    }

    public function getMain() {
        return $this;
    }

    public function getManager() {

    }

    public function getRequest() {

    }

    public function getRoot() {

    }

    public function run() {

    }
}