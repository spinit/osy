<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core\TestUnit;

use PHPUnit\Framework\TestCase;
use Spinit\Osy\Core\ComponentBuilder;

/**
 * Description of ComponentBuilderTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ComponentBuilderTest extends TestCase
{
    /**
     *
     * @var ComponentBuilder
     */
    private $object;
    
    public function setUp()
    {
        $this->object = new ComponentBuilder();
    }
    /**
     * @expectedException \Exception
     */
    public function testGetError()
    {
        $this->object->get('qualcosa');
    }
    
    public function testGet()
    {
        $this->object->setBase('', __NAMESPACE__.'\\Builder');
        $this->object->setBase('tt', __NAMESPACE__);
        
        $this->object->set('test','MyBuilder');
        $this->object->set('prova',__NAMESPACE__.'\\MyBuilder');
        $this->object->set('riprova','MyBuilder', 'tt');
        
        $this->assertEquals(get_class($this->object->get('test')), __NAMESPACE__.'\\Builder\\MyBuilder');
        $this->assertEquals(get_class($this->object->get('prova')), __NAMESPACE__.'\\MyBuilder');
        $this->assertEquals(get_class($this->object->get('riprova')), __NAMESPACE__.'\\MyBuilder');
    }
    
    /**
     * @expectedException \Exception
     */
    public function testNotFound()
    {
        $this->object->setBase('', __NAMESPACE__.'\\Builder');
        
        $this->object->set('test','MyBuilderNotExists');
        $this->object->get('test');
    }
}

class MyBuilder {}
namespace Spinit\Osy\Core\TestUnit\Builder;
class MyBuilder {}
