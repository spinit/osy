<?php
namespace Spinit\Osy\FieldBuilder\Html\Core\TestUnit;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Spinit\Osy\FieldBuilder\Html\Core\Command;
use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;

use PHPUnit\Framework\TestCase;

class CommandTest extends TestCase
{
    public function testCommand()
    {
        $command = new Command();
        $field = new FieldMock(array('name'=>'test', 'label'=>'TEST OK'));
        $actual = $command->build($field);
        $this->assertContains('>TEST OK</a>', $actual->get(false));
    }
}

class FieldMock implements FieldInterface
{
    public function __construct($data) {
        $this->data = $data;
    }
    public function get($name) {
        return Util\arrayGet($this->data, $name);
    }

    public function getChilds() {
        return array();
    }

    public function getName() {
        return $this->get('name');
    }

    public function getValue() {
        return $this->get('value');
    }

    public function setValue($value) {
        $this->data['value'] = $value;
    }

    public function getForm() {
        return $this;
    }
    public function getApplication() {
        return $this;
    }
    public function getManager() {
        return $this;
    }
    public function makePath(){
        return 'ok';
    }
    public function getResponse()
    {
        return new \Spinit\Osy\Http\Response();
    }

}