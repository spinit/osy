<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Adapter\Xml\FormXmlAdapter\UnitTest;

use Spinit\Osy\Adapter\Xml\FormXmlAdapter;
use PHPUnit\Framework\TestCase;

/**
 * Description of FormXmlAdapterTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FormXmlAdapterTest  extends TestCase
{ 
    public function testConstruct()
    {
        $form = new FormXmlAdapter(__DIR__.'/FormXmlAdapterTest.xml');
        $this->assertEquals('CODICE DI TEST', $form->getBuilderCode());
        $this->assertEquals(2, count($form->getFields()));
        $this->assertTrue($form->hasStruct());
    }
    public function testTrigger()
    {
        $form = new FormXmlAdapter(__DIR__.'/FormXmlAdapterTest.xml');
        $this->assertEquals(trim(file_get_contents(__DIR__.'/Trigger/ok-exec.php')), trim($form->getTrigger('ok', 'exec')));
        $this->assertEquals('', $form->getTrigger('ok', 'after'));
    }
}
