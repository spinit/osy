<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Adapter\Xml\FieldXmlAdapter\UnitTest;

use Spinit\Osy\Adapter\Xml\FieldXmlAdapter;
use PHPUnit\Framework\TestCase;

/**
 * Description of FieldXmlAdapterTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FieldXmlAdapterTest extends TestCase
{
    public function testGetChilds()
    {
        $field = new FieldXmlAdapter('<field name="txt_1"><field /><field /></field>');
        $this->assertEquals(2, count($field->getChilds()));
        $this->assertEquals('txt_1', $field->get('name'));
    }
}
