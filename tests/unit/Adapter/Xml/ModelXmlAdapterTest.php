<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy\Adapter\Xml\ModelXmlAdapterUnitTest;
use Spinit\Osy\Adapter\Xml\ModelXmlAdapter;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of ModelXmlAdapterTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelXmlAdapterTest extends \PHPUnit\Framework\TestCase
{
    public function testConstuctorFound()
    {
        $xmodel = new ModelXmlAdapter(__DIR__.'/ModelName/index.xml');
        $this->assertEquals(__DIR__.'/ModelName', $xmodel->getRoot());
        $xmodel = new ModelXmlAdapter(__DIR__.'/ModelXmlAdapterTest.xml');
        $this->assertEquals('', $xmodel->getRoot());
        $xmodel = new ModelXmlAdapter(__DIR__.'/ModelXmlAdapterTest/index.xml');
        $this->assertEquals('', $xmodel->getRoot());
    }
    /**
     * @expectedException Spinit\Util\Error\NotFoundException
     */
    public function testConstuctorNotFound()
    {
        $xmodel = new ModelXmlAdapter(__DIR__.'/ModelNotFound/index.xml');
    }
}
