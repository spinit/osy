<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Test;

use Spinit\Osy\Main;
use PHPUnit\Framework\TestCase;
use Spinit\Osy\Http\ServerRequest;

/**
 * Description of MainTestCase
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class MainTestCase extends TestCase
{
    private $main;
    private $domain = 'www.local.test';
    
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->main = new Main(CONNECTION_STRING);
    }
    
    /**
     * 
     * @return Main
     */
    public function getMain()
    {
        return $this->main;
    }
    
    public function resetDataSource()
    {
        $DS = $this->getMain()->getDataSource()->getAdapter()->getManager();
        $DS->execCommand('drop table if exists osx_ice');
        $DS->execCommand('drop table if exists osx_ice_url');
        $DS->execCommand('drop table if exists osx_app');
        $DS->execCommand('drop table if exists osy_app');
        
    }
    public function getResponse($method, $url, array $headers = array(), $body = null)
    {
        $request = new ServerRequest($method, $url, $headers);
        $request->withParsedBody($body);
        $instance = $this->main->makeInstance($request);
        return $instance->run();
    }
    public function getCommand($url, $command = '', array $body = array(), array $param = array())
    {
        return $this->getResponse('POST', 'http://'.$this->domain.'/'.ltrim($url, '/'), ['X-Opensymap-Command'=>$command], $body);
    }
    
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }
    public function getDomain()
    {
        return $this->domain;
    }
}
