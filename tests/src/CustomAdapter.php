<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Test;

use Spinit\Datasource\Core\AdapterInterface;
use Spinit\Datasource\Core\StructInterface;

/**
 * Description of ArrayDatasetAdapter
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class CustomAdapter implements AdapterInterface
{
    private $callback;
    
    public function __construct($callback)
    {
        $this->callback = $callback;
    }
    public function align(StructInterface $struct) {
        
    }

    public function delete($resource, $key) {
        
    }

    public function insert($resource, $data) {
        
    }
    /**
     * Gestisce 2 protocolli 
     * @param type $query
     * @param type $param
     */
    public function load($query, $param = array())
    {
        return call_user_func($this->callback, $query, $param);
    }

    public function update($resource, $data, $key) {
        
    }

}
