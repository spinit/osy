<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Test;

use Spinit\Datasource\Core\DataSetInterface;

/**
 * Description of DummyDataSet
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DummyDataSet implements DataSetInterface
{
    
    public function close() {
        
    }

    public function current() {
        
    }

    public function isOpen() {
        
    }

    public function key() {
        
    }

    public function next() {
        
    }

    public function position() {
        
    }

    public function rewind() {
        
    }

    public function valid() {
        
    }
    public function getMetadata($type = '')
    {
        return [];
    }
    
    public function getList()
    {
        return [];
    }
}
