<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Test;

use Spinit\Util\Dictionary;

/**
 * Description of DictionaryMock
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DictionaryMock extends Dictionary
{
    public function __call($method, $args)
    {
        if (!is_callable($this->get($method))) {
            return $this;
        }
        return parent::__call($method, $args);
    }
}
