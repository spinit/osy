<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\IntegratedTest\AA001\Install;

use Spinit\Util;
/**
 * Description of A02_ExecInstallTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class A02_ExecInstallTest extends \Spinit\Osy\Test\MainTestCase
{
    public function testInstallError()
    {
        $this->resetDataSource();
        
        $post = ['txt_username'=>''];
        $response = $this->getCommand('/', 'install', $post);
        $this->assertTrue(count($response->get('data.error')) > 0);
        $post = ['txt_username'=>'admin', 'txt_passwd'=>''];
        $response = $this->getCommand('/', 'install', $post);
        $this->assertTrue(count($response->get('data.error')) > 0);
        $post = ['txt_username'=>'admin', 'txt_passwd'=>'admin', 'txt_passwd2'=>'ciao'];
        $response = $this->getCommand('/', 'install', $post);
        $this->assertTrue(count($response->get('data.error')) > 0);
    }
    public function testInstallOk()
    {
        $strcon = explode(':', CONNECTION_STRING);
        
        if (in_array($strcon[0], ['mysql', 'mysqli'])) {
            $strcon[2] .= '_';
        }
        $post = [
            'txt_backend'=>$this->getDomain().'/osy',
            'txt_frontend'=>$this->getDomain(),
            'txt_strcon' => implode(':', $strcon),
            'txt_username'=>'admin',
            'txt_passwd'=>'admin',
            'txt_passwd2'=>'admin',
            'rd_frontend'=>'1'
        ];
        $response = $this->getCommand('/', 'install', $post);
        $fields = array('id_ice', 'url');
        $key = array('url'=>$this->getDomain().'/osy');
        $data = $this->getMain()->getDataSource('main')->find("osx_ice_url", $fields, $key);
        $this->assertEquals($key['url'], $data['url']);
        $this->assertEquals(16, strlen(ltrim($data['id_ice'], '0')));
    }
    
    public function testAuthentication()
    {
        $this->assertEquals('', Util\getenv('USER_LOGIN'));
        $this->assertEquals('', Util\getenv('USER_NAME'));
        $post = [
            'txt_username'=>'admin',
            'txt_password'=>'admin'
        ];
        $response = $this->getCommand('/osy', 'accedi', $post);
        $instance = $this->getMain()->getInstance();
        $pkey = $instance->getDataSource()->first("select hex(id) as id from osy_ang where aut_lgn = 'admin'");
        $this->assertArrayHasKey('href', $response['data']);
        $this->assertEquals($post['txt_username'], Util\getenv('USER_LOGIN'));
        $this->assertEquals($post['txt_username'], Util\getenv('USER_NAME'));
        $this->assertEquals($pkey['id'], Util\getenv('USER_ID'));
    }
    
    public function testUpdateAdminUser()
    {
        $response = $this->getCommand('/osy');
        
        $user = $this->getMain()->getInstance()->getApplication('uri:opensymap:opensymap.org#System')->getModel('Anag');
        $user->trigger('load', Util\getenv('USER_ID'));
        $this->assertEquals('admin', $user->get('aut_lgn'));
        $user->set('aut_lgn', 'root');
        $user->trigger('save');
        $this->assertEquals('root', $user->get('aut_lgn'));
        $user->trigger('load', Util\getenv('USER_ID'));
        $this->assertEquals('root', $user->get('aut_lgn'));
    }
}
