<?php
namespace Spinit\Osy\IntegratedTest\AA001\Install;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use PHPUnit\Framework\TestCase;
use Spinit\Osy\Main;
use Spinit\Util;
use Spinit\Osy\Http\ServerRequest;
use Spinit\Util\Error\StopException;
/**
 * Description of FirstTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class A01_InstallTest extends TestCase
{
    /**
     *
     * @var Main
     */
    private $object;
    
    public function setUp()
    {
        $this->object = new Main(CONNECTION_STRING);
    }
    
    public function testRichiestaPaginaIniziale()
    {
        $this->object->getDataSource()->getAdapter()->getManager()->execCommand('drop table if exists osx_ice');
        $this->object->getDataSource()->getAdapter()->getManager()->execCommand('drop table if exists osx_ice_url');
        $this->object->getDataSource()->getAdapter()->getManager()->execCommand('drop table if exists osx_app');
        $this->object->getDataSource()->getAdapter()->getManager()->execCommand('drop table if exists osy_app');
        $request = new ServerRequest('GET','http://www.demo.test/demo');
        $instance = $this->object->makeInstance($request);
        $this->assertInstanceOf("\\Spinit\\Osy\\Core\\Instance", $instance);
        $response = $instance->run();
        $this->assertInstanceOf("\\Spinit\\Osy\\Http\\ResponseHtml", $response);
        $this->assertEquals("text/html", $response->getHeaderLine('Content-type'));
        $this->assertContains('<!DOCTYPE html>', (string) $response);
    }
    
    public function testPaginaInstallazione()
    {
        $request = new ServerRequest('POST','http://www.demo.test/demo');
        $instance = $this->object->makeInstance($request);
        $response = $instance->run();
        $content = $response->get('data.content');
        $this->assertArrayHasKey('mainContent', $content);
        $this->assertContains('value="www.demo.test/demo/osy"', (string) $content['mainContent']);
        $this->assertContains('value="www.demo.test/demo"', (string) $content['mainContent']);
    }
    
    public function testPaginaInstallazioneProxy()
    {
        $request = new ServerRequest('POST','http://www.demo.test/demo', ['Proxypath'=>'prova']);
        $instance = $this->object->makeInstance($request);
        $response = $instance->run();
        $content = $response->get('data.content');
        $this->assertArrayHasKey('mainContent', $content);
        $this->assertContains('value="www.demo.test/prova/demo/osy"', (string) $content['mainContent']);
        $this->assertContains('value="www.demo.test/prova/demo"', (string) $content['mainContent']);
    }
    
    public function testPaginaCss()
    {
        $request = new ServerRequest('GET','http://www.demo.test/__asset/opensymap/css/desktop.css');
        $instance = $this->object->makeInstance($request);
        $this->expectOutputString(Util\file_get_contents(__OSY__.'/Manager/Backend/asset/css/desktop.css'));
        try {
            $response = $instance->run();
            $this->fail('Contenuto non trovato : desktop.css');
        } catch (StopException $ex) {
            
        }
    }
}
