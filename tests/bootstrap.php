<?php
namespace Spinit\Osy\Test\Bootstrap;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include(dirname(__DIR__). DIRECTORY_SEPARATOR . 'autoload.php');

use Spinit\Datasource\DataSource;
use Spinit\Util;
ob_start();

if(getenv('CONNECTION_STRING_TEST')) {
    define('CONNECTION_STRING', getenv('CONNECTION_STRING_TEST'));
} else {
    define('CONNECTION_STRING', 'sqlite::memory:');
}
echo "\n== CONNECTION STRING [".CONNECTION_STRING."]\n\n";

if (getenv('CONNECTION_INIT')) {
    $ds = new DataSource(CONNECTION_STRING);
    $ds->getAdapter()->getManager()->execCommand(getenv('CONNECTION_INIT'));
    $ds->getAdapter()->getManager()->reset();
    Util\console(getenv('CONNECTION_INIT'));
}

getenv('SPINIT_UTIL_EXECCODE', 'ERROR-NO-DELETE');
/*
class Bootstrap
{
    private static $self;
    
    public static function init()
    {
        if (!self::$self) {
            self::$self = new Bootstrap();
        }
    }
    
    private function __construct()
    {
        ob_start();
        $ini = new Util\Dictionary();
        $ini['dsClass'] = function($ds, $query, $param) {
            $adapter = new $ds;
            return $adapter->load($query, $param);
        };
        $ini['dsKey'] = function($ds, $query, $param) {
            $data = Util\j64u($ds);
            $fkey = array_shift(array_keys($param));
            foreach($data as $record) {
                if (Util\arrayGet($record, $fkey) == $param[$fkey]) {
                    return array($record);
                }
            }
            // se non è stato trovato il record ... viene restituito tutto il recordset
            return $data;
        };
        DataSource::registerAdapter(
            array_keys($ini->asArray()),
            function($conf) use ($ini) {
                $arConf = explode(':', $conf);
                return new \Spinit\Osy\Test\CustomAdapter(function($query, $param) use ($ini, $arConf)
                {
                    return $ini->{$arConf[0]}($arConf[1], $query, $param);
                });
            }
        );
    }
}

Bootstrap::init();

namespace Spinit\Util;

function j64($data)
{
    return base64_encode(json_encode($data));
}
function j64u($data)
{
    return json_decode(base64_decode($data), 1);
}

*/