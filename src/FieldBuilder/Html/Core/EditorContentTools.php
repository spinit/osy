<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy\FieldBuilder\Html\Core;

use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;

/**
 * Description of InputText
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class EditorContentTools extends Component
{
    public function makeContent(FieldInterface $field = null, $event = null)
    {
        $response = $field->getForm()->getResponse();
        $manager = $field->getForm()->getApplication()->getManager();
        $response->trigger('addcss', $manager->makePath('Component/Core/EditorContentTools/content-tools.min.css'));
        $response->trigger('addcss', $manager->makePath('Component/Core/EditorContentTools/editor.css'));
        $response->trigger('addjs', $manager->makePath('Component/Core/EditorContentTools/content-tools.min.js'));
        $response->trigger('addjs', $manager->makePath('Component/Core/EditorContentTools/editor.js'));
        $cnt = Util\Tag::create('div');
        $ect = $cnt->add(Util\Tag::create('div'))
            ->prp('data-editable')
            ->att('data-name', $field->getName())
            ->att('class', $field->get('class'));
        if ($tag = $field->get('tag')) {
            $ect = $ect->add(Util\Tag::create($tag));
        }
        $ect->add($field->getValue());
        $cnt->add(Util\Tag::create('textarea'))
            ->att('style','display:none')
            ->att('name', $field->getName())
            ->att('id', 'ECT_'.$field->getName())
            ->add($field->getValue());
        return $cnt;
    }
}
