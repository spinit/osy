<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\FieldBuilder\Html\Core;

use Spinit\Osy\Type\FieldInterface;
use Spinit\Util\Tag;
use Spinit\Util\Table;
use Spinit\Util;
/**
 * Description of ComboDataRange
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ComboSearch extends Component
{
    
    public function __construct() {
        $this->bindExec($this->getEventID('open'), array($this, 'onOpen'));
    }
    public function makeContent(FieldInterface $field, $event = null)
    {
        $root = Tag::create('div')->att('class', 'osy-combosearch')
            ->att('data-open', $this->getEventID('open'));
        $div = $root->add(Tag::create('div'))->att('class', 'view input');
        $response = $field->getForm()->getResponse();
        $app = $field->getForm()->getApplication();
        $manager = $app->getManager();
        if (!$event) {
            $response->trigger('addjs', $manager->makePath( 'Component/Core/ComboSearch/ComboSearchScript.js'));
            $response->trigger('addcss', $manager->makePath('Component/Core/ComboSearch/ComboSearchStyle.css'));
        }
        $values  = $field->getValue();
        $div->add(Tag::create('span'))->add('<i class="fa fa-search ico-search"></i>');
        $div->add(Tag::create('span'))->add('<i class="fa fa-ellipsis-h ico-dot "></i>');
        $from = $div->add(Tag::create('input'))
            ->att('name', $field->getName().'[id]')
            ->att('type', 'hidden')
            ->att('class', 'search-id')
            ->att('value', Util\arrayGet($values, 'id'));
        $to = $div->add(Tag::create('input'))
            ->att('name', $field->getName().'[name]')
            ->att('type', 'text')
            ->att('autocomplete', 'off')
            ->att('class', 'search-name')
            ->att('value', Util\arrayGet($values, 'name'));
        return $root;
    }
    public function onOpen($event, $target)
    {
        $values  = $target->getValue();
        $response = $target->getForm()->getResponse();
        $response['type'] = 'content';
        $response['data'] = $this->makeData($target, $values);
    }
    
    private function makeData($field, $values)
    {
        if ($field->get('options')) {
            return $this->makeOption($field->get('options'));
        }
        if ($field->get('datasource')) {
            return $this->makeDatasource($field);
        }
        return $this->makeEmpty();
    }
    
    private function makeEmpty()
    {
        return '<div class="empty"> Nessun elemento trovato </div>';
    }
    
    private function makeOption($options)
    {
        $data = array();
        foreach($options['item'] as $item) {
            $data = ['id' => md5($item['id']), 'name' => $item['name']];
        }
        return $this->make($data);
    }
    private function makeDatasource($field)
    {
        $query = Util\arrayGet($field->get('datasource'), 'query');
        if ($adapter = Util\arrayGet($field->get('datasource'), 'adapter')) {
            $ds = new DataSource();
            $ds->setAdapter($app->makeObject($adapter));
        } else {
            $source = Util\ArrayGet($query, 'source');
            $ds = $field->getForm()->getDataSource($source);
        }
        try {
            //$div->add('<div>'.json_encode($ds->getAdapter()->getManager()->getInfo()).'</div>');
            // esecuzione della query passandogli come parametri i valori presenti nel form
            $data = $ds->load($query, $field->getForm()->getValues(), $field->getValue());
            return $this->make($data, $field);
        } catch (\Exception $e) {
            return "<pre>".$e->getMessage()."</pre>";
        }        
    }
    
    private function make($data)
    {
        $div = new Tag('div');
        $found = false;
        foreach($data as $item) {
            $found = true;
            $key = array_shift($item);
            $dsc = array_shift($item);
            $div->add(new Tag('div'))
            ->att('class', 'option-row')
            ->att('data-id', $key)
            ->att('data-name', $dsc)
            ->add($dsc);
        }
        if (!$found) {
            $div->add('<div class="empty"> Nessun elemento trovato </div>');
        }
        return $div;
    }
}
