<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy\FieldBuilder\Html\Core;

use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;
use Spinit\Datasource\DataSource;
use Spinit\Osy\Http\ServerRequest;
use Spinit\Util\Dictionary;

/**
 * Description of InputText
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ComboBox extends Component
{
    public function __construct()
    {
    }
    public function makeContent(FieldInterface $field, $event = null)
    {
        $response = $field->getForm()->getResponse();
        $app = $field->getForm()->getApplication();
        $manager = $app->getManager();
        //$response->trigger('addjs', $manager->makePath( 'Component/Core/ComboBox/ComboBox.js'));
        //$response->trigger('addcss', $manager->makePath('Component/Core/ComboBox/ComboBox.css'));
        $this->content = Util\Tag::create('');
        $div = $this->content->add(Util\Tag::create('div')->att('class','osy-combobox'));
        $query = Util\arrayGet($field->get('datasource'), 'query');
        if ($adapter = Util\arrayGet($field->get('datasource'), 'adapter')) {
            $ds = new DataSource();
            $ds->setAdapter($app->makeObject($adapter));
        } else {
            $source = Util\ArrayGet($query, 'source');
            $ds = $field->getForm()->getDataSource($source);
        }
        try {
            //$div->add('<div>'.json_encode($ds->getAdapter()->getManager()->getInfo()).'</div>');
            // esecuzione della query passandogli come parametri i valori presenti nel form
            $data = $ds->load($query, $field->getForm()->getValues());
            // TODO : gestire il debug in modo generico sul datasorce
            if( $ds->getAdapter() and $ds->getAdapter()->getManager()) {
                $manager = $ds->getAdapter()->getManager();
                if (isset($manager->sql)) {
                    $div->add("<!--\n\n".print_r($manager->sql, 1)."\n\n-->");
                }
            }
            $div->add($this->make($data, $field));
        } catch (\Exception $e) {
            $div->add("<pre>".$e->getMessage()."</pre>");
        }        
        return $this->content;
    }
    
    private function make($data, $field)
    {
        if ($field->get('readonly')) {
            $descr = $field->getValue() ? '-- Not Found --' : '';
            foreach($data as $option) {
                $key = array_shift($option);
                if ($field->getValue() == $key) {
                    $descr = Util\nvl(array_shift($option), $key);
                    break;
                }
            }
            $div = new Util\Tag('div');
            $div->Add(new Util\Tag('input'))
                ->att('type', 'hidden')
                ->att('value', $field->getValue())
                ->att('name', $field->get('name'));
            $div->Add(new Util\Tag('div'))
                ->Add('&nbsp;'.$descr);
            return $div->att('class', 'input const');
        }
        // chiave dell'insieme dei record
        $select = new Util\Tag('select');
        $select->att('class', 'selectpicker');
        $select->att('name', $field->get('name'));
        if (!$field->get('notNull')) {
            $select->add(new Util\Tag('option'))->att('value', '')->add('-- select --');
        }
        $grp = '';
        $base = $select;
        foreach($data as $option) {
            $select->add('<!-- '.json_encode($option).' -->');
            $key = array_shift($option);
            $val = array_shift($option);
            $ngrp = array_shift($option);
            if ($grp != $ngrp) {
                $grp = $ngrp;
                $base = $select->add(new Util\Tag('optgroup'))->att('label', $ngrp);
            }
            $opt = $base->add(new Util\Tag('option'))->att('value', $key);
            $opt->add(Util\nvl($val, $key));
            if ($field->getValue() == $key) {
                $opt->att('selected', 'selected');
            }
        }
        return $select;
    }
}
