<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy\FieldBuilder\Html\Core;

use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;

/**
 * Description of InputText
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Command extends Component
{
    public function hasLabel()
    {
        return false;
    }
    public function makeContent(FieldInterface $field, $event = null)
    {
        $response = $field->getForm()->getResponse();
        $manager = $field->getForm()->getApplication()->getManager();
        $response->trigger('addjs', $manager->makePath('Component/Core/Command/CommandScript.js'));
        $response->trigger('addcss', $manager->makePath('Component/Core/Command/CommandStyle.css'));
        $div = Util\Tag::create('div')->att('class','osy-command');
        
        $class = Util\nvl($field->get('class'),'success');
        $div->att('class', $class, ' ');
        $div->add(Util\Tag::create('a'))
            ->att('class', 'btn btn-'.$class)
            ->att('href', '')
            ->att('command', $field->get('command'))
            ->att('data-param', json_encode($field->get('param')))
            ->add(Util\nvl($field->get('label'),' [-- Label non impostata --]'));
        
        
        return $div;
    }
}
