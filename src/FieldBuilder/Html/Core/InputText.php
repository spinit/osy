<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy\FieldBuilder\Html\Core;

use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;

/**
 * Description of InputText
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class InputText extends Component
{
    public function makeContent(FieldInterface $field, $event = null)
    {
        $type = Util\nvl($field->get('type'),'text');
        $tag = Util\Tag::create('input')
            ->att('type',$type)
            ->att('class', 'recorder')
            ->att('name',$field->get('name'));
        switch($type) {
            case 'radio':
            case 'checkbox':
                $tag->att('value', $field->get('value')?:'1');
                if ($tag->value == $field->getValue()) {
                    $tag->att('checked','checked');
                }
                break;
            default:
                $tag->att('value', Util\nvl($field->getValue(), $field->get('value')));
                break;
        }
        if ($field->get('readonly')) {
            $tag->att('type', 'hidden');
            $cnt = new Util\Tag('div');
            $cnt->att('class','input const')->add($tag);
            $cnt->add(new Util\Tag('div'))->add('&nbsp;'.$field->getValue());
            $tag = $cnt;
        }
        return $tag;
    }
    protected function getCell($field) {
        if ($field->get('type') == 'hidden') {
            return Util\Tag::create('');
        }
        return parent::getCell($field);
    }
}
