<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\FieldBuilder\Html\Core;

use Spinit\Osy\Type\FieldBuilderInterface;
use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;

/**
 * Description of Component
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class Component implements FieldBuilderInterface
{
    use Util\TriggerTrait;
    
    public function hasLabel()
    {
        return true;
    }
    public function build(FieldInterface $field, $event = null)
    {
        $div = $this->getCell($field);
        $content = $this->makeContent($field, $event);
        if ($event) {
            return $content;
        }
        $div->Add(Util\Tag::create('div'))->att('class', 'content')->add($content);
        return $div;
    }
    protected function getCell($field)
    {
        $levels = array('col-md-');
        $div = Util\Tag::create('div',$field->getName());
        $div->att('class', 'osy-component');
        $this->title = $div->add(Util\Tag::create(''));
        if ($this->hasLabel()) {
            $label = $this->title->add(Util\Tag::create('label'));
            $label->att('for', $field->get('name'));
            $label->add($field->get('label'));
        }
        $width = Util\asArray($field->get('width'), ' ');
        foreach($levels as $k=>$level) {
            $div->att('class', $level.Util\arrayGet($width, $k, '12'), ' ');
        }
        return $div;        
    }
    protected function getTitle()
    {
        return $this->title;
    }
    public function getEventID($event)
    {
        return md5(get_class($this).'@event:'.$event);
    }
    abstract public function makeContent(FieldInterface $field, $event = null);
    
    // permette di depurare i valori di un campo da quelli inseriti dal builder
    public function getValue(FieldInterface $field)
    {
        return $field->getValue();
    }
}
