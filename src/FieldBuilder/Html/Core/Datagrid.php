<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy\FieldBuilder\Html\Core;

use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;
use Spinit\Datasource\DataSource;
use Spinit\Osy\Http\ServerRequest;
use Spinit\Util\Dictionary;
use Spinit\Osy\FieldBuilder\Html\Core\ComboDataRange;

/**
 * Description of InputText
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Datagrid extends Component
{
    /**
     *
     * @var ComboDataRange
     */
    private $datarange;
    
    public function __construct()
    {
        $this->datarange = new ComboDataRange();
        $this->bindExec('open', array($this, 'onOpen'));
        $this->bindExec('add', array($this, 'onAdd'));
        $this->bindExec($this->datarange->getEventID('open'), array($this, 'onOpenData'));
    }
    public function makeContent(FieldInterface $field, $event = null)
    {
        $response = $field->getForm()->getResponse();
        $app = $field->getForm()->getApplication();
        $manager = $app->getManager();
        $div = Util\Tag::create('div')->att('class','osy-datagrid');
        $div->att('onscroll', "$(this).trigger('osy-scroll')");
        if ($h = Util\arrayGet($field->get('heightMax'),0)) {
            $div->att('style', 'max-height: '.$h.';');
        }
        $query = Util\arrayGet($field->get('datasource'), 'query');
        if ($adapter = Util\arrayGet($field->get('datasource'), 'adapter')) {
            $ds = new DataSource();
            $ds->setAdapter($app->makeObject($adapter));
        } else {
            $source = Util\ArrayGet($query, 'source');
            $ds = $field->getForm()->getDataSource($source);
        }
        try {
            //$div->add('<div>'.json_encode($ds->getAdapter()->getManager()->getInfo()).'</div>');
            // esecuzione della query passandogli come parametri i valori presenti nel form
            $data = $ds->load($query, $field->getForm()->getValues(), $field->getValue());
            $columns = $this->getColumns($query, $data->getMetadata());
            if (!$event) {
                $response->trigger('addjs', $manager->makePath( 'Component/Core/Datagrid/DatagridScript.js'));
                $response->trigger('addcss', $manager->makePath('Component/Core/Datagrid/DatagridStyle.css'));
                $this->makeCommand($field, $this->getTitle(), $columns);
            }
            // TODO : gestire il debug in modo generico sul datasorce
            if( $ds->getAdapter() and $ds->getAdapter()->getManager()) {
                $manager = $ds->getAdapter()->getManager();
                if (isset($manager->sql)) {
                    $div->add("<!--query\n\n".print_r($manager->sql, 1)."\n\n-->");
                }
            }
            $table = $this->make($data, $field, $columns);
            $div->add($table);
        } catch (\Exception $e) {
            $div->add("<pre>".$e->getMessage()."</pre>");
        }        
        return $div;
    }
    private function getColumns($query, $default)
    {
        if ($columns = Util\arrayGet($query, 'columns', null)) {
            $list = [];
            foreach($columns as $nme=>$conf) {
                $list[$nme] = array_merge(['name' =>$nme], $conf);
            }
            return $list;
        }
        return $default;
    }
    private function format($data, $conf)
    {
        switch(strtolower(Util\arrayGet($conf, 'type'))) {
            case 'datetime':
            case 'date':
                $date = explode(' ', $data);
                $tick = explode('/', $date[0]);
                if (count($tick)<3) {
                    $tick = explode('-', $date[0]);
                    $tick = array_reverse($tick);
                    $date[0] = implode('/', $tick);
                }
                return '<code class="date" title="'.implode(' ',$date).'">'.$date[0].'</code>';
        }
        
        if (Util\is_binary($data)) {
            $data = strtoupper(bin2hex($data));
        }
        if (Util\arrayGet($conf, 'apply')) {
            $data = call_user_func($conf['apply'], $data);
        }
        return $data;
    }
    private function make($data, $field, $columns)
    {
        // chiave dell'insieme dei record
        $pkey = explode(',', Util\arrayGet($data->current(), '__pkey', 'id'));
        $wrapper = new Util\Tag('div');
       
        $main = $wrapper->add(new Util\Tag('table'));
        $main->att('width', '100%');
        $this->makeHead($field, $main, $columns, $pkey);
        $this->makeBody($field, $main, $data, $columns, $pkey);
        return $wrapper;
    }
    private function makeHead($field, $main, $columns, $pkey)
    {
        $tr = $main->add(new Util\Tag('thead'))->add(new Util\Tag('tr'));
        if ($field->get('field')) {
            $tr->Add(new Util\Tag('th'))->add('<div><input type="checkbox" class="groupSet"/></div>');
        } 
        foreach($columns as $col) {
            $name = Util\arrayGet($col, 'name');
            if (!in_array($name, $pkey)) {
                $cell = $tr->Add(new Util\Tag('th'));
                $cell->add('<div>'.Util\arrayGet($col, 'label', $name).'</div>');
            }
        }
    }
    private function makeBody($field, $main, $data, $columns, $pkey)
    {
        $list = $field->getValue() ?: [];
        $main->add("<!-- ".json_encode($list, JSON_PRETTY_PRINT)." -->");
        $body = $main->add(new Util\Tag('tbody'));
        $sel = new Util\Tag('');
        
        foreach($data as $row) {
            $tr = $body->add(new Util\Tag('tr'))->att('class','dataRow');
            $tr->add('<!-- '.json_encode($row).' -->');
            $vkey = [];
            if ($field->get('field')) {
                $sel = $tr->Add(new Util\Tag('th'))->add(new Util\Tag('input'))
                    ->att('name',$field->get('name').'[sel][]')
                    ->att('type', 'checkbox')
                    ->att('class', 'groupItem');
            } 
            // campo singolo della chiave
            $fkey = null;
            foreach($columns as $fieldName => $col) {
                if (!in_array($fieldName, $pkey)) {
                    $cell = $tr->Add(new Util\Tag('td'));
                    $cell->add('<div class="cell">&nbsp;'.trim($this->format($row[$fieldName], $col)).'</div>');
                    if (Util\arrayGet($row, '__color')) {
                        $cell->att('style', 'color: '.Util\arrayGet($row, '__color').';', ' ');
                    }
                    if (Util\arrayGet($row, '__bgcolor')) {
                        $cell->att('style', 'background-color: '.Util\arrayGet($row, '__bgcolor').';', ' ');
                    }
                } else {
                    $vkey[$fieldName] = $fkey = $this->format($row[$fieldName], $col);
                }
            }
            ksort($vkey);
            $attkey = json_encode($vkey);
            $valkey = (count($vkey) == 1) ? $fkey : $attkey;
            if($field->get('formDetail')) {
                $tr->att('data-pkey', $attkey);
            }
            $sel->att('value', $valkey);
            if (in_array($valkey, $list)) {
                $sel->att('checked', 'checked');
            }
            $info = new \stdClass();
            $attinfo = json_encode($info);
            $tr->att('data-info', $attinfo);
        }
    }
    private function makeCommand($field, $wrapper, $columns)
    {
        $hasDate = false;
        foreach($columns?:[] as $conf) {
            if( in_array(strtolower(Util\arrayGet($conf, 'type')),['datetime', 'date'])) {
                $hasDate = true;
                break;
            }
        }
        $cnt = $wrapper->Add(Util\Tag::create('div'))
                ->att('class','osy-datagrid-src osy-datagrid-cmd');
        $cnt->add('<i class="fa fa-search" style="position:absolute; margin:6px;"></i>');
        $cnt->add(Util\Tag::create('input'))->att('name', $field->getName().'[q]')->att('class', 'input')->att('style','padding-left:25px;');
        if ($hasDate) {
            $wrapper->add($this->datarange->makeContent($field))->att('class','osy-datagrid-src',' ');
        }
        if($field->get('formDetail')) {
            $cnt = $wrapper->Add(Util\Tag::create('div'))
                    ->att('class','osy-datagrid-cmd');
            $cnt->Add(Util\Tag::create('a'))
                ->att('class','btn btn-primary')
                ->att('command','add')
                ->Add('+');
        }
    }
    
    public function onOpen($event, $target){
        $detailName = $target->get('formDetail');
        if (!$detailName) {
            return;
        }
        $target->getForm()->trigger('save');
        $detail = array_shift($detailName);
        $form = $target->getForm();
        $post = new Dictionary(Util\arrayGet($form->getRequest()->getParsedBody(), '_'));
        $app = $form->getApplication();
        $pkey = '';
        if ($post->get('osy.pkey')) {
            $pkey = base64_encode($post->get('osy.pkey'));
        }
        $urlDetail = $app->getInstance()->makePath($form->getApplication()->getLabel().'/'.$detail.'/'. $pkey);
        $formParam = json_decode(Util\arrayGet($target->get('formParam')?:array(), 0), 1) ?: array();
        $param = array();
        foreach($formParam as $field=>$name) {
            $param[$name] = $target->getForm()->getField($field)->getValue();
        }
        $request = new ServerRequest('POST', $urlDetail);
        $request->withParsedBody($param);
        $response = $app->getForm($detail)->run($request, [$pkey]);
        
        $response->addCommand("window.history.pushState(arguments[2], arguments[0], arguments[1])", $detail, $urlDetail, array('post'=>$param));
        $form->setResponse($response);
    }
    public function onAdd($event, $target){
        $this->onOpen($event, $target);
    }
    public function getValue(FieldInterface $field)
    {
        return Util\arrayGet($field->getValue(), 'sel');
    }
    
    public function onOpenData($event, $target)
    {
        $this->datarange->onOpen($event, $target);
    }
}
