<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy\FieldBuilder\Html\Core;

use Spinit\Osy\Type\FieldBuilderInterface;
use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;

/**
 * Description of InputText
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Panel implements FieldBuilderInterface
{
    use Util\TriggerTrait;
    
    public function build(FieldInterface $field, $event = null)
    {
        $levels = array('col-md-');
        $div = Util\Tag::create('div');
        $form = Util\nvl($field->get('form'), 'row');
        $div->class="osy-panel form-{$form}";
        if ($label = $field->get('label')) {
            $div->Add(Util\Tag::create('label'))->Add($label);
        }
        $width = Util\asArray($field->get('width'), ' ');
        foreach($levels as $k=>$level) {
            $div->att('class', $level.Util\arrayGet($width, $k, '12'), ' ');
        }
        foreach($field->getChilds() as $child) {
            $div->add($child->build());
        }
        return $div;
    }

    public function getValue(FieldInterface $field)
    {
        return $field->getValue();
    }

}
