<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\FieldBuilder\Html\Core;

use Spinit\Osy\Type\FieldInterface;
use Spinit\Util\Tag;
use Spinit\Util\Table;
use Spinit\Util;
/**
 * Description of ComboDataRange
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ComboDataRange extends Component
{
    
    public function __construct() {
        $this->bindExec($this->getEventID('open'), array($this, 'onOpen'));
    }
    public function makeContent(FieldInterface $field, $event = null)
    {
        $root = Tag::create('div')->att('class', 'osy-combodatarange')
            ->att('data-open', $this->getEventID('open'));
        $div = $root->add(Tag::create('div'))->att('class', 'view input');
        $response = $field->getForm()->getResponse();
        $app = $field->getForm()->getApplication();
        $manager = $app->getManager();
        if (!$event) {
            $response->trigger('addjs', $manager->makePath( 'Component/Core/ComboDataRange/ComboDataRangeScript.js'));
            $response->trigger('addcss', $manager->makePath('Component/Core/ComboDataRange/ComboDataRangeStyle.css'));
        }
        $values  = $field->getValue();
        $from = $div->add(Tag::create('input'))
            ->att('name', $field->getName().'[from]')
            ->att('type', 'hidden')
            ->att('class', 'date-from')
            ->att('value', Util\arrayGet($values, 'from'));
        $to = $div->add(Tag::create('input'))
            ->att('name', $field->getName().'[to]')
            ->att('type', 'hidden')
            ->att('class', 'date-to')
            ->att('value', Util\arrayGet($values, 'to'));
        $month = $div->add(Tag::create('input'))
            ->att('name', $field->getName().'[month]')
            ->att('type', 'hidden')
            ->att('class', 'month')
            ->att('value', Util\arrayGet($values, 'month'));
        $cnt = $div->add(Tag::create('code'));
        $cnt->add(Tag::create('span'))->add('<i class="fa fa-calendar"></i>');
        $cnt->add(Tag::create('span'))->att('class', 'view-from')->add($this->viewDate($from->value));
        $cnt->add(Tag::create('span'))->att('class', 'view-to')->add($this->viewDate($to->value));
        return $root;
    }
    private function viewDate($str)
    {
        $dat = explode('-', trim($str));
        array_reverse($dat);
        return implode('/', $dat);
    }
    public function onOpen($event, $target)
    {
        $values  = $target->getValue();
        $this->month = explode('-', Util\nvl(Util\arrayGet($values, 'month'), Util\arrayGet($values, 'from'), date('Y-m-d')));
        $this->min = $target->get('min');
        $this->max = $target->get('max');
        $response = $target->getForm()->getResponse();
        $response['type'] = 'content';
        $response['data'] = $this->makeCalendar();
    }
    
    private function makeCalendar()
    {
        $cnt = new Tag('div');
        $table = $cnt->att('class','calendar')->add(new Table());
        $this->writeHead($table->Cell(new Tag('div')));
        $table->Row();
        $this->writeBody($table->Cell(new Tag('div')));
        return $cnt;
    }
    
    private function writeHead($content)
    {
        $premonth = date('Y-m',mktime(0,0,0,$this->month[1]-1, 1, $this->month[0]));
        $sucmonth = date('Y-m',mktime(0,0,0,$this->month[1]+1, 1, $this->month[0]));
        $ll = $content->Add(new Tag('div'))->att('style','text-align:center');
        $ll->add(new Tag('div'))->att('style','float:left; cursor:pointer; font-size:150%; padding:2px 5px; margin-top:-5px;')->Att('onclick','$(this).trigger("setmonth",["'.$premonth.'"])')->add('&laquo;');
        $ll->add(new Tag('div'))->att('style','float:right; cursor:pointer; font-size:150%; padding:2px 5px; margin-top:-5px;')->Att('onclick','$(this).trigger("setmonth",["'.$sucmonth.'"])')->add('&raquo;');
        
        $mon = array(1=>'Gen', 'Feb', 'Mar', 'Apr','May', 'Giu','Jul', 'Aug', 'Sep', 'Opt', 'Nov', 'Dec');
        $ll->add(new Tag('span'))->add($mon[intval($this->month[1])].' '.$this->month[0]);
    }
    
    private function writeBody($content)
    {
        $cal = $content->add(new Table());
        $day = array('L','M','M', 'G', 'V', 'S', 'D');
        foreach($day as $d) {
            $cal->head($d)->att('class','week');
        }
        $cal->Row();
        $w = date('w', mktime(0,0,0,$this->month[1], 1, $this->month[0]));
        $end = date('d', mktime(0,0,0,$this->month[1]+1, 0, $this->month[0]));
        if (!$w) {
            $w = 7;
        }
        $w -= 1;
        // intestazione
        for($i=0; $i<$w; $i++) {
            $cal->cell('');
        }
        for($i=1; $i<=7-$w; $i++) {
            $cal->cell($this->makeDay($i));
        }
        while($i<$end) {
            $cal->Row();
            for($j=0; $j<7; $j++, $i++) {
                $cal->cell( $i <= $end ? $this->makeDay($i) : '');
            }
        }
        
    }
    
    private function makeDay($i)
    {
        $day = (new Tag('div'))->att('class','day');
        $day->add($i);
        $date = date('Y-m-d', mktime(0,0,0,$this->month[1], $i, $this->month[0]));
        $wdate = date('d/m/Y', mktime(0,0,0,$this->month[1], $i, $this->month[0]));
        if ($this->min and $this->min > $date) {
            $day->attAp('class', ' disabled');
            return $day;
        }
        if ($this->max and $this->max < $date) {
            $day->attAp('class', ' disabled');
            return $day;
        }
        $day->att('onclick', "$(this).trigger('setdate',['".$date."'])");
        return $day;
    }
}
