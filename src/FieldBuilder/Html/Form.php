<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\FieldBuilder\Html;

use Spinit\Util\Tag;
use Spinit\Osy\Adapter\Helper\FieldHelperAdapter;

/**
 * Description of Form
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Form
{
    private $box;
    
    public function __construct()
    {
        $this->box = new Tag('');
    }
    public function add($content)
    {
        $this->box->add($content);
    }
    public function build($form)
    {
        $box = $this->box->add(new Tag('div'));
        $this->makeCommand($form, $this->box->add(new Tag('div', 'mainCommand')));
        
        return $this->box;
    }
    
    private function hasCommand($form)
    {
        return $form->getAdapter()->get('save') or $form->getAdapter()->get('delete');
    }
    
    private function makeCommand($form, $cmd)
    {
        $cmpBuilder = new Core\Command();
        if ($form->getAdapter()->get('model')) {
            $field = new FieldHelperAdapter($form, array('command'=>'save', 'label'=>'Save', 'param'=>'back'));
            $cmd->add($cmpBuilder->build($field));
            
            if ($form->getPkey()) {
                $field = new FieldHelperAdapter($form, ['label'=>'Delete', 'class'=>'danger delete']);
                $cmd->add($cmpBuilder->build($field))->att('onclick',"$('#deleteModal').modal()");
            }
        }
    }
}
