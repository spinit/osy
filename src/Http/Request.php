<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Http;

use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Psr\Http\Message\UriInterface;
use GuzzleHttp\Psr7\Uri;

/**
 * Description of Request
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Request
{
    /**
     * @var GuzzleRequest
     */
    private $request;
    
    public function __construct($method, $uri, array $headers = array(), $body = null, $version = '1.1') {
        $this->request = new GuzzleRequest($method, $uri, $headers, $body, $version);
    }
    
    /**
     * Le chiamate vengono rigirate alla richiesta guzzle. Se esse modificano la richiesta (e quindi generano un nuovo oggetto)
     * allora il risultato diventa la richiesta corrente.
     * @param type $name
     * @param type $arguments
     * @return type
     */
    public function __call($name, $arguments) {
        $result = call_user_func_array(array($this->request, $name), $arguments);
        if (substr($name, 0, strlen('width')) == 'width') {
            $this->request = $result;
        }
        return $result;
    }
    public function getRequest()
    {
        return $this->request;
    }
    public function __toString() {
        return (string) $this->getUri();
    }
}
