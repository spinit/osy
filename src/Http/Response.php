<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Http;

use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Psr\Http\Message\StreamInterface;
use Spinit\Util;
/**
 * Description of Response
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Response extends Util\Dictionary
{
    /**
     *
     * @var type 
     */
    private $converter = null;

    /**
     *
     * @var GuzzleResponse
     */
    private $response;
    
    public function __construct($body = array(), array $headers = array())
    {
        if (!is_array($body)) {
            $body = array('data' => $body);
        }
        $body['status'] = isset($body['status']) ? $body['status'] : 'success';
        $body['message'] = isset($body['message']) ? $body['message'] : '';
        $body['data'] = isset($body['data']) ? $body['data'] : array();
        
        parent::__construct($body);
        
        $this->response = new GuzzleResponse(200, $headers);
        
        $this->setHeader('Content-Type', 'application/json');
        $this->setConverter(function($data, $response) {
            return \json_encode($data, JSON_PRETTY_PRINT);
        });
        
        $this->bindExec('addjs', function($event) {
            $src = $event->getParam(0);
            $this['data']['js'] = Util\ArrayGet($this['data'], 'js', array());
            if (!in_array($src, $this['data']['js'])) {
                $this['data']['js'][] = $src;
            }
        });
        $this->bindExec('addcss', function($event) {
            $src = $event->getParam(0);
            $this['data']['css'] = Util\ArrayGet($this['data'], 'css', array());
            if (!in_array($src, $this['data']['css'])) {
                $this['data']['css'][] = $src;
            }
        });
}
    public function setConverter($converter)
    {
        $this->converter = $converter;
    }
    public function getConverter()
    {
        return $this->converter;
    }
    public function setHeader($name, $value)
    {
        $this->response = $this->response->withHeader($name, $value);
    }
    public function addData()
    {
        $args = func_get_args();
        $type = array_shift($args);
        if (count($args)>0) {
            if (count($args) < 2) {
                $this['data'][$type][] = array_shift($args);
            } else {
                $index = array_shift($args);
                $this['data'][$type][$index] = array_shift($args);
            }
        }
        return $this;
    }
    public function setData()
    {
        $args = func_get_args();
        $type = array_shift($args);
        if (count($args)>0) {
            if (count($args) < 2) {
                $this['data'][$type] = array_shift($args);
            } else {
                $this['data'][$type]= $args;
            }
        }
        return $this;
    }
    public function addCommand()
    {
        return $this->addData('command', func_get_args());
    }
    public function addError()
    {
        return $this->addData('error', func_get_args());
    }
    public function hasError()
    {
        return count($this->get('data.error'));
    }
    public function addContent()
    {
        $args = func_get_args();
        array_unshift($args, 'content');
        return call_user_func_array(array($this,'addData'), $args);
    }
    
    /**
     * Permette di escludere le chiamate a header facendo l'overloading
     * @param string $header
     * @codeCoverageIgnore
     */
    protected function sendHeader($header)
    {
        header($header);
    }
    protected function getContents()
    {
        return call_user_func($this->getConverter(), $this, $this->response);
    }
    public function __toString()
    {
        try {
            $contents = $this->getContents();
            // Emit headers iteratively:
            $this->sendHeader('HTTP/'.
                $this->getProtocolVersion().' '.
                $this->getStatusCode().' '.
                $this->getReasonPhrase()
            );
            foreach ($this->getHeaders() as $name => $values) {
                foreach ($values as $value) {
                    $this->sendHeader(sprintf('%s: %s', $name, $value), false);
                }
            }
        } catch (\Exception $e) {
            $contents = $e->getMessage();
        }
        return $contents ?: '';
    }
    public function __call($name, $arguments)
    {
        return call_user_func_array(array($this->response, $name), $arguments);
    }
}
