<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Http;

use Spinit\Util;

/**
 * Description of ResponseHtml
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ResponseHtml extends Response
{
    private $template;
    
    public function __construct($headers = array())
    {
        parent::__construct(array(), $headers);
        $this->template = '<!--content-->';
        $this->setHeader('Content-Type', 'text/html');
        $this->setConverter(function($data, $response) {
            return $this->normalize($data);
        });
        $this->bindExec('setTemplate', function($event) {
            $this->template = $event->getParam(0);
        });
    }
    
    private function normalize($data)
    {
        $template = $this->template;
        preg_match_all('/<!--([^ ]+)-->/', $template, $matches);
        foreach ($matches[1] as $k => $field) {
            if ($field{0} == '.') {
                $field = ltrim($field, '.');
            } else {
                $field = 'data.'.$field;
            }
            $value = $data->get($field);
            $content = '';
            if (is_array($value)) {
                foreach($value as $v) {
                    if (is_array($v)) {
                        $content .= json_encode($v);
                    } else {
                        $content .= ($v instanceof Response) ? '' : $v;
                    }
                }
            } else {
                $content = $value;
            }
            $template = str_replace($matches[0][$k], $content, $template);
        }
        return $template;
    }
}
