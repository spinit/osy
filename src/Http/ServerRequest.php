<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Http;

use GuzzleHttp\Psr7\ServerRequest as GuzzleRequest;
use Psr\Http\Message\UriInterface;
use GuzzleHttp\Psr7\Uri;
use Spinit\Util;

/**
 * Description of Request
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ServerRequest
{
    /**
     * @var GuzzleRequest
     */
    private $request;
    
    public static function fromGlobals()
    {
        return new self(GuzzleRequest::fromGlobals(), false);
    }
    public function __construct($method, $uri, array $headers = array(), $body = null, $version = '1.1', $serverParams = array()) {
        if ($method instanceof GuzzleRequest) {
            $this->request = $method;
        } else {
            if (substr($uri, 0, 2) == '//') {
                $uri = 'http:'.$uri;
            }
            if (!strpos($uri, '://')) {
                $uri = 'http://'.$uri;
            }
            $this->request = new GuzzleRequest($method, $uri, $headers, $body, $version, $serverParams);
        }
    }
    
    /**
     * Le chiamate vengono rigirate alla richiesta guzzle. Se esse modificano la richiesta (e quindi generano un nuovo oggetto)
     * allora il risultato diventa la richiesta corrente.
     * @param type $name
     * @param type $arguments
     * @return type
     */
    public function __call($name, $arguments) {
        if (method_exists($this->request, $name)) {
            $result = call_user_func_array(array($this->request, $name), $arguments);
            if (substr($name, 0, strlen('with')) == 'with') {
                $this->request = $result;
            }
        } else {
            $uri = $this->request->getUri();
            $result = call_user_func_array(array($uri, $name), $arguments);
            if (substr($name, 0, strlen('with')) == 'with') {
                $this->withUri($result);
            }
        }
        return $result;
    }
    public function getRequest()
    {
        return $this->request;
    }
    public function __toString() {
        return (string) $this->getUri();
    }
    
    /**
     * Determina la root della request. Se la richiesta deriva da un proxy allora occorre impostare anche
     * la directory di proxy
     * @return type
     */
    public function getRoot()
    {
        $base = dirname(Util\arrayGet($this->getServerParams(), 'SCRIPT_NAME'));
        $url = array($this->getHost().($this->getPort()?':'.$this->getPort():''));
        if ($this->getHeaderLine('Proxypath')) {
            $url[] = trim($this->getHeaderLine('Proxypath'), '/');
        }
        if ($base and $base != '/') {
            $url[] = trim($base, '/');
        }
        return rtrim(implode('/', $url), '/');
    }
    
    /**
     * Restituisce l'ultima parte della uri
     */
    public function getLast($base)
    {
        $full = explode('://', (string)$this);
        return substr($full[1], strlen($base));
        
        var_dump($full);exit;
        $root = rtrim(dirname(Util\arrayGet($this->getServerParams(), 'SCRIPT_NAME')), '/');
        // viene tolto il path di partenza dello script
        $path = substr($this->request->getUri()->getPath(), strlen($root));
        // viene tolto il path base richiesto
        if ($path and $base) {
            $path = substr($path, strlen(ltrim($base, '/'))+1);
        }
        return ltrim($path, '/');
    }
    
    public function getUriPath()
    {
        $uri = explode('://' , (string) $this->getUri());
        $path = explode('/', array_pop($uri));
        $path[0] = $this->getRoot();
        return implode('/', $path);
    }
    
    public function getParsedBody()
    {
        return Util\nvl($this->request->getParsedBody(), array());
    }
}
