<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Adapter\Xml;

use Spinit\Osy\Type\FieldAdapterInterface;
use Spinit\Util;
/**
 * Description of FieldXmlAdapter
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class FieldXmlAdapter implements FieldAdapterInterface
{
    private $xml;
    private $childs = array();
    private $props = array();
    public function __construct($xml)
    {
        $this->xml = is_string($xml) ? simplexml_load_string($xml) : $xml;
        foreach ($this->xml->field as $field) {
            $this->childs[] = new self($field);
        }
        foreach ($this->xml->property as $prop) {
            $conf = json_decode(json_encode($prop), 1);
            unset($conf['@attributes']);
            $this->props[(string) $prop['name']] = $conf;
        }
    }

    public function getChilds()
    {
        return $this->childs;
    }
    
    public function get($name)
    {
        return Util\arrayGet($this->props, $name, (string) $this->xml[$name]);
    }
}
