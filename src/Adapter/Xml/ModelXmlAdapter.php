<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Adapter\Xml;

use Spinit\Osy\Type\ModelAdapterInterface;
use Spinit\Util;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of ModelXmlAdapter
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelXmlAdapter implements ModelAdapterInterface
{
    use \Spinit\Util\TriggerTrait;
    
    private $root;
    private $index;
    private $trigger = [];
    
    public function __construct($filename)
    {
        $filename = Util\sanitizePath($filename);
        $this->root = '';
        $fname = $filename;
        if (basename($fname) == 'index.xml') {
            if (!is_file($fname)) {
                $fname = dirname($fname).'.xml';
            } else {
                $this->root = dirname($fname);
            }
        }
        if (is_file($fname)) {
            $this->index = simplexml_load_file($fname);
        } else {
            throw new NotFoundException('File non trovato : '.$fname);
        }
        foreach(Util\nvl($this->index->trigger->event, []) as $event) {
            foreach(Util\nvl($event->children(), []) as $code) {
                $this->trigger[(string)$event['name']][(string)$event['when']][] = $code;
            }
        }
    }
    public function getRoot()
    {
        return $this->root;
    }

    public function getFields()
    {
        $result = array();
        foreach($this->index->xpath('//struct/field') as $field) {
            $result[(string) $field['name']] = $this->getFieldConf($field);
        }
        return $result;
    }
    private function getFieldConf($field)
    {
        $fieldAr = array();
        foreach($field->attributes() as $k=>$v) {
            $fieldAr[$k] = (string) $v;
        }
        $fieldAr['childs'] = array();
        foreach($field->xpath('field') as $subfield) {
            $fieldAr['childs'][(string) $subfield['name']] = $this->getFieldConf($subfield);
        }
        return $fieldAr;
    }
    public function get($name)
    {
        return (string) $this->index[$name];
    }
    
    public function execTrigger($event, $target)
    {
        foreach(Util\arrayGet(Util\arrayGet($this->trigger, $event['name']), $event['when'], []) as $code) {
            $this->process($code, $target);
        }
    }
    
    private function process($item, $target)
    {
        switch($item->getName()) {
            case 'set' :
                $target->set((string) $item['field'], $target->normalize($item));
        }
    }
}
