<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Adapter\Xml;

use Spinit\Osy\Core\ComponentBuilder;
use Spinit\Util;
/**
 * Description of ComponentBuilderXml
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ComponentBuilderXml extends ComponentBuilder
{
    public function __construct($xml)
    {
        $this->setBase('', (string)$xml['base']);
        foreach(Util\nvl($xml->base, array()) as $base) {
            $this->setBase((string) $base['name'], (string) $base);
        }
        foreach(Util\nvl($xml->form, array()) as $form) {
            $this->setForm((string) $form, (string) $form['base']);
        }
        foreach(Util\nvl($xml->field, array()) as $field) {
            $this->set((string) $field['type'], (string)$field, (string) $field['base']);
        }
    }
}
