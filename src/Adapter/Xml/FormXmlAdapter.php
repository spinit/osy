<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Adapter\Xml;

use Spinit\Osy\Type\FormAdapterInterface;
use Spinit\Util;
use Spinit\Osy\Adapter\Xml\ComponentBuilderXml;

use Spinit\Util\Error\NotFoundException;

/**
 * Description of FormXmlAdapter
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FormXmlAdapter implements FormAdapterInterface
{
    /**
     * Root path dove risiede il codice della form
     * @var \string
     */
    private $root;
    
    /**
     * Lista dei campi di primo livello
     * @var array
     */
    private $fields;
    
    /**
     * Struttura xml della form
     * @var \SimpleXML
     */
    private $index;
    
    /**
     * In fase di costruzione i field xml vengono mallati in oggetti Adapter
     * @param string $fname
     */
    public function __construct($fname, $name='')
    {
        $this->name = Util\biName($name);
        $fname = Util\sanitizePath($fname);
        $this->root = dirname($fname);
        if (!is_file($fname)) {
            throw new NotFoundException('Form non trovata ['.$this->getFullName().']');
        }
        $this->index = simplexml_load_file($fname);
        $this->fields = array();
        foreach($this->index->xpath('//struct/field') as $xml) {
            $this->fields[] = new FieldXmlAdapter($xml);
        }
    }
    
    public function getName()
    {
        return $this->name;
    }
    public function getFullName()
    {
        return implode(':',$this->name);
    }
    /**
     * Ritorna il codice del associato all'evento {event}-{when}
     * @param string $event
     * @param string $when
     * @return string
     */
    public function getTrigger($event, $when)
    {
        $fname = implode(DIRECTORY_SEPARATOR, array($this->root, 'Trigger', $event.'-'.$when.'.php'));
        if (is_file($fname)) {
            return file_get_contents($fname);
        }
        return '';
    }
    
    /**
     * Rivela se la form ha una struttura oppure risponde solo a comandi
     * @return boolean
     */
    public function hasStruct()
    {
        return count($this->index->xpath('//struct')) != 0;
    }
    
    /**
     * Ad una form è possibile associare un codice che gestisce la sua interfaccia
     * @return string
     */
    public function getBuilderCode()
    {
        return (string) Util\arrayGet($this->index->xpath('//struct/code'), 0);
    }
    
    /**
     * Lista dei campi mappati
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    public function getComponentBuilder()
    {
        return new ComponentBuilderXml($this->index->component);
    }
    
    public function get($name)
    {
        return (string) $this->index->struct[$name];
    }
    
    public function getEditRoleList()
    {
        return Util\asArray((string) $this->index['edit'], ',');
    }
    public function getViewRoleList()
    {
        return Util\asArray((string) $this->index['view'], ',');
    }
    public function isActive()
    {
        return !in_array((string) $this->index['active'], ['0', 'no']);
    }
}
