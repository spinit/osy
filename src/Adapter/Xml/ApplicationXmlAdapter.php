<?php

namespace Spinit\Osy\Adapter\Xml;

use Spinit\Osy\Type\ApplicationAdapterInterface;
use Spinit\Util;
use Spinit\Osy\Adapter\Xml\FormXmlAdapter;
use Spinit\Osy\Helper\ScanDirModules;
use Spinit\Util\Error\NotFoundException;
use Spinit\Osy\Core\ComponentBuilder;

/**
 * Description of ApplicationXmlAdapter
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ApplicationXmlAdapter implements ApplicationAdapterInterface
{
    private $root;
    private $index;
    private $name;
    
    public function __construct($fname, $label, $name)
    {
        $fname = Util\sanitizePath($fname);
        if (is_dir($fname)) {
            $fname.= DIRECTORY_SEPARATOR . 'index.xml';
        }
        if (!is_file($fname)) {
            throw new NotFoundException('Applicazione non trovata > '.$fname);
        }
        $this->root = dirname($fname);
        $this->index = simplexml_load_file($fname);
        $this->label = $label;
        $this->name = $name;
    }
    
    public function getLabel()
    {
        return $this->label;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getForm($name)
    {
        $biname = Util\biName($name);
        //var_dump($name, $biname);exit;
        $fname = $this->root."/Module/{$biname[0]}/Form/{$biname[1]}/index.xml";
        return new FormXmlAdapter($fname, $biname);
    }

    public function getModel($name)
    {
        $biname = Util\biName($name);
        $fname = $this->root."/Module/{$biname[0]}/Model/{$biname[1]}/index.xml";
        return new ModelXmlAdapter($fname);
    }
    public function getModelNameList()
    {
        $scan = new ScanDirModules();
        return $scan->getItemList($this->root.'/Module', 'Model');
    }

    public function getComponentBuilder()
    {
        return new ComponentBuilderXml($this->index->component);
    }

    public function getRouteList()
    {
        $list = array();
        foreach($this->index->routes as $routes) {
            foreach($routes->route as $route) {
                $list[] = ['path'=>(string)$route['path'], 'content'=>(string)$route];
            }
        }
        return $list;
    }

    public function getSourceCode($name)
    {
        $file = $this->root.'/Source/'.str_replace(':', '/', $name).'.php';
        return Util\file_get_contents($file);
    }

    public function getFormNameDefault()
    {
        return ((string) $this->index['formDefault']);
    }
    /**
     * Trasforma la gerarchia xml in una gerarchia array con childs come contenitore dei figli
     * @return type
     */
    public function getMenuList()
    {
        $list = [];
        foreach(Util\nvl($this->index->menubar->menu, []) as $menu) {
            $list[] = $this->produce($menu); 
        }
        return $list;
    }
    private function produce ($menu)
    {
        $item = [];
        foreach($menu->attributes() as $k => $v) {
            $item[(string)$k] = (string)$v;
        }
        if ($menu) {
            foreach(Util\nvl($menu->menu, []) as $k => $sub) {
                $item['childs'][] = $this->produce($sub);
            }
        }
        return $item;
    }
    
    /**
     * Restituisce un dataset contenente la configurazione dei ruoli all'interno dell'applicazione
     * @return \Spinit\Datasource\DataSetArray
     */
    public function getRoleList()
    {
        $result = [];
        foreach($this->index->roles->role as $role) {
            $name = (string) $role['name'];
            $key = $this->name.'@role:'.$name;
            $item = ['id'=>md5($key), 'id_app'=> md5($this->name), 'nme' => $name, 'cod' => $key, 'extender' => [], 'extending' => []];
            foreach($role->extender as $ext) {
                foreach(Util\asArray((string) $ext, ',') as $e) {
                    $item['extender'][] = $this->makeExtendItem($e);
                }
            }
            foreach($role->extending as $ext) {
                foreach(Util\asArray((string) $ext, ',') as $e) {
                    $item['extending'][] = $this->makeExtendItem($e);
                }
            }
            $result[$item['id']] = $item;
        }
        
        return new \Spinit\Datasource\DataSetArray($result);
    }
    private function makeExtendItem($e)
    {
        if (strpos($e, '@')===false) {
            $e = $this->name.'@role:'.$e;
        }
        list($app, ) = explode('@', $e);
        return ['id'=>md5($e), 'cod'=>$e, 'id_app'=>md5($app)];
    }
}
