<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Adapter;

use Spinit\Util;
use Spinit\Osy\Adapter\Xml\ApplicationXmlAdapter;

/**
 * Description of Loader
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FSXml {
    public function getInstance($data) {
        return new ApplicationXmlAdapter($data['src'], $data['nme'], $data['uri']);
    }
}
