<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Adapter;

use Spinit\Util;
/**
 * Description of Loader
 * Carica l'adapter corretto per l'applicazione
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class LoaderApplication
{
    public function load($data) {
        $class = __NAMESPACE__.'\\'.Util\nvl($data['typ'], 'FSXml');
        
        return Util\getInstance(new $class(), $data);
    }
    
    public function loadByName($datasource, $nameApp)
    {
        return $this->load($datasource->find('osx_app', '*', md5($nameApp)));
    }
    /**
     * Carica tutte gli adapter configurati su main e attivi su ice
     * @param type $main
     * @param type $ice
     * @return type
     */
    public function loadAll($main, $ice)
    {
        $list = array();
        $appIce = $ice->load('select id from osy_app')->getList();
        $appMain = $main->load('select * from osx_app')->getList();
        foreach($appIce as $appId) {
            if (isset($appMain[$appId])) {
                $list[] = $this->load($appMain[$appId]);
            }
        }
        return $list;
    }
}
