<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Adapter\Helper;

use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;

/**
 * Description of FieldUtilAdapter
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FieldHelperAdapter implements FieldInterface
{
    private $form;
    private $field;
    public function __construct($form, $field)
    {
        $this->form = $form;
        $this->field = $field;
    }
    public function get($name)
    {
        return Util\arrayGet($this->field, $name);
    }

    public function getChilds()
    {
        return Util\arrayGet($this->field, 'childs', array());
    }

    public function getForm()
    {
        return $this->form;
    }

    public function getName()
    {
        return Util\arrayGet($this->field, 'name');
    }

    public function getValue()
    {
        return Util\arrayGet($this->field, 'value');
    }

    public function setValue($value) 
    {
        $this->field['value'] = $value;
    }

}
