<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Spinit\Osy;

use Spinit\Util\Configuration as MainConfiguration;

/**
 * Description of Configuration
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Configuration extends MainConfiguration
{
    public function __construct() {
        parent::__construct(__DIR__, '../conf', 1);
    }
}
