<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core\Manager;

use Spinit\Util;
use Spinit\Osy\Core\Instance;
use Webmozart\Assert\Assert;

/**
 * Factory permette di :
 * - poter mappare i costruttori di manager con un nome
 * - poter registrare con quali gestori di rotte un manager dovrà essere inizializzato
 * - poter individuare e inizializzare il manager appropriato utilizzando le impostazioni di cui sopra
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Factory
{
    private static $managerList = array();
    private static $managerRouterList = array();
    
    public static function registerManagerConstructor($names, $constructor)
    {
        foreach(Util\asArray($names, ',') as $name) {
            self::$managerList[$name] = $constructor;
        }
    }
    public static function registerManagerRouter($names, $router)
    {
        foreach(Util\asArray($names, ',') as $name) {
            self::$managerRouterList[$name][] = $router;
        }
    }
    /**
     * Ricerca tra i manager predefiniti. Se non lo trova prende quelli presenti nel namespace
     * corrente
     * @param type $name
     * @param type $instance
     * @return type
     */
    public static function getManager($name, $instance)
    {
        $constructor = Util\arrayGet(self::$managerList, $name, __NAMESPACE__.'\\'.$name);
        $manager = Util\getInstance($constructor, $instance);
        Assert::isInstanceOf($manager, Util\upName(__NAMESPACE__).'\\Manager');
        foreach(Util\arrayGet(self::$managerRouterList, $name, array()) as $router) {
            $manager->addRouter($router);
        }
        return $manager;
    }
}
