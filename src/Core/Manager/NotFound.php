<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core\Manager;

use Spinit\Osy\Core\Manager;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of Frontend
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class NotFound extends Manager
{
    public function run($path = null) {
        throw new NotFoundException('Istance not found : '. $this->getInstance()->getRequest()->getRoot());
    }
}
