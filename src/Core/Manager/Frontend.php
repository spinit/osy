<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core\Manager;

use Spinit\Osy\Core\Manager;
use Spinit\Util;
use Spinit\Osy\Helper\AssetRouter;
use Spinit\Util\Error\NotFoundException;
use Spinit\Util\Error\StopException;
use Spinit\Osy\Helper\Matcher;

/**
 * Description of Frontend
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class Frontend extends Manager
{
    use Util\TriggerTrait;
    
    private $custom;
    private $main;
    private $root;
    private $routes = array();
    
    public function __construct($instance)
    {
        parent::__construct($instance);
        $this->custom = getcwd().'/Frontend/themes/'.Util\nvl('', 'traveler').'/';
        $this->main  = __OSY__.'/Manager/Frontend/themes/'.Util\nvl('', 'traveler').'/';
    }
    public function getRoot()
    {
        return $this->root;
    }
    private function getContent($file, $path = '')
    {
        $this->root = $this->custom;
        $fname = $this->custom.$file;
        if (!is_file($fname)) {
            $this->root = $this->main;
            $fname = $this->main.$file;
        }
        ob_start();
        include($fname);
        return ob_get_clean();
    }
    
    /**
     * Trova l'applicazione da dover lanciare associata al path richiesto
     * se non lo trova fa partire il blog
     * @param type $path
     */
    public function run($request = null)
    {
        $request = $request ?: $this->getInstance()->getRequest();
        $path = $request->getLast($this->getInstance()->getRoot());
        try {
            $this->initRoute($path);
            // viene fatto partire il processo di generazione del contenuto
            $content = $this->getContent('index.php', $path);
        } catch (NotFoundException $e) {
            $content = $this->getContent('notfound.php', $path);
        }
        return str_replace('{{WEB_ROOT}}', '/', $content);
    }
    
    public function display($event)
    {
        ob_start();
        $result = $this->trigger($event);
        echo ob_get_clean();
        $this->displayResult($result);
    }
    private function displayResult($result)
    {
        if (is_array($result)) {
            foreach($result as $item) {
                $this->displayResult($item);
            }
        } else {
            echo $result;
        }
    }
    
    /**
     * Vengono prese in considerazione tutte le route di tutte le app attive sull'istanza.
     * Verranno eseguite solo quelle che matchano con il path indicato.
     * Quando una route viene eseguita, istanzia il controller in essa indicata. Tale controller si registra
     * sul Manager come ascoltatore degli eventi di interesse. Inoltre al controller vengono passati i parametri estratti
     * dalla url come indicato dal path della route.
     * @param type $path
     */
    private function initRoute($path)
    {
        $routes = new Matcher();
        $routes->bindExec('notfound', [Util\throwError($path,'Spinit:Util:Error:NotFoundException')]);
        foreach($this->getInstance()->getApplicationList() as $app) {
            foreach($app->getRouteList() as $route) {
                $routes->add($route->getPath(), function($args) use ($route, $app, $path) {
                    $app->makeObject($route->getContent(), $this, $args, $path);
                });
            }
        }
        $routes->exec($path);
    }
}
