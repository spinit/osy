<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core\Manager;

use Spinit\Osy\Core\Manager;
use Spinit\Osy\Core\Application;
use Spinit\Osy\Adapter\Xml\ApplicationXmlAdapter;
use Spinit\Util;
use Spinit\Util\Error\NotFoundException;
use Spinit\Osy\Http\Response;
use Spinit\Osy\Configuration;
use Spinit\Osy\Adapter\LoaderApplication;
use Spinit\Osy\Core\User;

use Webmozart\Assert\Assert;

/**
 * Opensymap è incaricato di trovare ed eseguire la form dell'applicazione richiesta
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class Backend extends Manager
{
    /**
     *
     * @var Configuration
     */
    private $conf = null;
    
    /**
     *
     * @var LoaderApplication
     */
    private $loaderApplication = null;
    
    public function __construct($instance)
    {
        parent::__construct($instance);
        $this->conf = new \Spinit\Osy\Configuration();
        $this->setLoaderApplication(new LoaderApplication());
        $this->conf = new Configuration();
        $this->bindExec('init', function() {
            foreach($this->getInstance()->getApplicationList() as $app) {
                $app->init();
            }
        });
    }
    public function getLoaderApplication()
    {
        return $this->loaderApplication;
    }
    public function setLoaderApplication($loader)
    {
        $this->loaderApplication = $loader;
    }
    public function run($request = null)
    {
        $this->getInstance()->setApplication($this->getSystem());
        $pathArray = Util\asArray($request->getLast($this->getInstance()->getRoot()), '/');
        if (!($user = $this->getUser())) {
            $runner =  $this->getSystem()->getForm('Login');
        } else {
            $req = ['app'=>array_shift($pathArray), 'form'=>array_shift($pathArray)];

            $appOpen = $this->checkUserApplication($user, $req['app'], $req['form']);
            $runner = $this->getApplication($appOpen);
            $this->getInstance()->setApplication($runner);
        }
        return $runner->run($request, $pathArray);
    }
    
    protected function checkUserApplication($user, $app, $form)
    {
        if (empty($app)) {
            return ['System','Main'];
        }
        return [$app, $form];
    }
    public function getUser()
    {
        $user = $this->getInstance()->getUser();
        if ($user) {
            return $user;
        }
        if (Util\getenv('USER_ID')) {
            $ouser = $this->getSystem()->getModel('Anag');
            $ouser->trigger('load', [Util\getenv('USER_ID')]);
            if ($ouser->get('id')) {
                return $ouser;
            }
        }
        return null;
    }
    public function logout()
    {
        $this->getInstance()->getMain()->trigger('setAuthUser', null);
    }
    public function authUser($username, $password)
    {
        Assert::notEmpty(trim($username), "Utente non impostato");
        // viene ricercato l'utente impostato nelle variabili d'ambiente
        $user = $this->getInstance()->getDataSource()->load(
            $this->conf->get('AuthUser'),
            ['username' => trim($username), 'url'=>$this->getInstance()->getInfo('id_url')]
        )->current();
        
        Assert::notNull($user, "Utente non trovato : ".$username);
        Assert::eq(strtoupper($user['pwd']), strtoupper(md5($password)), "Credenziali errate");
        
        $this->getInstance()->getMain()->trigger('setAuthUser', array($user['id'], $user['lgn'], Util\nvl($user['nme'], $user['lgn'])));
        $ouser = $this->getSystem()->getModel('Anag');
        $ouser->trigger('load', [$user['id']]);
        $this->getInstance()->setUser($ouser);
        return $ouser;
    }
    
    protected function getApplication($AppName)
    {
        if (!is_array($AppName)) {
            $AppName = [$AppName];
        }
        $app = array_shift($AppName);
        $form = array_shift($AppName);
        $appRec = $this->getInstance()->getDataSource()->load(
            $this->conf->get('Application'),
            array('name'=>$app)
        );
        if (!$appRec->current()) {
            throw new NotFoundException("Applicazione non trovata : ".$app);
        }
        $opp = new Application($this->getInstance(), $form);
        $opp->setAdapter($this->getLoaderApplication()->load($appRec->current()));
        $opp->setManager($this);
        return $opp;
    }
    
    public function getTemplate()
    {
        $root = __OSY__.str_replace('/', DIRECTORY_SEPARATOR, '/Manager/Backend/template/');
        ob_start();
        if ($this->getInstance()->getInfo('id') == '') {
            include($root.'install.php');
        } else {
            include($root.'desktop.php');
        }
        
        return str_replace(
            array('{{WEB_ROOT}}'),
            array('//'.trim($this->getInstance()->getInfo('url'), '/')),
            \ob_get_clean()
        );
    }
    
    public function makePath($url)
    {
        return $this->getInstance()->makePath('__asset/opensymap/'.ltrim($url, '/'));
    }
}
