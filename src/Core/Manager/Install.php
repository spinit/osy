<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core\Manager;

use Spinit\Osy\Core\Manager;
use Spinit\Osy\Core\Application;
use Spinit\Osy\Adapter\Xml\ApplicationXmlAdapter;
use Spinit\Util;
use Spinit\Util\Error\NotFoundException;
use Spinit\Osy\Http\Response;

/**
 * Opensymap è incaricato di trovare ed eseguire la form dell'applicazione richiesta
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

class Install extends Manager
{
    public function run($path = null)
    {
        $app = (new Application($this->getInstance(), 'Main'));
        $app->setAdapter(new ApplicationXmlAdapter(__OSY__.'/app/Install/index.xml', 'Install', 'uri:osyapp:opensymap.org#Install'));
        $app->setManager($this);
        return $app->run($this->getInstance()->getRequest());
    }
    
    public function getTemplate()
    {
        ob_start();
        include(__OSY__.'/Manager/Backend/template/install.php');
        return str_replace(
            array('{{WEB_ROOT}}'),
            array('//'.trim($this->getInstance()->getRoot(),'/')),
            \ob_get_clean()
        );
    }
    
    public function makePath($url)
    {
        return $this->getInstance()->makePath('__asset/opensymap/'.ltrim($url, '/'));
    }
}
