<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core\Manager;

use Spinit\Util;

/**
 * Description of FrontendController
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class FrontendController
{
    private $manager;
    private $args;
    private $path;
    private $application;
    
    public function __construct(Frontend $manager, $args, $path)
    {
        $this->manager = $manager;
        $this->args = $args;
        $this->path = $path;
        $this->application = null;
        $this->init();
    }
    
    public function setApplication($application)
    {
        $this->application = $application;
    }
    public function getApplication()
    {
        return $this->application;
    }
    public function init()
    {
        
    }
    public function getManager()
    {
        return $this->manager;
    }
    
    public function getArgs()
    {
        $params = func_get_args();
        if(!count($params)) {
            return $this->args;
        }
        return Util\arrayGetAssert($this->args, $params[0]);
    }
    public function getPath()
    {
        return $this->path;
    }
}
