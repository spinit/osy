<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core;

use Spinit\Util\Error\StopException;

/**
 * Description of Channel
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Channel {
    private $list = [];
    public function __construct()
    {
        date_default_timezone_set("Europe/Rome");
        header('Content-Type: text/event-stream; charset=utf-8');
        header('Cache-Control: no-cache');
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Methods: GET');
        header('Access-Control-Expose-Headers: X-Events');
        header("Access-Control-Allow-Origin: *");
        
        @ini_set('zlib.output_compression',0);

        @ini_set('implicit_flush',1);

        @ob_end_clean();

        set_time_limit(0);
        
        ignore_user_abort(true);
        
    }
    private function stampa($message)
    {
        if (is_array($message) and !isset($message['time'])) {
            $message['time'] = [time(),date('d/m/Y H:i:s')];
        }
        echo "data: ".str_pad(json_encode($message), 1024 * 64, ' ')."\r\n\r\n";
        while(ob_get_level()) {
            ob_end_flush();
        }
        flush();
    }
    public function add($runner)
    {
        $this->list[] = $runner;
        return $this;
    }
    public function run($timeSleep = 1)
    {
        $start = 0;
        while(true) {
            foreach($this->list as $runner) {
                $this->checkIsAlive();
                call_user_func($runner, function ($message){
                    $this->stampa($message);
                });
            }
            while (time() - $start < $timeSleep) {
                usleep(500 * 1000); // 1/2 di sec
                $this->checkIsAlive();
            }
            $start = time();
        }
        throw new StopException();
    }
    public function each($list)
    {
        foreach($list as $message) {
            $this->checkIsAlive();
            $this->stampa($message);
        }
        throw new StopException();
    }
    
    private function checkIsAlive()
    {
        if(connection_aborted()){
            exit;
        }
    }
}
