<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core;

use Spinit\Util;
use Spinit\Util\Error\NotFoundException;
use Spinit\Osy\Core\Instance;
use Spinit\Osy\Adapter\Xml\ApplicationXmlAdapter;

/**
 * Description of Manager
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
abstract class Manager
{
    use Util\TriggerTrait;
    
    private $routes = array();
    
    
    /**
     * Applicazione che contiene tutte le risorse comuni
     * @var Application
     */
    private $system = null;
    
    public function __construct($instance)
    {
        $this->instance = $instance;
    }
    
    protected function getSystemAdapter($instance)
    {
        $ld = new \Spinit\Osy\Adapter\LoaderApplication();
        return $ld->loadByName($instance->getMain()->getAppRegister(), 'uri:opensymap:opensymap.org#System');
    }
    /**
     * 
     * @return Application
     */
    public function getSystem()
    {
        if (!$this->system) {
            $this->system = (new Application($this->getInstance()))
                ->setManager($this)
                ->setAdapter($this->getSystemAdapter($this->instance));
        }
        return $this->system;
    }

    /**
     * 
     * @return Instance
     */
    public function getInstance()
    {
        return $this->instance;
    }
    
    abstract public function run($path = null);
    
    public function getUser() { return null; }
    public function authUser($username, $password) { return null; }
    /**
     * Template di default
     * @return string
     */
    public function getTemplate()
    {
        return '<!--content-->';
    }
    
    public function addRouter($router)
    {
        $this->routes[] = $router;
    }
    
    public function getRunner($path)
    {
        foreach($this->routes as $route) {
            if ($runner = Util\getInstance($route, $this->getInstance(), $path)) {
                return $runner;
            }
        }
        throw new NotFoundException('Nessun Runner trovato');
    }
    
    public function makePath($url)
    {
        return $this->getInstance()->makePath($url);
    }
    public function getFormBuilder()
    {
        $builder = $this->getSystem()->getFormBuilder(false);
        return $builder;
    }
    
    public function getFieldBuilder($type)
    {
        $builder = $this->getSystem()->getFieldBuilder($type, false);
        return $builder;
    }
    
}
