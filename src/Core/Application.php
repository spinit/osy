<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core;

use Spinit\Osy\Type\InstanceInterface;
use Spinit\Osy\Type\ApplicationAdapterInterface;
use Spinit\Osy\Core\Manager;
use Spinit\Osy\Type\ApplicationInterface;
use Spinit\Util;

use Webmozart\Assert\Assert;

/**
 * Description of Application
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Application implements ApplicationInterface
{
    use Util\ExecCodeTrait;
    
    /**
     *
     * @var Instance
     */
    private $instance;
    
    /**
     *
     * @var Manager
     */
    private $manager;
    
    /**
     *
     * @var ApplicationAdapterInterface
     */
    private $adapter;
    
    private $formName;
    
    private $componentBuilder;
    
    private $name;
    
    public function __construct(InstanceInterface $instance, $form = '')
    {
        $this->instance = $instance;
        $this->setFormName($form);
    }
    
    public function getLabel()
    {
        return $this->getAdapter()->getLabel();
    }
    public function getName()
    {
        return $this->getAdapter()->getName();
    }
    public function setFormName($formName)
    {
        $this->formName = $formName;
        return $this;
    }
    public function getFormName()
    {
        return $this->formName ?: $this->getAdapter()->getFormNameDefault();
    }
    public function setAdapter(ApplicationAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->componentBuilder = $adapter->getComponentBuilder();
        return $this;
    }
    
    public function getAdapter()
    {
        Assert::notNull($this->adapter, 'ApplicationAdapter non impostato');
        return $this->adapter;
    }
    
    /**
     * 
     * @return Instance
     */
    public function getInstance()
    {
        return $this->instance;
    }
    
    /**
     * 
     * @return Manager
     */
    public function getManager()
    {
        Assert::notNull($this->manager);
        return $this->manager;
    }
    
    public function setManager(Manager $manager)
    {
        $this->manager = $manager;
        return $this;
    }
    /**
     * Se la form ha una struttura e la chiamata è in GET allora la risposta è un html
     * altrimenti è un json
     * @return type
     */
    public function run($request, $path = array())
    {
        $form = $this->getForm($this->getFormName());
        return $form->run($request, $path);
    }
    public function getForm($name)
    {
        $form = new Form($this, $name);
        $form->setAdapter($this->getAdapter()->getForm($name));
        return $form;
    }
    public function getModel($name)
    {
        return new Model($this, $this->getAdapter()->getModel($name));
    }

    public function getModelNameList()
    {
        return $this->getAdapter()->getModelNameList();
    }

    public function getDataSource($name = '')
    {
        return $this->getInstance()->getDataSource($name);
    }
    public function init()
    {
        foreach($this->getModelNameList() as $modelName) {
            // inizializzazione
            $this->getModel($modelName)->init();
        }

        $role = $this->getManager()->getSystem()->getModel('Role');
        foreach($this->getAdapter()->getRoleList() as $key => $item) {
            
            $extender = $item['extender'];
            unset ($item['extender']);
            $extending = $item['extending'];
            unset ($item['extending']);
            foreach($extending as $ex) {
                $item['extend'][] = $ex['id'];
            }
            $role->clear();
            $role->trigger('load', [$key]);
            if ($role->get('id')) {
                continue;
            }
            $role->clear();
            list($app, ) = explode('@', $item['cod']);
            $item['id_app'] = md5($app);
            $role->set($item);
            $role->set('id', $key);
            $role->trigger('save');
            foreach($extender as $ex) {
                $role->clear();
                $role->trigger('load', $ex['id']);
                $extend = $role->get('extend')?:[];
                $extend[] = $key;
                $role->set('extend', $extend);
                $role->trigger('save');
            }
        }
        return $this;
    }
    public function getFieldBuilder($type, $deepSearch = true)
    {
        try {
            return $this->componentBuilder->get($type);
        } catch (\Exception $ex) {
            if ($deepSearch) {
                return $this->getManager()->getFieldBuilder($type);
            }
            throw $ex;
        }
    }
    
    public function getFormBuilder($deepSearch = true)
    {
        try {
            return $this->componentBuilder->getForm();
        } catch (\Exception $ex) {
            if ($deepSearch) {
                return $this->getManager()->getFormBuilder();
            }
            throw $ex;
        }
    }
    
    public function getRouteList()
    {
        $list = [];
        foreach($this->getAdapter()->getRouteList() as $route) {
            $list[] = new Route($this, $route['path'], $route['content']);
        }
        return $list;
    }
    
    public function makeObject()
    {
        $args = func_get_args();
        $name = $args[0];
        $code = $this->getAdapter()->getSourceCode($name);
        $ns = __NAMESPACE__.'\\SourceCode\\NS'.md5($code);
        $classPath = $ns.Util\getClassPath($name);
        if (!class_exists($classPath)) {
            $source = str_replace('<?php', '<?php namespace '.Util\upName($classPath).';', $code);
            $this->execCode($source);
        }
        $args[0]  = $classPath;
        $object = call_user_func_array('\\Spinit\\Util\\getInstance', $args);
        if (method_exists($object, 'setApplication')) {
            $object->setApplication($this);
        }
        return $object;
    }
    
    public function getUrl($formName = '')
    {
        $url = $this->getInstance()->makePath($this->getLabel().'/');
        if ($formName) {
            $url .= $formName.'/';
        }
        return $url;
    }
}
