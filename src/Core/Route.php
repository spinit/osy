<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core;

/**
 * Description of Route
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Route
{
    private $application;
    private $path;
    private $content;
    
    public function __construct($application, $path, $content)
    {
        $this->application = $application;
        $this->path = $path;
        $this->content = $content;
    }
    public function getPath()
    {
        return $this->path;
    }
    public function getApplication()
    {
        return $this->application;
    }
    public function getContent()
    {
        return $this->content;
    }
}
