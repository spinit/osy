<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core;

use Spinit\Osy\Type\ModelInterface;
use Spinit\Osy\Type\ModelAdapterInterface;
use Spinit\Datasource\DataSource;
use Spinit\Osy\Type\ApplicationInterface;

use Spinit\Util;

use Webmozart\Assert\Assert;

use Spinit\Datasource\Core\GetTableStructInterface;
use Spinit\Datasource\TableStruct;

use Spinit\UUIDO;

use Spinit\Osy\Helper\ModelInitializer;

/**
 * Description of Model
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Model implements ModelInterface, GetTableStructInterface
{
    use \Spinit\Util\TriggerTrait;
    
    private $application;
    private $adapter;
    private $datasource;
    private $pkey;
    private $fields;
    private $counter;
    private $data;
    private $traceField = array('traced' => '__pref_id__', 'tracing'=>'__pref_rec__', 'suffix'=>'_');
    
    public function __construct(ApplicationInterface $application = null, ModelAdapterInterface $adapter = null)
    {
        $this->setApplication($application);
        $this->setAdapter($adapter);
        
        $this->bindExec('save', function() {
            return $this->trigger(empty($this->pkey) ? 'insert' : 'update');
        });
        $this->bindExec('load', function($event) {
            return $this->load($event->getParam(0));
        });
        $this->bindExec('update', function() {
            return $this->update();
        });
        $this->bindExec('insert', function() {
            return $this->insert();
        });
        $this->bindExec('delete', function() {
            return $this->delete();
        });
        
    }
    protected function onTrigger($event)
    {
        $this->getAdapter()->execTrigger($event, $this);
    }
    protected function load($pkey)
    {
        $this->clear();
        $this->setPkey($pkey);
        $this->data = $this->read($pkey);
        $this->set($this->data);
        return $this;
    }
    
    private function read($pkey)
    {
        $DS = $this->getDataSource();
        
        $resource = $this->getAdapter()->get('resource');
        $data = $DS->find($resource, $this->fields, $this->checkPkey($pkey));
        return $data;
    }
    
    public function get($name)
    {
        $field = Util\arrayGetAssert($this->fields, $name);
        return Util\arrayGet($field, 'value');
    }
    
    public function set($name, $value = '')
    {
        if (is_array($name)) {
            foreach($name as $k=>$v) {
                $this->set($k, $v);
            }
            return $this;
        }
        if ($name) {
            $field = Util\arrayGetAssert($this->fields, $name);
            $this->fields[$name]['value'] = $value;
        }
        return $this;
    }
    
    public function map($name, $struct)
    {
        $this->fields[$name] = $struct;
    }
    private function checkPkey($pkey)
    {
        if (!is_array($pkey) and !empty($pkey)) {
            $pkey = array('id'=>$pkey);
        }
        return $pkey;
    }
    public function setPkey($pkey)
    {
        $this->pkey = $this->checkPkey($pkey);
        return $this;
    }
    public function getPkey()
    {
        return $this->pkey;
    }
    private function update()
    {
        $resource = (string)$this->getAdapter()->get('resource');
        
        if (!$this->data and $this->pkey) {
            $this->data = $this->read($this->pkey);
        }
        // si salvano i nuovi dati nel Datasource
        $data = array();
        foreach($this->fields as $name => $field) {
            if (!isset($field['value'])) {
                // se non è stato impostato ... allora non entra a far parte della modifica
                continue;
            }
            $data[$name] = $field['value'];
            if (Util\arrayGet($field, 'apply')) {
                $data[$name] = [Util\arrayGet($field, 'value'), Util\arrayGet($field, 'apply')];
            }
        }
        return $this->getDataSource()->update($resource, $data, $this->pkey, $this->data);
    }
    private function delete()
    {
        $resource = (string)$this->getAdapter()->get('resource');
        return $this->getDataSource()->delete($resource, $this->pkey);
    }
    
    /**
     * Effettua lo spostamento della riga da modificare nella tabella di tracing aggiustando gli ID
     * @param type $resource
     *
    private function traceData($resource)
    {
        if ($this->hasTrace()) {
            $resourceTrace = $resource.$this->traceField['suffix'];
            $keyTrace = null;
            $key = null;
            foreach($this->pkey as $name => $value) {
                $key = $this->fields[$name];
                $key['value'] = $value;
                $keyTrace = $this->fields[$name];
                $keyTrace['value'] = (string) $this->getCounter()->next();
            }
            $this->getDataSource()->trace($resource, $key, $resourceTrace, $keyTrace, $this->traceField);
            $this->fields[$this->traceField['traced']]['value'] = $keyTrace['value'];
        }
        
    }
    */
    /**
     * Se la primary key non ha valore ed è un binkey allora viene inizializzata
     * @return type
     */
    private function insert()
    {
        $this->pkey = array();
        foreach($this->fields as $key => $field) {
            $name = Util\arrayGet($field, 'name', $key);
            // verifica / impostazione chiave primaria
            if (Util\arrayGet($field, 'pkey')) {
                if (Util\arrayGet($field, 'value') == '' and 
                    Util\arrayGet($field, 'type') == 'binkey') {
                    if (Util\arrayGet($field, 'size') == '8') {
                        $this->set($name, UUIDO\randHex(16));
                    } else {
                        $this->set($name, $this->getCounter()->next());
                    }
                }
                $this->pkey[$name] = $this->get($name);
            }
        }
        $data = [];
        foreach($this->fields as $name => $field) {
            $data[$name] = Util\arrayGet($field, 'value');
            if (Util\arrayGet($field, 'apply')) {
                $data[$name] = [Util\arrayGet($field, 'value'), Util\arrayGet($field, 'apply')];
            }
        }
        return $this->getDataSource()->insert($this->getAdapter()->get('resource'), $data);
        echo json_encode($data);exit;
    }
    
    public function setDataSource(DataSource $datasource)
    {
        $this->datasource = $datasource;
    }
    /**
     * 
     * @return DataSource
     */
    public function getDataSource()
    {
        $datasource = $this->datasource ?: $this->getApplication()->getDataSource($this->getAdapter()->get('datasource'));
        return $datasource;
    }
    
    /**
     * 
     * @param ModelAdapterInterface $adapter
     */
    public function setAdapter(ModelAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        if ($adapter) {
            $this->fields = array();
            foreach($adapter->getFields() as $name => $struct) {
                $this->map($name, $struct);
            }
        }
       
    }
    
    /**
     * 
     * @return ModelAdapterInterface
     */
    public function getAdapter()
    {
        Assert::notNull($this->adapter);
        return $this->adapter;
    }
    
    /**
     * Imposta l'applicazione di riferimento
     * @param ApplicationInterface $application
     */
    public function setApplication(ApplicationInterface $application)
    {
        $this->application = $application;
    }
    /**
     * 
     * @return ApplicationInterface
     */
    public function getApplication()
    {
        Assert::notNull($this->application);
        return $this->application;
    }
    
    public function getFields()
    {
        return $this->fields;
    }
    public function init()
    {
        $this->tracing = false;
        try {
            $main = $this->getApplication()->getDataSource('main');
        } catch (Exception $ex) {
            $main = null;
        }
        $this->getDataSource()->align($this, $main);
        return $this;
    }

    public function clear()
    {
        $this->pkey = null;
        foreach(array_keys($this->fields) as $key) {
            unset($this->fields[$key]['value']);
        }
        return $this;
    }
    public function setCount($counter)
    {
        $this->counter = $counter;
    }
    
    public function getCounter()
    {
        return $this->counter ?: $this->getApplication()->getInstance()->getCounter();
    }
    
    public function normalize($string)
    {
        $string = (string) $string;
        preg_match_all('/\{\{([^ ]+)\}\}/', $string, $matches);
        foreach ($matches[1] as $k => $name) {
            $string = str_replace($matches[0][$k], $this->get($name), $string);
        }
        return trim($string);
    }
    /**
     * Restituisce la struttura da dover creare sul database.
     * Nel caso della tabella dati viene restituito quello che si è impostato nel model.
     * Nel caso della tabella tracing occorre aggiungere la colonna per l'id della riga dati di riferimento
     * @return TableStruct
     */
    public function getTableStruct()
    {
        $name = (string)$this->getAdapter()->get('resource');
        $fields = $this->fields;
        return new TableStruct(array('name'   => $name, 'fields' => $fields));
    }
}
