<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core;

use Spinit\Util\Error\NotFoundException;
use Spinit\Util;
use Webmozart\Assert\Assert;

/**
 * ComponentBuilder ha il compito di:
 * - mappare i diversi tipi di componenti con le relative classi che
 * saranno delegate alla loro formattazione;
 * - restituire la classe delegata relativa al tipo richiesto.
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ComponentBuilder
{
    private $base = array();
    private $conf = array();
    private $form = null;
    public function get($name)
    {
        try {
            $conf = Util\arrayGetAssert($this->conf, $name);
        } catch (\Exception $e) {
            $conf = Util\arrayGetAssert($this->conf, '');
        }
        return $this->make($conf);
    }
    private function make($conf)
    {
        Assert::notNull($conf);
        $base = Util\arrayGet($this->base, $conf['base']);
        $builder = [];  
        $type = $conf['type'];
        if ($base) {
            $arType = explode(':', $type);
            do {
                if($el = array_shift($arType) != '..') {
                    break;
                }
                $base = Util\upName($base, ':');
                $type = implode(':', $arType);
            } while(count($arType));
            $builder[]= $base;
        }
        $builder[] = $type;
        try {
            $class = str_replace(':', '\\', ":".ltrim(implode(':', $builder), ':'));
            return Util\getInstance($class);
        } catch (\Exception $e) {
            $class = str_replace(':', '\\', ":".ltrim(array_pop($builder), ':'));
            return Util\getInstance($class);
        }
    }
    
    public function setForm($type, $base = '')
    {
        $this->form = ['type'=>$type, 'base'=>$base];
    }
    
    public function getForm()
    {
        return $this->make($this->form);
    }
    public function setBase($name, $base)
    {
        $this->base[$name] = $base;
    }
    
    public function set($name, $type, $base = '')
    {
        $this->conf[$name] = array('type' => $type, 'base' => $base);
    }
}
