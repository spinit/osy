<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core;

use Spinit\Osy\Type\FieldAdapterInterface;
use Spinit\Osy\Type\FormInterface;
use Webmozart\Assert\Assert;
use Spinit\Osy\Type\FieldInterface;
use Spinit\Util;

/**
 * Description of Field
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Field implements FieldInterface
{
    use Util\TriggerTrait, Util\ExecCodeTrait;
    
    private $form;
    private $adapter;
    private $value;
    private $childs;
    private static $nameCount = array();
    private $name;
    private $builder;
    
    public function __construct(FormInterface $form, FieldAdapterInterface $adapter)
    {
        self::$nameCount[$form->getFullName()] = Util\arrayGet(self::$nameCount, $form->getFullName(),1);
        $this->name = 'Field'.md5($form->getFullName()).'x'.str_pad(self::$nameCount[$form->getFullName()]++, 6, '0', STR_PAD_LEFT);
        $this->form = $form;
        $this->setAdapter($adapter);
        $this->builder = $this->getForm()->getFieldBuilder($this->getAdapter()->get('type'));
        Assert::isInstanceOf($this->builder, Util\upName(__NAMESPACE__)."\\Type\\FieldBuilderInterface");
        $this->bindExec('@build', function($event) {
           $this->getForm()->getResponse()->addContent($this->getName(), $this->build($event)); 
        });
    }
    
    private function setAdapter(FieldAdapterInterface $adapter)
    {
        $this->childs = array();
        $this->adapter = $adapter;
        foreach($this->getAdapter()->getChilds() as $child)
        {
            $this->childs[] = new self($this->form, $child);
        }
    }
    
    /**
     * Esegue il codice registrato sull'adapter per quel particolare evento
     * @param type $event
     * @param type $param
     */
    protected function onTrigger($event, $param = array())
    {
        $this->getBuilder()->execTrigger($event, $this);
    }
    
    
    /**
     * TODO: Validare l'input
     * @param type $value
     */
    public function setValue($value)
    {
        /*
         * if ($value not valid) {
         *    $this->form->getResponse()->addError($nameField, $errorMessage);
         * }
         */
        $this->value = $value;
    }
    public function __toString() {
        $value = $this->getValue();
        if (is_array($value)) {
            $value = json_encode($value);
        }
        return $value;
    }
    public function getValue()
    {
        return $this->value;
    }
    public function getName()
    {
        $name = trim($this->get('name'));
        return $name ? $name : $this->name;
    }
    /**
     * 
     * @return FieldAdapterInterface
     */
    public function getAdapter()
    {
        return $this->adapter;
    }
    
    public function getBuilder()
    {
        return $this->builder;
    }
    /**
     * 
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }
    
    /**
     * Genera il codice di rappresentazione dell'oggetto
     * @return string
     */
    public  function build($event = null)
    {
        return $this->getBuilder()->build($this, $event);
    }
    
    public function getChilds()
    {
        return $this->childs;
    }
    
    public function get($name)
    {
        return $this->getAdapter()->get($name);
    }
}
