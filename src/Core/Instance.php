<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core;

use Spinit\Util;
use Spinit\Osy\Core\Manager\Factory as ManagerFactory;
use Spinit\Datasource\DataSource;
use Spinit\Osy\Configuration;
use Spinit\Osy\Http\ResponseHtml;
use Spinit\Osy\Http\Response;

use Spinit\Osy\Http\ServerRequest;
use Spinit\Osy\Type\InstanceInterface;
use Spinit\Util\Error\NotFoundException;
use Spinit\Osy\Core\Application;
use Spinit\Osy\Main;
use Spinit\UUIDO;
use Spinit\Osy\Core\Manager;

use Webmozart\Assert\Assert;

use Spinit\Osy\Adapter\LoaderApplication;

/**
 * Description of Instance
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Instance implements InstanceInterface
{
    use Util\TriggerTrait;
    
    private $info;
    /**
     *
     * @var Main
     */
    private $main;
    private $request;
    private $manager;
    private $DSMap = array();
    /**
     *
     * @var Application
     */
    private $application = null;
    
    private $user = null;
    
    private $counter = null;
    
    // punto di ingresso dell'istanza
    private $root = '';
    
    public function __construct($main, $request)
    {
        $this->main = $main ;
        $this->request = $request;
        $this->init();
        $this->mapDataSource('main', $this->main->getDataSource());
        if ($this->getInfo('id')) {
            $this->mapDataSource('', new DataSource($this->getInfo('con_str')));
        }
        // punto di partenza dell'instanza
        $this->root = rtrim($this->getInfo('url') ?: $this->getRequest()->getRoot(), '/') . '/';
        $this->bindExec('init', function() {
            $this->getManager()->trigger('init');
        });
   }
    
    public function setUser($user)
    {
        $this->user = $user;
    }
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Inizializzazione istanza e manager di riferimento
     */
    private function init()
    {
        // manager di default
        $DS = $this->getMain()
            ->getDataSource();
        try {
            $list = $DS->load(
                $this->getConfiguration()->get('Instance'),
                array('url' => $this->getRequest()->getUriPath())
            );
            $this->info = $list->current();
            if (!$this->getInfo('id')) {
                // se l'stanza non è stata trovata ...
                $this->info['manager'] = 'NotFound';
            } else {
                // se è stata trovata ma non ha un manager impostato allora viene preso Frontend
                $this->info['manager'] = Util\nvl($this->getInfo('manager'), 'Frontend');
            }
        } catch (\Exception $ex) {
            //Assert::contains($ex->getMessage(), 'not found');
            // se il sistema non è inizializzato allora viene preso Install
            $this->info = ['manager'=>'Install'];
        }
        $this->counter = new UUIDO\Maker(ltrim($this->getInfo('id'), '0'));
        $this->counter->setCounter(new UUIDO\Counter($this->getMain()->getCounterNext()));
        $this->manager = $this->makeManager($this->getInfo('manager'));
    }
    
    /**
     * Imposta l'applicazione corrente
     * @param type $application
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }
    
    
    /**
     * Restituisce la liste delle applicazioni attive
     */
    public function getApplicationList()
    {
        $list = array();
        $loader = new LoaderApplication();
        foreach($loader->loadAll($this->getDataSource('main'), $this->getDataSource()) as $adapter) {
            $app = new Application($this);
            $app->setAdapter($adapter);
            $app->setManager($this->getManager());
            $list[] = $app;
        }
        return $list;
    }
    /**
     * Ritorna l'applicazione corrente
     * @return Application
     */
    public function getApplication($name = '')
    {
        if (!$name) {
            Assert::notNull($this->application, 'Applicazione non ancora impostata');
            return $this->application;
        }
    
        $loader = new LoaderApplication();
        $app = new Application($this, $name);
        $app->setAdapter($loader->loadByName($this->getDataSource('main'), $name));
        $app->setManager($this->getManager());
        return $app;
    }
    
    /**
     * Se il manager non è impostato viene preso Frontend
     * @return Manager
     */
    public function getManager()
    {
        // recupero del manager individuato
        return $this->manager;
    }
    
    /**
     * 
     * @return Configuration
     */
    protected function getConfiguration()
    {
        return new Configuration();
    }
    /**
     * 
     * @return ServerRequest
     */
    public function getRequest()
    {
        return $this->request;
    }
    public function getInfo()
    {
        $args = func_get_args();
        if (count($args) == 0) {
            return $this->info;
        }
        return Util\arrayGet($this->info, $args[0]);
    }
    /**
     * 
     * @return Main
     */
    public function getMain()
    {
        return $this->main;
    }
    
    protected function makeManager($name)
    {
        return ManagerFactory::getManager($name, $this);
    }
    /**
     * Esegue il manager passandogli il path di sua competenza
     * @return type
     */
    public function run()
    {
        $manager = $this->getManager();
        //var_dump([$instancePath, $base, $path]);
        // sul manager qualche runner si prenota per poter essere eseguito?
        try {
            return call_user_func($manager->getRunner($this->getRequest()->getLast($this->getRoot())), new Response());
        } catch (NotFoundException $e) {
            
        }
        $response = $manager->run($this->getRequest());
        if (is_string($response)) {
            $response = (new ResponseHtml())->addContent($response);
        }
        if (!($response instanceof Response)) {
            $response = new Response($response);
        }
        return $response;
    }
    
    public function getRoot()
    {
        return $this->root;
    }
    public function makePath($url)
    {
        $base = $this->getInfo('url') ?: $this->getRoot();
        return '//'.trim($base, '/').'/'.ltrim($url, '/');
    }
    
    /**
     * Registra il datasource e fornisce la funzione di sostituzione del nome del DB e il contatore
     * @param type $name
     * @param type $DS
     */
    public function mapDataSource($name, $DS)
    {
        $this->DSMap[$name] = $DS;
        $DS->setCounter($this->getCounter());
        $DS->addNormalizer(function($code) {
            $list = [];
            foreach($this->DSMap as $name=>$DS) {
                $DBname = $DS->getName();
                if ($DBname) {
                    $DBname .= '.';
                }
                $list['{{%'.$name.'}}.'] = $DBname;
            }
            return str_replace(array_keys($list), array_values($list), $code);
        });
    }
    
    public function getDataSource($name = '')
    {
        return Util\arrayGet($this->DSMap, $name, Util\throwError("Data source non presente : ".$name));
    }
    
    public function getCounter()
    {
        return $this->counter;
    }
}
