<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Core;

use Spinit\Osy\Type\ApplicationInterface;
use Spinit\Osy\Type\FormAdapterInterface;
use Spinit\Osy\Http;

use Webmozart\Assert\Assert;
use Spinit\Osy\FieldBuilder\Html\Core as Component;
use Spinit\Osy\Type\FormInterface;

use Spinit\Util;
use Spinit\Osy\Core\Field;
use Spinit\Osy\Core\ComponentBuilder;

/**
 * Description of Form
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Form implements FormInterface
{
    use Util\TriggerTrait, Util\ExecCodeTrait;
    
    private $response;
    private $fieldList = array();
    private $fieldsRoot = array();
    private $componentBuilder;
    private $request;
    private $root;
    private $path;
    private $model;
    //private $adapter;
    private $pkey;
    public function __construct(ApplicationInterface $application)
    {
        $this->application = $application;
        $this->bindExec('@load', function () {
            $this->loadModelData();
        });
        $this->bindExec('@load-post', function ($event) {
            foreach($event->getParam(0) as $name => $value) {
                try {
                    $this->getField($name)->setValue(is_string($value) ? trim($value) : $value);
                } catch (\Exception $e) {

                }
            }
        });
        $this->bindExec('@init', function(){
            if (Util\arrayGet($this->path, 0)) {
                $this->pkey = json_decode(base64_decode(Util\arrayGet($this->path, 0)), 1);
                if (!$this->pkey) {
                    //throw new \Exception('Elemento non riconosciuto : '.Util\arrayGet($this->path, 0));
                    return;
                }
            }
            // carica nei campi i valori pervenuti
            $post = $this->getRequest()->getParsedBody();
            $osy = new Util\Dictionary(Util\arrayGet($post, '_'));
            
            unset ($post['_']);
            $this->trigger('@load-pre', [$post, $osy]);
            if (!$osy->get('osy.init')) {
                $this->trigger('@load', [$post, $osy]);
            } else {
                $this->trigger('@load-no', [$post, $osy]);
            }
            $this->trigger('@load-post', [$post, $osy]);
        });
        
        $this->bindExec('@build', function(){
            $response = $this->getResponse();
            $response->addContent('mainContent', $this->build());
            $response->addData('command','$(window).trigger("initData");');
        });
        $this->bindCommand();
    }
    public function getPkey()
    {
        return $this->pkey;
    }
    private function loadModelData()
    {
        if (!$model = $this->getModel() or !$this->pkey) {
            return;
        }
        $model->trigger('load', [$this->pkey]);
        // carica nei campi presenti nel model
        foreach($this->fieldList as $name => $cmp) {
            if ($field = $cmp->get('field')) {
                $cmp->setValue($model->get($field));
            }
        }
    }
    private function bindCommand()
    {
        $this->bindExec('save', function($event) {
            if ($model = $this->getModel()) {
                $model->clear()->setPkey($this->pkey);
                foreach($this->fieldList as $name => $cmp) {
                    if ($field = $cmp->get('field')) {
                        $model->set($field, $cmp->getBuilder()->getValue($cmp));
                    }
                }
                $model->bindExec('insert', function() {
                   $this->trigger('@insert'); 
                });
                $model->bindExec('update', function() {
                   $this->trigger('@update'); 
                });
                $model->trigger('save');
                $urlDetail = $this->getUrl().'/'.base64_encode(json_encode($model->getPkey()));
                $this->getResponse()->addCommand("if (window.history.state) {window.history.state.form = {}}; window.history.replaceState(window.history.state, null, arguments[0])", $urlDetail);
            }
            if ($event->getParam(0) == 'back') {
                $this->getResponse()->addCommand("window.history.back()");
            }
        });
        $this->bindExec('delete', function($event) {
            if ($model = $this->getModel()) {
                $model->clear()->setPkey($this->pkey);
                $model->trigger('delete');
            }
            $this->getResponse()->addCommand("window.history.back()");
        });
    }
    public function getName()
    {
        return $this->getAdapter()->getName();
    }
    
    public function getFullName()
    {
        return $this->getAdapter()->getFullName();
    }
    public function setRoot($root)
    {
        $this->root = $root;
        return $this;
    }
    public function getRoot()
    {
        return $this->root ? : $this->getUrl();
    }
    public function getModel()
    {
        if (!$this->model) {
            $modelName = $this->getAdapter()->get('model');
            if (!$modelName) {
                return null;
            }
            $this->model = $this->getApplication()->getModel($modelName);
        }
        return $this->model;
    }
    /**
     * 
     * @param Http\Response $response
     */
    public function setResponse(Http\Response $response)
    {
        $this->response = $response;
        return $this;
    }
    
    /**
     * 
     * @return Http\Response
     */
    public function getResponse()
    {
        return $this->response;
    }
    
    /**
     * Esegue il codice registrato sull'adapter per quel particolare evento
     * @param type $event
     * @param type $param
     */
    protected function onTrigger($event, $param = array()) {
        $this->execCode(
            $this->getAdapter()->getTrigger($event['name'], $event['when']),
            array('parameters' => $param, 'event' => $event),
            $this->getFullName().'-'.$event['name'].'-'.$event['when'].'-'
        );
    }
    
    /**
     * Imposta l'adapter e ne carica la struttura dati
     * @param FormAdapterInterface $adapter
     * @return $this
     */
    public function setAdapter(FormAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->componentBuilder = $adapter->getComponentBuilder();
        $this->fieldList = array();
        foreach($adapter->getFields() as $child) {
            $field = new Field($this, $child);
            $this->fieldsRoot[] = $field->getName();
            $this->registerField($field);
        }
        
        return $this;
    }
    
    private function registerField(Field $field)
    {
        $this->fieldList[$field->getName()] = $field;
        foreach($field->getChilds() as $child) {
            $this->registerField($child);
        }
    }
    
    /**
     * 
     * @param type $name
     * @return Field
     */
    public function getField($name)
    {
        return Util\arrayGet($this->fieldList, $name, Util\throwError("Componente non trovato : ".$name));
    }
    
    public function getFields()
    {
        return $this->fieldList;
    }
    
    /**
     * Ritorna l'insieme dei "valori" memorizzati nella form
     * @return array
     */
    public function getValues()
    {
        $values = [];
        foreach($this->getFields() as $name=>$field) {
            $values[$name] = $field->getValue();
        }
        return $values;
    }
    /**
     * 
     * @return FormAdapterInterface
     */
    public function getAdapter()
    {
        Assert::notNull($this->adapter);
        return $this->adapter;
    }
    
    /**
     * 
     * @return ApplicationInterface
     */
    public function getApplication()
    {
        return $this->application;
    }
    
    /**
     * 
     * @return Request
     */
    public function getRequest()
    {
        if ($this->request) {
            return $this->request;
        }
        return $this->getApplication()->getInstance()->getRequest();
    }
    /**
     * Esecuzione Form
     * @return Response
     */
    public function run($request = null, $path = array())
    {
        $this->request = $request;
        $this->path = $path;
        $event = '';
        $is_GET = $this->getRequest()->getMethod() == 'GET';
        if ($is_GET and $this->getAdapter()->hasStruct()) {
            $this->setResponse($this->makeHtmlResponse());
        } else {
            $event = ltrim($this->getRequest()->getHeaderLine('X-Opensymap-Command'), '@');
            $response = $this->makeJsonResponse();
            $this->setResponse($response);
            $this->trigger('@init');
            if ($this->getAdapter()->get('setUrl')!='no') {
                $response['url'] = $this->getUrl();
            }
            $param = new Util\Dictionary(Util\arrayGet($this->getRequest()->getParsedBody(), '_', Array()));
            $field = $this->getRequest()->getHeaderLine('X-Opensymap-Field');

            if ($field) {
                $this->getField($field)->trigger(Util\nvl($event, '@build'), json_decode($param->get('osy.param'), 1));
            } else {
                $this->trigger(Util\nvl($event, '@build'), json_decode($param->get('osy.param'), 1));
            }
        }
        
        return $this->getResponse();
    }
    
    public function getUrl()
    {
        $url = $this->getApplication()->getUrl($this->getFullName());
        if ($this->pkey) {
            $url .= base64_encode(json_encode($this->pkey)).'/';
        }
        return $url;
    }
    private function makeHtmlResponse()
    {
        $response = new Http\ResponseHtml();
        $response->trigger('setTemplate', [$this->getApplication()->getManager()->getTemplate()]);
        return $response;
    }
    private function makeJsonResponse()
    {
        return new Http\Response();
    }
    
    /**
     * In fase di build occorre disegnare i vari componenti della form.
     * Per fare questo viene richiesto il gestore dei builder all'adapter.
     * Questo gestore verrà usato dal form man mano che i field richiederanno
     * quale builder è delegato alla propria formattazione.
     * 
     * @return type
     */
    public function build()
    {
        // richiesta del gestore dei builder
        $content = '';
        if ($code = $this->getAdapter()->getBuilderCode()) {
            $content = $this->execCode($code);
        } else {
            $builder = $this->getFormBuilder();
            foreach($this->fieldsRoot as $name) {
                $builder->add($this->getField($name)->build());
            }
            $content = $builder->build($this);
        }
        return $content;
    }
    
    public function getFormBuilder()
    {
        try {
            return $this->componentBuilder->getForm();
        } catch (\Exception $ex) {
            return $this->getApplication()->getFormBuilder();
        }
    }
    
    public function getFieldBuilder($type)
    {
        try {
            return $this->componentBuilder->get($type);
        } catch (\Exception $ex) {
            return $this->getApplication()->getFieldBuilder($type);
        }
    }
    
    public function getDatasource($name = '')
    {
        return $this->getApplication()->getDataSource($name);
    }
}
