<?php
namespace Spinit\Util;

function is_binary($str) {
    $ret = false;
    if (function_exists('\\is_binary')) {
        $ret = \is_binary($str);
    }
    if (!$ret) {
        return (substr_count($str, "\x00") > 0 or json_encode($str) === false);
    }
    return true;
}


function each($dataset, $serializer)
{
    foreach($dataset as $idx => $record)
    {
        call_user_func($serializer, $idx, $record);
    }
}

function getList($dataset)
{
    $list = [];
    each($datset, function($idx, $record) use (&$list) {
        $keys = array_keys($record);
        switch (count($keys)) {
            case 1:
                $list[] = $record[$keys[0]];
                break;
            case 2 :
                $list[$keys[0]] = $record[$keys[1]];
                break;
            default :
                $list[$keys[0]] = $record;
                break;
        }
    });
    return $list;
}

  class Table extends Tag{
     
     private $CurRow;
     private $CurCel;
     public function __construct(){
        parent::__construct('table');
     }
     
     public function Row($v=null){
        if (!empty($v)){
            $this->CurRow->Add($v);
        } else {
            $this->CurRow = parent::Add(new Tag('tr'));
        }
        return $this->CurRow;
     }
     public function Add($o, $d = 'last')
     {
        if (!$this->CurCel) $this->Cell($o);
        else $this->CurCel->Add($o, $d);
        return $o;
     }
     public function AddTop($o)
     {
        if (!$this->CurCel) $this->Cell($o);
        else $this->CurCel->AddTop($o);
        return $o;
     }
     public function Head($val=null){
        return $this->Cell($val,'th');
     }
     
     public function GetRow(){
        return $this->CurRow;
     }
     public function GetCell(){
        return $this->CurCel;
     }
     
     public function Cell($val=null, $typ='td'){
        if (empty($this->CurRow)){
            $this->Row();
        }
        $this->CurCel = $this->CurRow->Add(new Tag($typ));
        if ($val === '0' or !empty($val)){
            $this->CurCel->Add($val);
        }
        return $this->CurCel;
     }
     
     public function CellXY($val=null,$xr,$yc){
        if (!is_object($this->Cnt[$xr])){
            $this->CurRow = $this->Cnt[$xr] = new Tag('tr');
        }
        $t = $this->Cnt[$xr]->Add(new Tag('td'),$yc)->Add($val);
        return $t;
     }
  }