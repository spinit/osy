<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy;

use Spinit\Osy\Http\ServerRequest;
use Spinit\Osy\Core\Instance;
use Spinit\Datasource\DataSource;
use Spinit\Util;
use Spinit\Osy\Http\Response;

use Spinit\Util\Error\StopException;
use Spinit\Osy\Helper\Register;

use Webmozart\Assert\Assert;

use Spinit\Datasource\Core\DataSetInterface;
use Spinit\Datasource\DataSetArray;
use Spinit\Util\Error\UtilException;
use Spinit\Osy\Error\MessageException;
use LogicException;
use Spinit\Osy\Core\Application;

/**
 * Main è la classe di ingresso per individuale il controller che dovrà essere chiamato per processare la richiesta.
 * Per fare questo deve effettuare il seguenti passaggi:
 * - individuare l'istanza individuata dalla prima parte della richiesta
 * - individuare il manager che dovrà continuare il processo di ricerca. Esso con la parte restante della uri dovrà:
 *      > individuare l'applicazione e il form in caso di Backend OR
 *      > individuare il controller nel caso di Frontend
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Main
{
    use \Spinit\Util\TriggerTrait;
    /**
     *
     * @var ServerRequest
     */
    private $request;
    
    /**
     *
     * @var string
     */
    private $connectionString = '';
    
    private $sessionActive = false;
    /**
     *
     * @var DataSource
     */
    private $datasource;
    
    /**
     *
     * @var Instance
     */
    private $instance;
    
    /**
     *
     * @var Register
     */
    private $appRegister;
    
    private $counterNext;
    
    private $listApp = array(
        array('uri'=>'uri:opensymap:opensymap.org#System', 'typ'=>'FSXml', 'nme'=>'System', 'src'=>'/app/System', 'auto'=>'1'),
        array('uri'=>'uri:opensymap:opensymap.org#Blog', 'typ'=>'FSXml', 'nme'=>'Blog', 'src'=>'/app/Blog', 'auto'=>'1'),
    );
    
    public function __construct($connectionString = '', $sessionActive = true)
    {
        $this->sessionActive = $sessionActive;
        $this->connectionString = Util\nvl($connectionString, Util\getenv('CONNECTION_STRING'));
        if (!defined('CONNECTION_STRING')) {
            define('CONNECTION_STRING', $this->connectionString);
        }
        $this->datasource = null;
        $this->initRegister();
        $this->initAutenticationUser();
        try {
            $this->initRedisCounter();
        } catch (\Exception $e) {
            Util\console("Redis Counter", Util\nvl(Util\getenv('COUNTER_HOST'), 'counter'), $e->getMessage());
        }
    }
    
    /**
     * L'autenticazione è affidata alla valorizzazione di 2 variabili di ambiente.
     * Se è attiva la sessione queste variabili vengono prese dalla sessione.
     */
    private function initAutenticationUser()
    {
        if ($this->sessionActive) {
            Util\getenv('USER_ID',      Util\Session::get('USER_ID'));
            Util\getenv('USER_LOGIN',   Util\Session::get('USER_LOGIN'));
            Util\getenv('USER_NAME',    Util\Session::get('USER_NAME'));
        }
        
        $this->bindBefore('setAuthUser', function($event) {
            Util\getenv('USER_ID', $event->getParam(0));
            Util\getenv('USER_LOGIN', $event->getParam(1));
            Util\getenv('USER_NAME', $event->getParam(2));
            
            if ($this->sessionActive) {
                Util\Session::set('USER_ID', Util\getenv('USER_ID'));
                Util\Session::set('USER_LOGIN', Util\getenv('USER_LOGIN'));
                Util\Session::set('USER_NAME', Util\getenv('USER_NAME'));
            }
        });
        
    }
    private function initRedisCounter()
    {
        // impostazione conter redis
        Assert::classExists('\\Redis', 'Redis non installato');
        $redis = new \Redis();
        $conf = Util\asArray(Util\nvl(getenv('COUNTER_HOST'), 'counter'), ':');
        Assert::true(@$redis->connect($conf[0], $conf[1]), 'Connessione a redis non possibile');
        
        $clean = function($prefix, $node) use ($redis) {
            foreach ($redis->hkeys($node) as $key) {
                if ($key != $prefix) {
                    $redis->hdel($node, $key);
                }
            }
        };
        $nexter = function ($prefix, $node) use ($redis, $clean) {
            // si chiede a redis il valore successivo di $prefix sull'insieme $node
            $val = $redis->hIncrBy($node, $prefix, 1);
            Util\console('REDIS VALUE', $val, $node, $prefix);
            // se $node ha più di 10 $prefix allora vengono tolti
            if ($redis->hLen($node) > 10) {
                // altrimenti vengono tolti i prefix che non servono più
                $clean($node, $prefix);
            }
            return $val;
        };
        $this->setCounterNext($nexter);
    }
    
    /**
     * 
     */
    private function initRegister()
    {
        $this->appRegister = new Register();
        $listApp = array();
        // prime app a disposizione
        foreach($this->listApp as $app) {
            $app['src'] = dirname(__DIR__).$app['src'];
            $this->appRegister->addItem(md5($app['uri']), $app);
        }
    }
    /**
     * Method factory per la generazione della richiesta Server
     * @return RequestServer
     * @codeCoverageIgnore
     */
    protected function makeRequest()
    {
        return ServerRequest::fromGlobals();
    }
    
    /**
     * Genera l'istanza e la imposta come corrente
     * @param type $request
     * @return \Spinit\Osy\Instance\InstanceInstall
     */
    public function makeInstance($request)
    {
        $this->instance = new Instance($this, $request);
        return $this->instance;
    }
    /**
     * Ritorna l'ultima istanza creata o ne crea una nel caso manchi
     * @return Instance
     */
    public function getInstance($request = null)
    {
        if (!$this->instance or $request) {
            $this->makeInstance($request ? $request : $this->getRequest());
        }
        return $this->instance;
    }
    
    /**
     * 
     * @return ServerRequest
     */
    public function getRequest()
    {
        if (!$this->request) {
            $this->request = $this->makeRequest();
        }
        return $this->request;
    }
    
    /**
     * Determina l'istanza e la esegue. In caso di errore lo mostra.
     * @return type
     */
    public function run()
    {
        
        $this->trigger('init');
        try {
            $response = $this->getInstance()->run();
        } catch (StopException $e) {
            $this->trigger('stop', [$e]);
            return ob_get_clean();
        } catch (UtilException $e) {
            $response = new Response(['status'=>'error', 'message'=>$e->getMessage()]);
            $this->trigger('error', [$response]);
        } catch (MessageException $e) {
            $response = new Response(['status'=>'error', 'message'=>$e->getMessage()]);
            $this->trigger('error', [$response]);
        } catch (LogicException $e) {
            $response = new Response(['status'=>'error', 'message'=>$e->getMessage()]);
            $this->trigger('error', [$response]);
        } catch (\Exception $e) {
            $response = new Response(['status'=>'error', 'message'=>$e->getMessage(), 'trace'=>explode("\n",$e->getTraceAsString())]);
            $this->trigger('error', [$response]);
        }
        $this->trigger('finish');
        return $response;
    }
    
    public function getConnestionString()
    {
        return $this->connectionString;
    }

    public function getDataSource()
    {
        if (!$this->datasource) {
            $this->datasource = new DataSource($this->connectionString);
        }
        return $this->datasource;
    }
    
    public function getAppRegister()
    {
        return $this->appRegister;
    }
    
    public function getCounterNext()
    {
        return $this->counterNext;
    }
    
    public function setCounterNext($nexter)
    {
        $this->counterNext = $nexter;
    }
}
