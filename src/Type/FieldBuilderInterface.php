<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Type;

use Spinit\Osy\Type\FieldAdapterInterface;
use Spinit\Osy\Type\FieldInterface;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface FieldBuilderInterface
{
    public function build(FieldInterface $field, $event = null);
    public function trigger($name);
    public function getValue(FieldInterface $field);
}
