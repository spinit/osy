<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Type;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

interface ApplicationAdapterInterface
{
    public function getForm($name);
    public function getModel($name);
    public function getModelNameList();
    public function getComponentBuilder();
    public function getRouteList();
    public function getSourceCode($name);
    public function getFormNameDefault();
    public function getLabel();
    public function getName();
    public function getMenuList();
}
