<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Type;

use Spinit\Datasource\DataSource;
use Spinit\Osy\Type\ModelAdapterInterface;
use Spinit\Osy\Type\ApplicationInterface;
/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface ModelInterface {
    
    public function get($name);
    
    public function set($name, $value);
    
    /**
     * 
     * @param DataSource $datasource
     */
    public function setDataSource(DataSource $datasource);
    
    /**
     * 
     * @return DataSource
     */
    public function getDataSource();
    
    /**
     * 
     * @param ModelAdapterInterface $adapter
     */
    public function setAdapter(ModelAdapterInterface $adapter);
    
    /**
     * 
     * @return ModelAdapterInterface
     */
    public function getAdapter();
    
    /**
     * 
     * @return ApplicationInterface
     */
    public function getApplication();
    
    /**
     * Pulisce tutti i campi interni riportandolo ad un elemento vuoto
     */
    public function clear();
}
