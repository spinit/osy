<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Type;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */

interface FormAdapterInterface
{
    public function hasStruct();
    public function getTrigger($event, $when);
    public function getBuilderCode();
    public function getFields();
    public function getName();
    public function getFullName();
    public function getComponentBuilder();
}
