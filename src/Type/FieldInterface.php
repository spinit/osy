<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Type;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface FieldInterface
{
    public function get($name);
    public function getName();
    public function getChilds();
    public function getValue();
    public function setValue($value);
    public function getForm();
}
