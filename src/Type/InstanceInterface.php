<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Type;

/**
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
interface InstanceInterface
{
    public function getRequest();
    public function getRoot();
    public function getInfo();
    public function getMain();
    public function getManager();
    public function run();
}
