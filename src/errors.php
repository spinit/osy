<?php

namespace Spinit\Osy\Error;

use Spinit\Util\Error\NotFoundException;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class RouteNotFoundException extends NotFoundException {};

class MessageException extends \Exception {};