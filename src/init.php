<?php


namespace Spinit\Osy;

Core\Manager\Factory::registerManagerRouter('Backend,Install', function($instance, $path) {
    $root = '__asset/opensymap/';
    if (substr($path, 0, strlen($root)) == $root) {
        $pathManager = substr($path, strlen($root));
        return new Helper\AssetRouter($instance, __OSY__. '/Manager/Backend/asset/', $pathManager);
    }
});
