<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Helper;

/**
 * Matcher ha il compito di mappare una serie serie di funzioni da dover 
 * poi eseguire se la chiave corrisponde ad una data stringa
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Matcher
{
    use \Spinit\Util\TriggerTrait;
    
    private $data = array();
    private $patternPlaceholder = array(
        '?i' => '([\\d]+){1}', 
        '?w' => '([a-zA-Z][\\w]*){1}', 
        '?s' => '([\\w]+){1}', 
        '*'  => '(.*){1}', 
        '/'  => '\\/'        
    );
    public function add($url, $callable, $idx = 0)
    {
        $this->data[$url][$idx][] = $callable;
    }
    /**
     * Esegue la funzione indicata passandogli i controller che si sono registrati sul path
     * @param type $path
     * @return Route
     */
    public function exec($path)
    {
        $path = '/'.ltrim($path, '/');
        $priority = array();
        foreach(array_keys($this->data) as $url) {
            $allowedParamChars = '[a-zA-Z0-9\_\-]+';
            //sostituzione del placeolder che mappa il parametro con un nome
            $pattern = preg_replace(
                '/:(' . $allowedParamChars . ')([^\/]*)/',   # Replace ":parameter"
                '(?<$1>$2)', # with "(?<parameter>)"
                $url
            );
            $pattern = str_replace(
                array_keys($this->patternPlaceholder),
                array_values($this->patternPlaceholder),
                $pattern
            );
            // se il pattern non viene riconosciuto
            $esito = @preg_match('|^'.$pattern.'$|', $path, $matches);
            if (!$esito) {
                continue;
            }
            $first = array_shift($matches);
            foreach($this->data[$url] as $list) {
                foreach($list as $key => $call) {
                    $priority[$key][] = [$call, $matches, $path, $pattern];
                }
            }
        }
        if (!count($priority)) {
            return $this->trigger('notfound');
        }
        ksort($priority);
        $ex = [];
        $ca = 0;
        foreach($priority as $list) {
            foreach($list as $caller) {
                $ca+=1;
                try {
                    call_user_func($caller[0], $caller[1]);
                } catch (Spinit\Util\Error\NotFoundException $e) {
                    $ex[] = $e;
                }
            }
        }
        // se nessuno dei controller ha saputo gestire il path ... allora viene lanciata l'ultima eccezione
        if (count($ex) == $ca) {
            throw array_pop($ex);
        }
    }
}
