<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Helper;

use Spinit\Datasource\Core\DataSetInterface;
use Spinit\Datasource\DataSetArray;

use Spinit\Util;
use Spinit\Util\Error\FoundException;
use Spinit\Osy\Type\RegisterInterface;

use Webmozart\Assert\Assert;
/**
 * Registro gerarchico.
 * Permetted i poter registrare sia elementi che contenitori di elementi.
 * Ogni contenitore (Register è un contenitore) associa ad ogni elemento una chiave.
 * Quanto viene richiesto un elemento, viene cercato tra i propri altrimenti viene
 * richiesto ai contenitori.
 * Quando vengono richiesti tutti gli elementi allora viene fatto un merge tra tutti
 * i contenitori
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Register implements RegisterInterface
{
    private $items = array();
    private $containers = array();
    
    public function addItem($name, $item)
    {
        $this->items[$name] = $item;
        return $this;
    }
    public function addRegister(RegisterInterface $register)
    {
        $this->containers[] = $register;
        return $register;
    }
    
    public function getItems()
    {
        $result = $this->items;
        foreach($this->containers as $register) {
            foreach($register->getItems() as $name => $item) {
                $result[$name] = $item;
            }
        }
        return new DataSetArray($result);
    }
    
    public function getItem($name)
    {
        return Util\arrayGet($this->items, $name, function() use ($name) {
            try {
                foreach($this->containers as $register) {
                    if($found = $register->getItem($name)) {
                        throw new FoundException($found);
                    }
                }
            } catch (FoundException $ex) {
                return $ex->getMessage();
            }
        });
    }
    
    /**
     * Funzione di mascheramento che la rende compatibile con un datasource
     * @param type $resource
     * @param type $field
     * @param type $pkey
     * @return type
     */
    public function find($resource, $field, $pkey)
    {
        return $this->getItem($pkey);
    }
}
