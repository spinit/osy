<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Helper;
use Spinit\Util;

/**
 * Description of ScanModules
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ScanDirModules
{
    public function getItemList($root, $type = '')
    {
        $result = array();
        $dir = Util\sanitizePath($root);
        if (!is_dir($dir) or !($dh = opendir($dir))) {
            return $result;
        }
        while (($file = readdir($dh)) !== false) {
            if (!($fname = $this->check($dir, $file, $type))) {
                continue;
            }
            if (empty($type)) {
                $result[] = $fname;
            } else {
                foreach($this->getItemList($root.'/'.$fname.'/'.$type) as $item) {
                    $result[] = $fname.':'.$item;
                }
            }
        }
        closedir($dh);
        return $result;
    }
    
    private function check($dir, $file, $type)
    {
        if (in_array($file, array('.', '..'))) {
            return false;
        }
        $dest = Util\sanitizePath($dir.'/'.$file);
        if (is_file($dest)) {
            if ($type) {
                return false;
            }
            $finfo = pathinfo($dest);
            return $finfo['filename'];
        }
        return $file;
    }
}
