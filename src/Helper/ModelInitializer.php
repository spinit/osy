<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Helper;

use Spinit\Util;

/**
 * Description of ModelInitializer
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ModelInitializer
{
    private $DSapp;
    private $DSmain;
    
    public function __construct($DSapp, $DSmain)
    {
        $this->DSapp = $DSapp;
        $this->DSmain = $DSmain;
    }
    
    public function align($model)
    {
        $tables = $this->analize((string)$model->getAdapter()->get('resource'), $model->getFields());
    }
    
    private function analize($resource, $fields)
    {
        $tables = [];
        foreach($fields as $field) {
            $type = $this->loadType($this->DSapp->getInfo(0), Util\arrayGet($field, 'type'));
        } 
    }
}
