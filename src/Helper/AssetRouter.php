<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Osy\Helper;

use Spinit\Util;
use Spinit\Util\Error\StopException;
use Spinit\Util\Error\NotFoundException;
use Spinit\Osy\Type\InstanceInterface;

/**
 * Description of AssetRouter
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class AssetRouter {
    
    private $root;
    private $path;
    private $instance;
    
    private $mime = array(
        'png'=>'image/png',
        'jpg'=>'image/jpg',
        'jpeg'=>'image/jpeg',
        'txt'=>'text/plain',
        'css'=>'text/css',
        'js'=>'application/javascript',
        'woff'=>'application/x-font-woff',
        'woff2'=>'application/x-font-woff2'
    );
    public function __construct(InstanceInterface $instance, $root, $path)
    {
        $this->root = rtrim(Util\sanitizePath($root), DIRECTORY_SEPARATOR);
        $pathArg = explode('?', $path);
        $this->path = Util\sanitizePath($pathArg[0]);
        $this->instance = $instance;
    }
    
    /**
     * 
     * @return InstanceInterface
     * @codeCoverageIgnore
     */
    public function getInstance()
    {
        return $this->instance;
    }
    public function __invoke($response)
    {
        $filepath = DIRECTORY_SEPARATOR . ltrim($this->path, DIRECTORY_SEPARATOR);
        $fname = $this->root. $filepath;
        $finfo = \pathinfo($fname);
        $finfo['path'] = '';
        if (\is_file($fname)) {
            return $this->loadFile($fname, $finfo, $response);
        }
        return $this->searchFile($finfo, $response);
    }
    
    private function searchFile($pathInfo, $response)
    {
        $basename = $pathInfo['dirname']. DIRECTORY_SEPARATOR . $pathInfo['filename'] .'.php';
        if (\is_file($basename)) {
            $arguments = array_filter(explode(DIRECTORY_SEPARATOR, $pathInfo['path']), 'strlen');
            include($basename);
            return $response;
        }
        $pathInfo['path'] = $pathInfo['basename'] . ($pathInfo['path'] ? DIRECTORY_SEPARATOR : '') . $pathInfo['path'];
        $pathInfo['basename'] = $pathInfo['filename'] = basename($pathInfo['dirname']);
        $pathInfo['dirname'] = dirname($pathInfo['dirname']);
        if (strlen($pathInfo['dirname']) > strlen($this->root)) {
            return $this->searchFile($pathInfo, $response);
        }
        throw new NotFoundException();
    }
    
    private function loadFile($fname, $pathInfo, $response)
    {
        switch($pathInfo['extension']) {
            case 'php':
                $arguments = array_filter(explode(DIRECTORY_SEPARATOR, $pathInfo['path']), 'strlen');
                include($fname);
                return $response;
            default:
                $mime = Util\arrayGet($this->mime, $pathInfo['extension']);
                if ($mime) {
                    header('Content-Type: '.$mime);
                }
                readfile($fname);
                throw new StopException();
        }
        
    }
}
