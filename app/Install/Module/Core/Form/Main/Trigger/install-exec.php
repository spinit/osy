<?php
namespace Spinit\Osy\app\Install;

use Spinit\UUIDO;
use Spinit\Osy\Http\ServerRequest;
use Spinit\Datasource\DataSource;
use Spinit\Osy\Core\Instance;

use Spinit\Util;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// verifica dei dati 
// TODO: occorre che i field validino l'input secondo le indicazioni passate nell'xml e mettano
// la form in stato di error. Se la Form è in error allora il comando richiesto non potrà essere eseguito.
// sarebbe carino peter impostare sulla form i diversi insiemi di validazione per i diversi comandi impartiti

$response = $this->getResponse();
if (!$this->getField('txt_username')->getValue()) {
    $response->addError('txt_username',"Nome non impostato");
    return;
}
if (!$this->getField('txt_passwd')->getValue()) {
    $response->addError('txt_passwd',"Password non impostata");
    return;
}
if ($this->getField('txt_passwd')->getValue() != $this->getField('txt_passwd2')->getValue()) {
    $response->addError('txt_passwd2',"Password non uguali");
    return;
}

// verifica dell'esistenza (o della creazione senza errori) del DB
// e mappaggio con il nome <empty>
try {
    $this->getApplication()
        ->getInstance()
        ->mapDataSource('', new DataSource($this->getField('txt_strcon')->getValue()));
} catch (\Exception $e) {
    $response->addError('txt_strcon',"Impossibile collegarsi al DB");
    return;
}

$instance = $this->getApplication()->getInstance();

// reset autenticazioni precedenti
$instance->getMain()->trigger('setAuthUser');

// inizializzazione  database master + istanza
$app = $this->getApplication()->getManager()->getSystem()->init();

// Aree di accesso utente
$urlUser = array();

// Creazione istanza
$ice = $app->getModel('Instance');
$ice->set('con_str', $this->getField('txt_strcon')->getValue());
$ice->set('act', '1');
$ice->trigger('save');
// Creazione Url di accesso
$iceBe = $app->getModel('InstanceUrl');
$iceBe->set('id_ice', $ice->get('id'));
$iceBe->set('url', $this->getField('txt_backend')->getValue());
$iceBe->set('act', '1');
$iceBe->set('mng', 'Backend');
$iceBe->trigger('save');
$urlUser[] = (string) $iceBe->get('id');
if ($this->getField('txt_frontend')->getValue()) {
    $iceFe = $app->getModel('InstanceUrl');
    $iceFe->set('id_ice', $ice->get('id'));
    $iceFe->set('url', $this->getField('txt_frontend')->getValue());
    $iceFe->set('act', '1');
    $iceFe->set('mng', 'Frontend');
    $iceFe->trigger('save');
    // se l'utente è abilitato
    if ($this->getField('rd_frontend')->getValue()) {
        $urlUser[] = (string) $iceFe->get('id');
    }
}
// init applicazioni registrate
$osyApp = $app->getModel('Application');
$iceApp = $app->getModel('ApplicationIce');
foreach($instance->getMain()->getAppRegister()->getItems() as $key => $item) {
    // occorre impostare a null la chiave per poter riutilizzare il modello per più registrazioni
    $osyApp->clear();
    $osyApp->set($item);
    $osyApp->set('id', $key);
    $osyApp->trigger('save');
    
    $iceApp->clear();
    $iceApp->set('id', $osyApp->get('id'));
    $iceApp->set('nme', $osyApp->get('nme'));
    $iceApp->set('act', '1');
    $iceApp->trigger('save');
}

$iceCur = new Instance($instance->getMain(), new ServerRequest('POST', $this->getField('txt_backend')->getValue()));

$iceCur->run();

// Registrazione utente Root
$user = $iceCur->getApplication()->getModel('Anag');
$user->set('eml_1', $this->getField('txt_username')->getValue());
$user->set('aut_act', '1');
$user->set('aut_lgn', $this->getField('txt_username')->getValue());
$user->set('aut_pwd', md5($this->getField('txt_passwd')->getValue()));
$user->set('aut_role', md5('uri:opensymap:opensymap.org#System@role:root'));
$user->trigger('save');

$role = $iceCur->getApplication()->getModel('Role');
foreach($this->getDatasource()->load("select hex(id) as id from ".$role->getAdapter()->get('resource')) as $item) {
    $role->trigger('load', [$item['id']]);
    $role->set('urls', array_merge(Util\nvl($role->get('urls'),[]), $urlUser));
    $role->trigger('save');
}
$this->getResponse()->setData("href", '//'.$this->getField('txt_backend')->getValue());
