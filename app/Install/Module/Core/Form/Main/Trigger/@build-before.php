<?php

use Spinit\Util;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$request = $this->getRequest();
$pathBase = (string) $request->getRoot();
$arName = explode('/', trim($request->getPath(), '/'));
$iceName = array_shift($arName);
if ($iceName) {
    $pathBase .= '/'.$iceName;
}
$this->getField('txt_backend')->setValue($pathBase.'/osy');
$this->getField('txt_frontend')->setValue($pathBase);
$strcon = explode(':', CONNECTION_STRING);
if (in_array($strcon[0], ['mysql', 'mysqli'])) {
    $strcon[2] .= "_{$iceName}";
}
$this->getField('txt_strcon')->setValue(implode(':', $strcon));