<?php

use Spinit\Util;
use Spinit\Osy\Core\Manager\Frontend;
use Spinit\Osy\Core\Manager\FrontendController;
use Spinit\Util\Error\NotFoundException;
/**
 * Description of Index
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Main extends FrontendController
{
    public function init()
    {
        $this->getManager()->bindExec('index.begin', function() {
            if($this->getArgs(0)) {
                throw new NotFoundException();
            }
        });
        $this->getManager()->bindExec('body.name', 'Hello world');
        $this->getManager()->bindExec('body.menu', function() { return $this->makeMenu();});
    }
    
    private function makeMenu()
    {
        return json_encode($this->getArgs());
    }
}
