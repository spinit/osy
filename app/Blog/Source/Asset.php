<?php

use Spinit\Util;
use Spinit\Osy\Core\Manager\Frontend;
use Spinit\Osy\Core\Manager\FrontendController;
use Spinit\Util\Error\NotFoundException;
use Spinit\Util\Error\StopException;

/**
 * Description of Index
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Asset extends FrontendController
{
    
    private $mime = array(
        'png'=>'image/png',
        'jpg'=>'image/jpg',
        'jpeg'=>'image/jpeg',
        'txt'=>'text/plain',
        'css'=>'text/css',
        'js'=>'application/javascript',
        'woff'=>'application/x-font-woff',
        'woff2'=>'application/x-font-woff2'
    );
    
    public function init()
    {
        $this->getManager()->bindBefore('index.begin', function() {
            try {
                $path = $this->getArgs(0);
            } catch (\Exception $e) {
                $path = $this->getPath();
            }
            list($fname,) = explode('?', $this->getManager()->getRoot().$path);
            $finfo = \pathinfo($fname);
            $finfo['path'] = '';
            if (\is_file($fname)) {
                return $this->loadFile($fname, $finfo, $path);
            }
            // favicon non trovato
            if ($path = 'favicon.ico') {
                die('');
            }
            return $this->searchFile($finfo, $path);
        });
    }
    
    private function searchFile($pathInfo, $path)
    {
        $basename = $pathInfo['dirname']. DIRECTORY_SEPARATOR . $pathInfo['filename'] .'.php';
        if (\is_file($basename)) {
            $arguments = array_filter(explode(DIRECTORY_SEPARATOR, $pathInfo['path']), 'strlen');
            include($basename);
            throw new StopException();
        }
        $pathInfo['path'] = $pathInfo['basename'] . ($pathInfo['path'] ? DIRECTORY_SEPARATOR : '') . $pathInfo['path'];
        $pathInfo['basename'] = $pathInfo['filename'] = basename($pathInfo['dirname']);
        $pathInfo['dirname'] = dirname($pathInfo['dirname']);
        if (strlen($pathInfo['dirname']) > strlen($this->getManager()->getRoot())) {
            return $this->searchFile($pathInfo, $path);
        }
        throw new NotFoundException($path);
    }
    
    private function loadFile($fname, $pathInfo, $path)
    {
        switch($pathInfo['extension']) {
            case 'php':
                $arguments = array_filter(explode(DIRECTORY_SEPARATOR, $pathInfo['path']), 'strlen');
                ob_start();
                include($fname);
                break;
            default:
                $mime = Util\arrayGet($this->mime, $pathInfo['extension']);
                if ($mime) {
                    header('Content-Type: '.$mime);
                }
                readfile($fname);
                break;
        }
        throw new StopException();
    }
}
