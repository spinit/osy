<?php
use Spinit\Util;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ($ice = Util\arrayGet($this->getRequest()->getParsedBody(), 'hdn_ice')) {
    $instance = $this->getApplication()->getInstance();
    $DSM = $instance->getDatasource('main');
    $ice = $DSM->find('osx_ice', 'con_str', $ice);
    $instance->mapDataSource('', new \Spinit\Datasource\DataSource($ice['con_str']));
}