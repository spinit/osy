<?php

/* 
 * cambiamento Datasource nel caso di caricamento dei dati dell'istanza
 */
$this->trigger('@swapDS',[$this->getField('txt_cs')->getValue()]);
// aggiornamento griglia applicazioni
$this->getField('dgr_app')->setValue($this->getDatasource()->load("select hex(id) as id from osy_app")->getList());