<?php
use Spinit\Datasource\DataSource;


$response = $this->getResponse();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!$this->getField('txt_cs')->getValue()) {
    $response->addError('txt_cs','String di connessione  non impostata');
    $event->stopPropagation();
}
try {
    $DS = new DataSource($this->getField('txt_cs')->getValue());
} catch (\Exception $e) {
    $response->addError('txt_strcon',"Impossibile collegarsi al DB [{$e->getMessage()}]");
    $event->stopPropagation();
}

$this->getApplication()
    ->getInstance()
    ->mapDataSource('', $DS);

$this->getApplication()->init();