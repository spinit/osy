<?php
use Spinit\Util;

/* 
 * Cambio localmente il datasource di istanza con quello corrente per effettuare le modifiche sul DB indicato nella Form.
 * Una volta fatte le modifiche ristabilisco il database di istanza
 */

$ice = $this->getDatasource();
$instance = $this->getApplication()->getInstance();
$listApp = $this->getDatasource()->load("select hex(id) as id, hex(id) as oo from osy_app")->getList();
$listNew = $this->getField('dgr_app')->getBuilder()->getValue($this->getField('dgr_app'));

foreach($listNew as $app) {
    if (isset($listApp[$app])) {
        unset($listApp[$app]);
    } else {
        $rec = ['act' => '1', 'id' => $app];
        $this->getDatasource()->insert('osy_app', $rec);
    }
}
foreach($listApp as  $rec) {
    $this->getDatasource()->delete('osy_app', $rec);
}
$instance->trigger('init');
