<?php
use Spinit\Util;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$instance = $this->getApplication()->getInstance();

if ($cs = $event->getParam(0)) {
    $current = new \Spinit\Datasource\DataSource($cs);
} else {
    $current = new Spinit\Datasource\DataSourceLoad(new \Spinit\Datasource\DataSetArray());
}
$instance->mapDataSource('current', $current);
$instance->mapDataSource('', $current);
