<?php

use Webmozart\Assert\Assert;
use Spinit\Util;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$main = $this->getApplication()->getInstance()->getMain();
$main->trigger('setAuthUser');

$manager = $this->getApplication()->getManager();
$user = $manager->authUser($this->getField('txt_username')->getValue(), $this->getField('txt_password')->getValue());

$main->trigger('setAuthUser', [$user->get('id'), $user->get('aut_lgn'), Util\nvl($user->get('nme'),$user->get('aut_lgn'))]);

$this->getResponse()->setData("href", 'http://'.$this->getApplication()->getInstance()->getInfo('url'));