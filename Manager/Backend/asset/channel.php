<?php

use Spinit\Osy\Core\Channel;

date_default_timezone_set("Europe/Rome");



header('Content-Type: text/event-stream; charset=utf-8');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Expose-Headers: X-Events');
header("Access-Control-Allow-Origin: *");
class MainChannel
{
    private $detail;
    private $data = array();
    
    public function __construct($detail)
    {
        $this->detail = $detail;
    }
    function setMessageData()
    {
        $this->data['messagesDropdown'] = '<i class="fa fa-fw fa-envelope"></i>
            <span class="d-lg-none">Messages
              <span class="badge badge-pill badge-primary">12 New</span>
            </span>
            <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>';
        $time = date('H:i:s');
        $this->data['messagesDropdownContent'] = <<< ENDSNIPPET
            <h6 class="dropdown-header">New Messages:</h6>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
              <strong>David Miller</strong>
              <span class="small float-right text-muted">11:21 AM</span>
              <div class="dropdown-message small">Hey there {$time}! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they don't overflow over to the sides!</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
              <strong>Jane Smith</strong>
              <span class="small float-right text-muted">11:21 AM</span>
              <div class="dropdown-message small">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
              <strong>John Doe</strong>
              <span class="small float-right text-muted">11:21 AM</span>
              <div class="dropdown-message small">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item small" href="#">View all messages</a>
ENDSNIPPET;
    }
    function setAlertData()
    {
        $this->data['alertsDropdown'] = '<i class="fa fa-fw fa-bell"></i>
        <span class="d-lg-none">Alerts
          <span class="badge badge-pill badge-warning">6 New</span>
        </span>
        <span class="indicator text-warning d-none d-lg-block">
          <i class="fa fa-fw fa-circle"></i>
        </span>';
        
        $time = md5(date('H:i:s'));
        $this->data['alertsDropdownContent'] =  <<< ENDSNIPPET
        <h6 class="dropdown-header">New Alerts:</h6>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">
          <span class="text-success">
            <strong>
              <i class="fa fa-long-arrow-up fa-fw"></i>Status Update {$time}</strong>
          </span>
          <span class="small float-right text-muted">11:21 AM</span>
          <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">
          <span class="text-danger">
            <strong>
              <i class="fa fa-long-arrow-down fa-fw"></i>Status Update</strong>
          </span>
          <span class="small float-right text-muted">11:21 AM</span>
          <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">
          <span class="text-success">
            <strong>
              <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
          </span>
          <span class="small float-right text-muted">11:21 AM</span>
          <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item small" href="#">View all alerts</a>
ENDSNIPPET;
    }
    public function getData()
    {
        $this->data = ['globalTime' => date('d D H:i')];
        if ($this->detail) {
            $this->setMessageData();
            $this->setAlertData();
        }
        return $this->data;
    }
}

$content = new MainChannel(!!$this->getInstance()->getManager()->getUser());
(new Channel())
->add(function($send) use ($content) {
    $send(['content'=>$content->getData()]);
})
->run(5);
/*
->each((function() {
    $content = new MainChannel();
    while (true) {
        yield ['content'=>$content->getData()];
        sleep(2);
    }
})());
 * 
 */
