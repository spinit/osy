/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {
    window.addEventListener('load', function() {
        ContentTools.StylePalette.add([
            new ContentTools.Style('Author', 'author', ['p'])
        ]);
    });
    function init()
    {
        var editor  = ContentTools.EditorApp.get();
        editor.destroy();
        if ($('*[data-editable]').length) {
            editor.init('*[data-editable]', 'data-name');
            editor.addEventListener('saved', function (ev) {
               var regions = ev.detail().regions;
                for (var name in regions) {
                    if (regions.hasOwnProperty(name)) {
                        $('textarea#ECT_'+name).val(regions[name]);
                    }
                }
            });
        }
    }
    $(document).on('initContent', '#mainContent', init);
    init();
})();
