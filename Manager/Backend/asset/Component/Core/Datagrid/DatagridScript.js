$(document).on('osy-scroll', '.osy-datagrid', function(event) {
    var ttl = null;
    if (!(ttl = $(this).data('ttl'))) {
        var tt = $('<table></table>');
        var tr = $('thead', this);
        tt.append(tr.clone(true));
        ttl = $('<div></div>').attr('style','position:absolute');
        ttl.append(tt);
        $(this).prepend(ttl);
        $(this).data('ttl', ttl);
        var tdata = $('th > div', tr);
        var thead = $('th > div', tt);
        $(this).data('th-data', tdata);
        $(this).data('th-head', thead);
        function resize() {
            tdata.each(function(idx, cell) {
                $(thead.get(idx)).width($(cell).width());
            });
        }
        $(window).on('resize', resize);
        resize();
    }
    ttl.css('top', $(this).scrollTop());
    //console.log($(this).scrollTop());
});
$(document).on('click', '.osy-datagrid .groupSet', function(event) {
    var grp = this;
    $('.groupItem', $(this).closest('.osy-datagrid')).each(function() {
       this.checked = grp.checked; 
    });
});
$(document).on('click', '.osy-datagrid .groupItem', function(event) {
    event.stopPropagation();
});

$(document).on('click', '.osy-datagrid .dataRow', function() {
    if (!$(this).attr('data-pkey')) {
        $('.groupItem', this).click();
    }
    var post = $('form').serializeArray();
    post.push({'name':'_[osy][pkey]', 'value':$(this).attr('data-pkey')});
    post.push({'name':'_[osy][info]', 'value':$(this).attr('data-info')});
    Desktop.send(
        location,
        post,
        {
            'X-Opensymap-Command':'open',
            'X-Opensymap-Field':$(this).closest('.osy-component').attr('id')
        }
    );
});

$(document).on('keyup', '.osy-datagrid-src input', function() {
    
    var post = $('form').serializeArray();
    Desktop.send(
        location,
        post,
        {
            'X-Opensymap-Field':$(this).closest('.osy-component').attr('id')
        }
    );
});

$(document).on('click', '.osy-datagrid-cmd .btn', function() {
    if (!$(this).attr('command')) {
        return;
    }
    var post = $('form').serializeArray();
    post.push({'name':'_[osy][info]', 'value':$(this).attr('data-info')});
    if ($(this).attr('data-param')) {
        post.push({'name':'_[osy][param]', 'value':$(this).attr('data-param')});
    }
    Desktop.send(
        location,
        post,
        {
            'X-Opensymap-Command':$(this).attr('command'),
            'X-Opensymap-Field':$(this).closest('.osy-component').attr('id')
        }
    );
});
