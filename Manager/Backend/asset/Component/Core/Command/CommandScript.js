/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).on('click', '.osy-command a', function(event) {
    event.preventDefault();
    var post = $(this).closest('form').serializeArray();
    if ($(this).attr('data-param')) {
        post.push({'name':'_[osy][param]', 'value':$(this).attr('data-param')});
    }
    if ($(this).attr('command')) {
        if(history.state && history.state.post) {
            for( var e in history.state.post) {
                post.push({'name': e, 'value':history.state.post[e]});
            }
        }
        Desktop.send(
            location,
            post,
            {'X-Opensymap-Command':$(this).attr('command')}
        );
    }
});
