/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function(){
    
    function sendCommand(view, command, post) {
        Desktop.send(
            location,
            post,
            {
                'X-Opensymap-Command':command,
                'X-Opensymap-Field':view.closest('.osy-component').attr('id')
            },
            function(event, response) {
                Desktop.box(view.parent(), response.data);
            }
        );
    }
    $(document).on('keyup', '.osy-combosearch .search-name', function(event) {
        var base = $(this).closest('.osy-combosearch');
        event.preventDefault();
        event.stopPropagation();
        console.log(event.keyCode);
        if (moveOption(event)) {
            return false;
        }
        sendCommand($(this), base.data('open'), $('form').serializeArray());
    });

    $(document).on('select', '.osy-combosearch .option-row', function(event, key, desc) {
        var cmp = $(this).closest('.osy-combosearch');
        $('.search-id', cmp).val(key);
        $('.search-name', cmp).val(desc);
    });

    function moveOption(event)
    {
        switch(event.keyCode) {
            case 13:
                console.log($('#boxContent .current'));
                $('#boxContent .current').trigger('click');
                break;
            case 37:
            case 39:
                break;
            case 38:
                moveup();
                break;
            case 40:
                movedown();
                break;
            default:
                return false;
        }
        return true;
    }
    
    function moveup()
    {
        var box = $('#boxContent');
        var last = box.find('.current');
        if (!last[0]) {
            box.find('.option-row:last').addClass('current');
        } else {
            last.removeClass('current');
            last = last.prev();
            if (!last[0]) {
                last = box.find('.option-row:last');
            }
            last.addClass('current');
        }
    }
    
    function movedown()
    {
        var box = $('#boxContent');
        var first = box.find('.current');
        if (!first[0]) {
            box.find('.option-row:first').addClass('current');
        } else {
            first.removeClass('current');
            first = first.next();
            if (!first[0]) {
                first = box.find('.option-row:first');
            }
            first.addClass('current');
        }
    }
    
})();