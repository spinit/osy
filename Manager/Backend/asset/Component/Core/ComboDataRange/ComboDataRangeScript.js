/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function(){
    
    function sendCommand(view, command, post) {
        Desktop.send(
            location,
            post,
            {
                'X-Opensymap-Command':command,
                'X-Opensymap-Field':view.closest('.osy-component').attr('id')
            },
            function(event, response) {
                Desktop.box(view.parent(), response.data).css('right','0px');
            }
        );
    }
    $(document).on('click', '.osy-combodatarange .view', function(event) {
        var base = $(this).closest('.osy-combodatarange');
        console.log(base.data('open'), base);
        sendCommand($(this), base.data('open'), $('form').serializeArray());
    });

    $(document).on('setmonth', '.osy-combodatarange', function(event, dat) {
        $('.month', this).val(dat);
        sendCommand($('.view', this), $(this).data('open'), $('form').serializeArray());
    });

    $(document).on('closeBox', '.osy-combodatarange', function(event, dat) {
        $('.month', this).val('');
    });
    
    $(document).on('setdate', '.osy-combodatarange', function(event, dat) {
        var dr = this;
        function setVal(who, dat) 
        {
            $('.date-'+who, dr).val(dat);
            var dsp = dat.split('-').reverse().join('/');
            if (who == 'to' && dsp) {
                dsp = ' - '+dsp;
            }
            $('.view-'+who, dr).html(dsp);
            $('.date-'+who).trigger('change');
        }
        if (!$('.date-from', this).val() || $('.date-to', this).val()) {
            // se la prima data è vuota oppure la seconda data è già impostata ... 
            // allora si reinizializza
            setVal('to', '');
            setVal('from', dat);
        } else {
            // la seconda data non è impostata ... quindi se la prima è impostata
            // occorre evitare che from > to
            if ($('.date-from', this).val()) {
                if ($('.date-from', this).val() > dat) {
                    //swap
                    setVal('to', $('.date-from', this).val());
                    setVal('from', dat);
                } else {
                    setVal('to', dat);
                }
                $(event.target).trigger('close');
            } else {
                setVal('from', dat);
            }
        }
    });

})();