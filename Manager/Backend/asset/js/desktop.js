(function($, window, document, undefined){
    var Desktop = {}
    window.Desktop = Desktop;
    var commonHeaders = {};
    
    var debug = (console && console.log) ? console.log : function() {};
    var error = (console && console.error) ? console.error : function() {};
    function nvl(a,b)
    {
        return a ? a : b;
    }
    function cleanUrl(url)
    {
        url = nvl(url, location);
        return nvl(url.href, url);
    }
    function sendData(url, param, headers, callback)
    {
        var target = window;
        if ($.isArray(url)) {
            target = url.shift();
            url = url.shift();
        }
        if (typeof(param) == 'function') {
            callback = param;
            param = [];
            headers = {};
        }
        if (typeof(headers) == 'function') {
            callback = headers;
            headers = {};
        }
        $.extend(headers, commonHeaders);
        var arg = {
            'method':'POST',
            'data':param,
            'headers' : headers
        };
        $.ajax(cleanUrl(url), arg).done(function(response) {
            if (typeof(callback) == 'function')
            {
                /**
                 * Alla callback viene passato un evento che permette di poter
                 * indicare se proseguire con la normale gestione oppure no.
                 */
                var stop = 0;
                var ev = {
                    'stopPropagation':function() {
                        stop = 1;
                    }
                };
                callback(ev, response);
                if (stop || nvl(response.type, '') != '') {
                    return;
                }
            }
            if (!response) {
                Desktop.alert('Risposta non pervenuta');
            }
            if (response.message) {
                Desktop.alert(response.message);
            }
            if (response.status != 'success') {
                return;
            }
            if (nvl(response.type, '') == '') {
                try {
                    processData.apply(target, [response.data]);
                } catch (e) {
                    error(e, url, arg);
                }
            }
            if (response.url) {
                history.replaceState(history.state, null, response.url);
            }
        }).fail(function(){
            processData.apply(target,[{'content':{'mainContent':arguments[0].responseText}}]);
        });
    }
    var jsAdded = {};
    function addJs(jsList) {
        function load()
        {
            if (jsList.length<=0) {
                return;
            }
            var src = jsList.shift();
            if (src in jsAdded) {
                return load();
            }
            jsAdded[src] = 1;
            console.log(src);
            $.getScript(src, load);
        }
        load();
    }
    Desktop.addJs = addJs;
    
    var cssAdded = {};
    function addCss(cssList) {
        $.each(cssList, function(key, value) {
            if (value in cssAdded) {
                return;
            }
            cssAdded[value] = 1;
            $('<link rel="stylesheet" type="text/css" href="'+value+'"/>').appendTo('head');
        });
    }
    function processData(data) {
        if (typeof(data) == typeof('')) {
            return;
        }
        if (!data) {
            throw 'NO DATA';
        }
        if (data.href) {
            var url = data.href;
            if ($.isArray(url)) {
                url = url.shift();
            }
            location = url;
            return;
        }
        if (data.js) {
            addJs(data.js);
        }
        if (data.css) {
            addCss(data.css);
        }
        if (data.content) {
            writeData(data.content);
        }
        if (data.command) {
            execCommand.apply(this,[data.command]);
        }
        if (data.error) {
            execError(data.error);
        }
    }
    function writeData(data) {
        $.each(data, function(key, value) {
            var cmp = $('#'+key);
            var elm = $('>.content', cmp);
            if (!elm[0]) {
                elm = cmp;
            }
            elm.html(value);
            cmp.trigger('initContent');
        });
    }
    function execCommand(data)
    {
        var target = this;
        $.each(data, function(key, value) {
            var code = value;
            var args = [];
            if ($.isArray(value)) {
                code = value.shift();
                args = value;
            }
            (new Function(code)).apply(target, args);
        });
    }
    function displayErrorElement(info) 
    {
        var cnt = $('<div class="error-message"></div>').html(info);
        var el = $(this).addClass('error');
        el.append(cnt);
        $('input', this).one('focus', function() {
            el.removeClass('error');
            cnt.remove();
        });
    }
    /**
     * Come visualizzatore di default si prende displayErrorElement.
     * Ogni elemento di errore si attende della forma [idelemento, messaggio].
     * Se si vuole passare un visualizzatore alternativo basta passare l'errore
     * nella forma [idelemento, code, arg1(info){, argn}*]
     * @param {type} errors
     * @returns {undefined}
     */
    function execError(errors) {
        $.each(errors, function(key, value) {
            var el = $('#'+value.shift());
            var fnc = displayErrorElement;
            if (value.length>1) {
                fnc = (new Function('info', value.shift()));
            }
            fnc.apply(el, value)
        });
    }
    Desktop.send = sendData;
    Desktop.listen = function (url) {
        var target = window;
        if ($.isArray(url)) {
            target = url.shift();
            url = url.shift();
        }
        var es = new EventSource(cleanUrl(url));
        es.onmessage = function(event)
        {
            processData.apply(target, [JSON.parse(event.data)]);
        }
        return es;
    }
    Desktop.alert = function(msg) {
        alert(msg);
    }
    
    Desktop.goto = function(url) {
        location = cleanUrl(url);
    }
    $(window).on('popstate', function()
    {
        sendData(location, history.state && history.state.post);
    });
    
    Desktop.box = function(target, content)
    {
        $('#boxContent').trigger('close');
        var box = $('<div id="boxContent" style="border: 1px solid silver; position: absolute;background-color: white;padding: 5px;border-radius: 3px; z-index:100;"></div>');
        box.on('close', function(){
            box.remove();
            $(target).trigger('closeBox');
        })
        box.on('click', '.option-row', function(event) {
            $(this).trigger('select',[$(this).data('id'), $(this).data('name'), $(this)])
            $('#boxContent').trigger('close');
        });
        box.on('click', function(event) {
            event.stopPropagation();
        });
        $(document).one('click', function(){box.trigger('close')});
        $(target).append(box);
        box.html(content);
        return box;
    }
})(jQuery, window, document);
