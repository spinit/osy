/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function resizeCommandBar() {
    $('#mainCommand').width($('#mainContent').width());
}
$(window).on('resize', function() {
    resizeCommandBar();
});

$(document).on('change', 'input.recorder', function() {
    var state = history.state || {};
    state.form = state.form || {};
    switch(this.type) {
        case 'checkbox':
            state.form[this.name+'__'+this.value] = this.checked;
            break;
        default:
            state.form[this.name] = this.value;
            break;

    }
    $(this).closest('.osy-component').addClass('changed');
    history.replaceState(state, null);
});

$(document).on('recorder', 'input.recorder', function() {
    var state = history.state || {};
    state.form = state.form || {};
    switch(this.type) {
        case 'checkbox':
            if (state.form[this.name+'__'+this.value] !== undefined) {
                this.checked = state.form[this.name+'__'+this.value];
                $(this).closest('.osy-component').addClass('changed');
            }
            break;
        default:
            if (state.form[this.name] !== undefined) {
                this.value = state.form[this.name];
                $(this).closest('.osy-component').addClass('changed');
            }
            break;

    }
});

$(document).on('click', 'a.form', function(event) {
    event.preventDefault();
    Desktop.send(this.href);
});

// inizializzazione form
$(document).on('initContent', '#mainContent', function(event) {
    if (event.target == this) {
        resizeCommandBar();
        $('.osy-component').trigger('initContent');
    }
});

// inizializzazione componenti
$(document).on('initContent', '.osy-component', function() {
    $('.recorder', this).trigger('recorder');
});

// qualsiasi inizializzazione produce un refactoring dell'interfaccia
$(document).on('initContent', function() {
    $(window).trigger('resize');
});
