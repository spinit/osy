<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if($this->getInstance()->getManager()->getUser()) {
    ob_start();
    include(dirname(__DIR__).'/template/topMenu.php');
    $response->addContent('mainNav', ob_get_clean());
    $response->addData('exec','$(window).trigger("initTooltip")');
}