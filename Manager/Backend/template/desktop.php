<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Spinit s.r.l.">
  <title>Opensyamp</title>
  <!-- Bootstrap core CSS-->
  <link href="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{WEB_ROOT}}/__asset/opensymap/admin/css/sb-admin.css" rel="stylesheet">
  <link href="{{WEB_ROOT}}/__asset/opensymap/css/desktop.css" rel="stylesheet">
  
  <link href="{{WEB_ROOT}}/__asset/opensymap/Component/Core/Command/CommandStyle.css" rel="stylesheet">
  <link href="{{WEB_ROOT}}/__asset/opensymap/Component/Core/Datagrid/Datagrid.css" rel="stylesheet">
</head>
<?php
    if ($this->getUser()) {
?>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    </nav>
    <section class="content-wrapper">
<?php
    } else {
?>
<body>
    <section style="min-height: 100%; padding:50px">
<?php
    }
?>
        <form style="margin-bottom:57px;" onsubmit="return false;">
            <input type="hidden" name="_[osy][init]" value="1"/>
            <div id="mainContent">
            </div>
        </form>
    </section>
    <!-- Bootstrap core JavaScript-->
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/jquery/jquery.min.js"></script>
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/chart.js/Chart.min.js"></script>
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/datatables/jquery.dataTables.js"></script>
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/js/sb-admin.js"></script>
    <!-- Custom scripts for this page-->
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/js/sb-admin-datatables.js"></script>
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/js/sb-admin-charts.js"></script>
    
    
    <script src="{{WEB_ROOT}}/__asset/opensymap/js/desktop.js"></script>
    <script src="{{WEB_ROOT}}/__asset/opensymap/js/init.js"></script>
    <script>
    $(document).on('click', 'a.command', function(event) {
        var cmd = $(this);
        var src = cmd.data('osy-send');
        if (!src) {
            src = cmd.attr('href');
        } else {
            src = "{{WEB_ROOT}}/__asset/opensymap/"+src;
        }
        event.stopPropagation();
        event.preventDefault();
        Desktop.send([cmd, src]);
    });

    $(function () {
        // richiesta menu' ...
        Desktop.send("{{WEB_ROOT}}/__asset/opensymap/init/", function() {
            // e si richiede il contenuto della pagina in POST
            Desktop.send(location, history.state && history.state.post);
            // successivamente si fa partire il flusso dati standard del canale
            setTimeout(function() {
                Desktop.listen("{{WEB_ROOT}}/__asset/opensymap/channel/");
            }, 500);
        });
    });
    </script>
    <div class="modal fade show" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;">
      <div class="modal-dialog" role="document">
        <div class="modal-content" style="border:2px solid red;">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="color:red">Attenzione !</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
            <div class="modal-body">L'elemento corrente verrà cancellato dal sistema. <br/>Continuare con l'operazione?</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Annulla</button>
            <span class='osy-command'>
            <a class="btn btn-danger" command="delete" data-dismiss="modal">Continua</a>
            </span>
          </div>
        </div>
      </div>
    </div>
</body>

</html>
