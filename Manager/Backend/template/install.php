<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Spinit s.r.l.">
  <title>Opensyamp</title>
  <!-- Bootstrap core CSS-->
  <link href="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <!--link href="{{WEB_ROOT}}/__asset/opensymap/admin/css/sb-admin.css" rel="stylesheet"-->
  <style>

@media (max-width: 400px) {
    #globalTime {
        display: none;
    }
}
label {
    display : block;
    margin-bottom: 0px;
    margin-top:5px;
}

.error {color: red}
.error input {border-color: red;}
.error-message {display:none; bottom:0px;}
.error .error-message{display:block; position:absolute;}

.content-wrapper {padding-top:0px;}
#mainCommand
{
    position:fixed; bottom:0px; border-top:1px solid silver; background-color: #e9ecef;
}
.scroll-to-top {
    bottom:50px;
}
body.sticky-footer {
    margin-bottom:0px;
}
body.sticky-footer .content-wrapper {
    min-height: calc(100vh - 56px);
}
.osy-panel label
{
    display:block;
}
.osy-component {position:relative;}
.osy-component input[type=text] {width:100%;}
.osy-component input[type=password] {width:100%;}
.form-row {margin-left: 0px; margin-right:0px;}
.osy-panel.form-group > label {
    font-weight: bold;
    color:silver;
}
.osy-command {
    text-align: center;
    padding:15px 0px 2px;
}
  </style>
</head>

<body class="fixed-nav sticky-footer" id="page-top">
    <section class="content-wrapper">
        <h1>Opensymap</h1>
        <div>
            Benvenuto in <strong>Opensymap</strong>, il Framework per la costruzione dei gestionali.
        </div>
        <div>
            Opensymap è un middleware che permette di creare più istanze applicative. Ogni istanza ha:
        <ul>
            <li>un suo database di riferimento</li>
            <li>un dominio/path di backend</li>
            <li>un dominio/path di frontend.</li>
        </ul>
        Ogni applicazione istallata nell'istanza può decidere quale comportamento adottare nei due ambienti (backend, frontend).
        </div>
        <div>
            Di seguito verranno richiesti 2 gruppi di informazioni:
            <ul>
                <li>
                    Dati relativi all'istanza quali le url di backend e frontend. Da queste due url sarà possibile accedere alla stessa istanza
                    che però adotterà comportamenti diversi per la visualizzazione dei dati;
                </li>
                <li>
                    Dati relativi all'accesso dell'utente amministratore dal backend e, facoltativamente, dal frontend.
                </li>
            </ul>
        </div>
        <form>
            <div id="mainContent">
            </div>
        </form>
    </section>
    <!-- Bootstrap core JavaScript-->
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/jquery/jquery.min.js"></script>
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{WEB_ROOT}}/__asset/opensymap/admin/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{WEB_ROOT}}/__asset/opensymap/js/desktop.js"></script>
    <script>
        function resizeCommand()
        {
            console.log('resize');
            $('#mainCommand').width($('#mainContent').width());
        }
        $(window).on('resize',resizeCommand);
        // chiamata POST
        Desktop.addJs(['{{WEB_ROOT}}/__asset/opensymap/Component/Core/Command/CommandScript.js']);
        Desktop.send(location.href);
    </script>
</body>

</html>
