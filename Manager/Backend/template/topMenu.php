<?php
use Spinit\Osy\Type\ApplicationInterface;
use Spinit\Util;

class SpinitTopMenu
{
    public $menuBar = array();
    public function getMenuBar()
    {
        return $this->menuBar['/'];
    }
    public function register(ApplicationInterface $app)
    {
        $base = $app->getName(). '@menu:';
        foreach($app->getAdapter()->getMenuList() as $k => $menu) {
            $this->registerItem($app, $menu, $base, $k, rtrim(Util\arrayGet($menu, 'parent'), '/'). '/');
        }
    }
    private function registerItem($app, $menu, $base, $k, $parent)
    {
        $base = rtrim($base,'/');
        if ($code = Util\arrayGet($menu, 'code')) {
            $k = $code;
        } 
        $key = $base.(substr($base,-1)==':' ? '' : '/').$k.'/';
        $childs = Util\arrayGet($menu, 'childs', []);
        unset($menu['childs']);
        // viene inserito il parent che non ancora non è stato inserito
        if (!isset($this->menuBar[$parent])) {
            $dummy = ['key'=>$parent , 'childs' => []];
            $this->menuBar[$parent] = &$dummy;
        }                
        $item = array_merge($menu, [
            'childs' => [],
            'key'=>$key,
            'href'=>'',
            'parent' => $parent]);
        
        if (Util\arrayGet($item, 'form')) {
            $item['href'] = $app->getUrl(Util\arrayGet($item, 'form'));
        }
        if (isset($this->menuBar[$key])) {
            foreach($item as $k=>$v) {
                if (in_array($k, ['childs'])) {
                    continue;
                }
                $this->menuBar[$key][$k] = $v;
            }
            $this->menuBar[$this->menuBar[$key]['parent']]['childs'][] = &$this->menuBar[$key];
        } else {
            $this->menuBar[$key] = &$item;
            $this->menuBar[$parent]['childs'][] = &$this->menuBar[$key];
        }
        unset($item);
        foreach($childs as $k => $child) {
            $this->registerItem($app, $child, $key, $k, $key);
        }
    }
}
$sp = new SpinitTopMenu();
foreach($this->getInstance()->getApplicationList() as $app) {
    $sp->register($app);
}
        
function display($bar) {
    if (!count(Util\arrayGet($bar, 'childs',[]))) {
        return new Util\Tag('');
    }
    $ul = new Util\Tag('ul');
    $ul->att('class', 'sidenav-second-level collapse');
    $ul->att('id', md5(Util\arrayGet($bar, 'key')));
    foreach($bar['childs'] as $child) {
        $li = $ul->add(new Util\Tag('li'));
        $li->att(['class' => "nav-item", 'data-toggle'=>"tooltip", 'data-placement'=>"right", 'title'=>Util\arrayGet($child,'title')]);
        $li->add(displayText($child, md5(Util\arrayGet($bar, 'key'))));
        $li->add(display($child));
    }
    return $ul;
}
function displayText($child, $parent)
{
    if ($href = Util\arrayGet($child, 'href')) {
    $snippet = <<<EOTEXT
    <a class="nav-link form" href="{$href}">
        <i class="fa fa-fw fa-{{image}}"></i>
        <span class="nav-link-text">{{text}}</span>
      </a>
EOTEXT;
    } else {
        $md5 = md5(Util\arrayGet($child,'key'));
        $snippet = <<<EOTEXT
      <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#{$md5}" data-parent="#{$parent}">
        <i class="fa fa-fw fa-{{image}}"></i>
        <span class="nav-link-text">{{text}}</span>
      </a>
EOTEXT;
    }
    return str_replace(
        array('{{href}}', '{{image}}', '{{text}}'),
        array(Util\arrayGet($child,'href'), Util\arrayGet($child,'image'), Util\arrayGet($child,'text')),
        $snippet);
}
?>
<style>
    .dropdown-menu {
        right:0px;
        left: initial;
    }
</style>
<a class="navbar-brand form" href="<?php echo $this->getInstance()->makePath('')?>">Logo</a>
<div>
        <form class="form-inline xmy-2 xmy-lg-0 xmr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>

</div>
<code id="globalTime">....</code>
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarResponsive">
    <?php if(0) debug(); else echo display($sp->getMenuBar())->att('class','navbar-nav navbar-sidenav');?>
  <ul class="navbar-nav sidenav-toggler">
    <li class="nav-item">
      <a class="nav-link text-center" id="sidenavToggler">
        <i class="fa fa-fw fa-angle-left"></i>
      </a>
    </li>
  </ul>
  <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      </a>
      <div class="dropdown-menu" aria-labelledby="messagesDropdown" id="messagesDropdownContent">
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        
      </a>
      <div class="dropdown-menu" aria-labelledby="alertsDropdown" id="alertsDropdownContent">
      </div>
    </li>
    <!-- li class="nav-item">
      <form class="form-inline my-2 my-lg-0 mr-lg-2">
        <div class="input-group">
          <input class="form-control" type="text" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-primary" type="button">
              <i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </form>
    </li -->
    <li class="nav-item">
      <a class="nav-link command" data-toggle="modal" data-target="#exampleModal" data-osy-send="logout">
        <i class="fa fa-fw fa-sign-out"></i>Logout</a>
    </li>
  </ul>
</div>
  
<?php
function debug()
{
?>
  <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
      <a class="nav-link" href="index.html">
        <i class="fa fa-fw fa-dashboard"></i>
        <span class="nav-link-text">Dashboard</span>
      </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
      <a class="nav-link" href="charts.html">
        <i class="fa fa-fw fa-area-chart"></i>
        <span class="nav-link-text">Charts</span>
      </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
      <a class="nav-link" href="tables.html">
        <i class="fa fa-fw fa-table"></i>
        <span class="nav-link-text">Tables</span>
      </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
      <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
        <i class="fa fa-fw fa-wrench"></i>
        <span class="nav-link-text">Components</span>
      </a>
      <ul class="sidenav-second-level collapse" id="collapseComponents">
        <li>
          <a href="navbar.html">Navbar</a>
        </li>
        <li>
          <a href="cards.html">Cards</a>
        </li>
      </ul>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
      <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
        <i class="fa fa-fw fa-file"></i>
        <span class="nav-link-text">Example Pages</span>
      </a>
      <ul class="sidenav-second-level collapse" id="collapseExamplePages">
        <li>
          <a href="login.html">Login Page</a>
        </li>
        <li>
          <a href="register.html">Registration Page</a>
        </li>
        <li>
          <a href="forgot-password.html">Forgot Password Page</a>
        </li>
        <li>
          <a href="blank.html">Blank Page</a>
        </li>
      </ul>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
      <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
        <i class="fa fa-fw fa-sitemap"></i>
        <span class="nav-link-text">Menu Levels</span>
      </a>
      <ul class="sidenav-second-level collapse" id="collapseMulti">
        <li>
          <a href="#">Second Level Item</a>
        </li>
        <li>
          <a href="#">Second Level Item</a>
        </li>
        <li>
          <a href="#">Second Level Item</a>
        </li>
        <li>
          <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">Third Level</a>
          <ul class="sidenav-third-level collapse" id="collapseMulti2">
            <li>
              <a href="#">Third Level Item</a>
            </li>
            <li>
              <a href="#">Third Level Item</a>
            </li>
            <li>
              <a href="#">Third Level Item</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
      <a class="nav-link" href="#">
        <i class="fa fa-fw fa-link"></i>
        <span class="nav-link-text">Link</span>
      </a>
    </li>
  </ul>
<?php
}