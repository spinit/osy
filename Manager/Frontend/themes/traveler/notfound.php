<!DOCTYPE HTML>
<!--
	Aesthetic by gettemplates.co
	Twitter: http://twitter.com/gettemplateco
	URL: http://gettemplates.co
-->
<html>
    <?php echo $this->getContent('head.php')?>
    <body>
		
        <?php $this->display('body.begin');?>
	<div class="gtco-loader"></div>
	
	<div id="page">

	
	<!-- <div class="page-inner"> -->
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="{{WEB_ROOT}}"><?php $this->display('body.name');?> <em>.</em></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
                                    <?php $this->display('body.menu');?>
					<ul>
						<li><a href="destination.html">Destination</a></li>
						<li class="has-dropdown">
							<a href="#">Travel</a>
							<ul class="dropdown">
								<li><a href="#">Europe</a></li>
								<li><a href="#">Asia</a></li>
								<li><a href="#">America</a></li>
								<li><a href="#">Canada</a></li>
							</ul>
						</li>
						<li><a href="pricing.html">Pricing</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>	
				</div>
			</div>
			
		</div>
	</nav>

	<div id="gtco-subscribe" style="background-color: #fd5959   ">
		<div class="gtco-container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Page not found</h2>
                                        <p><code style="display:block;">/<?php echo $path?></code></p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
                                    <form class="form-inline" onsubmit="return false">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="message" class="sr-only">Contact me please!</label>
								<input type="text" class="form-control" id="message" placeholder="Message">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<button type="submit" class="btn btn-default btn-block">Contact me please</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
    <?php echo $this->getContent('footer.php')?>
    </body>

</html>

