<?php
$this->display('index.begin');
?>
<!DOCTYPE HTML>
<!--
	Aesthetic by gettemplates.co
	Twitter: http://twitter.com/gettemplateco
	URL: http://gettemplates.co
-->
<html>
    <?php echo $this->getContent('head.php')?>
    <?php echo $this->getContent('body.php')?>
</html>
<?php
$this->display('index.end');

