    <head>
        <?php $this->display('head.meta.begin');?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Traveler &mdash; Free Website Template, Free HTML5 Template by GetTemplates.co</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by GetTemplates.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="GetTemplates.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

        <?php $this->display('head.meta.end');?>
        <?php $this->display('head.link.begin');?>
        
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{WEB_ROOT}}theme/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{WEB_ROOT}}theme/css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="{{WEB_ROOT}}theme/css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{WEB_ROOT}}theme/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{WEB_ROOT}}theme/css/magnific-popup.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{WEB_ROOT}}theme/css/bootstrap-datepicker.min.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="{{WEB_ROOT}}theme/css/owl.carousel.min.css">
	<link rel="stylesheet" href="{{WEB_ROOT}}theme/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="{{WEB_ROOT}}theme/css/style.css">


        <?php $this->display('head.link.end');?>
        <?php $this->display('head.script.begin');?>
	<!-- Modernizr JS -->
	<script src="{{WEB_ROOT}}theme/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
        <?php $this->display('head.script.end');?>

    </head>
